with System ;
with Interfaces.C ; use Interfaces.C ;
with gsl ;

package fns is

   function Value( arg : double ; params : System.Address ) 
            return double;
   Pragma Convention(C,Value);

   function Ex1( arg : double ; params : System.Address ) 
            return double;
   Pragma Convention(C,Ex1);

   function Tangent( arg : double ; params : System.Address ) 
            return double;
   Pragma Convention(C,Tangent);

   function LogSqrt( arg : double ; params : System.Address ) 
            return double;
   Pragma Convention(C,LogSqrt);

   function Power( arg : double ; params : System.Address ) 
            return double;
   Pragma Convention(C,Power);

   function Sin( arg : double ; params : System.Address ) 
            return double;
   Pragma Convention(C,Sin);

end fns ;