with Ada.Numerics.Elementary_Functions ; use Ada.Numerics.Elementary_Functions ;

package body fns is
    function Value( arg : double ; params : System.Address ) 
            return double is
    begin
        return arg ;
    end Value;
   
    function Ex1( arg : double ; params : System.Address ) 
            return double is
        use gsl.double_elementary_functions ;
    begin
        return arg ** 1.5 ;
    end Ex1 ;


    function Tangent( arg : double ; params : System.Address ) 
            return double is
        use gsl.double_elementary_functions ;
    begin
        return Tan(arg, 2.0 * Ada.Numerics.Pi) ;
    end Tangent;


   function LogSqrt( arg : double ; params : System.Address ) 
            return double is
    use gsl.double_elementary_functions ;

        alpha : double ;
                for alpha'Address use params ;
    begin
        return Log(alpha * arg , base => Ada.Numerics.e ) / sqrt( arg );
    end LogSqrt; 


    function Power( arg : double ; params : System.Address ) 
            return double is

        pow : int ;
            for pow'Address use params ;
    begin
        return arg ** Integer(pow) + 1.0 ;
    end Power ;

   function Sin( arg : double ; params : System.Address ) 
            return double is
    begin
        return gsl.double_elementary_functions.Sin(arg);
    end Sin ;
end fns ;