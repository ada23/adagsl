#!/bin/bash
rm bin/*
pushd chebyshev ; alr build ; popd
pushd complex ; alr build ; popd
pushd fit ; alr build ; popd
pushd movstat ; alr build ; popd
pushd randist ; alr build ; popd
pushd statistics ; alr build ; popd
pushd bspline ; alr build ; popd
pushd clarks ; alr build ; popd
pushd deriv ; alr build ; popd
pushd fft ; alr build ; popd
pushd integration ; alr build ; popd
pushd permutation ; alr build ; popd
pushd rng ; alr build ; popd
pushd combination ; alr build ; popd
pushd filter ; alr build ; popd
pushd interp ; alr build; popd
pushd qrng; alr build; popd
pushd sf; alr build; popd
pushd vector; alr build; popd
