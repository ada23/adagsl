#!/bin/bash

# pushd vector/test ; alr build ; ./test.sh ; popd
pushd filter/test ; alr build ;./test.sh ; popd
# pushd permutation/test ; alr build ;./test.sh ; popd
pushd randist/test ; alr build ;./test.sh ; popd
# pushd combination/test ; alr build ;./test.sh ; popd
pushd interp/test ; alr build ; ./test.sh ; popd
# pushd fit/test ; alr build ; ./test.sh ; popd
pushd movstat/test ; alr build ; ./test.sh ; popd
pushd qrng/test ; alr build ; ./test.sh ; popd
pushd rng/test ; alr build ;./test.sh ; popd
pushd statistics/test ; alr build ;./test.sh ; popd