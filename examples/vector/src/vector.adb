with Ada.Text_Io; use Ada.Text_IO ;
with Ada.Long_Float_Text_IO ; use Ada.Long_Float_Text_IO ;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io;
with Interfaces.C; use Interfaces.C ;
with Interfaces.C.Strings; use Interfaces.C.Strings;
with Ada.Text_Io.C_Streams;

with gnat.source_info ;

with gsl ;
with gsl.rng ;
with gsl.randist ;
with gsl.statistics ;
with gsl.rstat ;
with gsl.vector_double ;
with gsl.sort_double ;

procedure Vector is
   vec : access gsl.vector_double.gsl_vector := gsl.vector_double.alloc(5);
   procedure Setup is
   begin
      for vi in 1..5
      loop
         gsl.vector_double.set(vec,size_t(vi-1),double(vi)+0.5);
      end loop ;
   end Setup ;

   procedure Test0 is
      outfile : File_Type ;
      Status : Int ;
   begin
      Put_Line(gnat.source_info.enclosing_entity);
      Create(outfile,Out_File,"vector.txt");
      Status := gsl.vector_double.fprintf(Ada.Text_Io.C_Streams.C_Stream(outfile),
                                vec,
                                New_String("%.5g"));
      Close(outfile);
   end Test0;

   procedure Test1 is
      vin : access gsl.vector_double.gsl_vector := gsl.vector_double.alloc(5);
      infile : File_Type;
      Status : Int ;
   begin
      Put_Line(gnat.source_info.enclosing_entity);
      Open(infile,In_File,"vector.txt");
      Status := gsl.vector_double.fscanf(Ada.Text_Io.C_Streams.C_Stream(infile),vin);
      Close(infile);
      for i in 1..Integer(vin.size)
      loop
         Put(i); Put(" => "); Put(Long_float(gsl.vector_double.get(vin,size_t(i-1)))); New_Line;
         Flush ;
      end loop ;
   end Test1 ;

   procedure Test2 is
   begin
      Put_Line(gnat.source_info.enclosing_entity);
      for vi in 1..10
      loop
         Put(vi); Put(" => "); Put(Long_float(gsl.vector_double.get(vec,size_t(vi-1)))); New_Line;
         Flush ;
      end loop ;
   end Test2 ;
begin
   Setup;
   Test0;
   Test1;
   Test2;
end Vector;
