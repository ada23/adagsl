set term png size 1000,500
set datafile separator ";"
set output output
set key right bottom
set title "Square Wave filtered K=7 " 
plot  \
     filename using 2:3 title "signal" with lines lc "blue" , \
     filename using 2:4 title "median filter" with lines lc "red" , \
     filename using 2:5 title "recursive median filter" with lines lc "green"