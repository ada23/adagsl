#!/bin/bash
../../bin/filter
gnuplot -e "filename='Filter.Test1.kernel.csv' ; output='test1_kernel.png'"  kernel.gp
gnuplot -e "filename='Filter.Test1.data.csv' ;   output='test1_data.png'"  data.gp
gnuplot -e "filename='Filter.Test2.filtered.csv' ;   output='test2_filtered.png'" filtered.gp
gnuplot -e "filename='Filter.Test3.csv' ;   output='test3.png'" sqwave.gp
gnuplot -e "filename='Filter.Test4.csv' ;   output='test4.png'" impulse.gp