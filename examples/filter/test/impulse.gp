set term png size 1000,1000
set datafile separator ";"
set output output

set key left top
set title "Impulse detection example"
plot filename using 1:2 title "data" with lines lc "blue" , \
     filename using 1:3 title "filtered" with lines lc "green" , \
     filename using 1:6 title "lower" with lines lc "red" , \
     filename using 1:7 title "upper" with lines lc "brown" 

