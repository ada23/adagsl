set term png size 1000,500
set datafile separator ";"
set output output
set key right bottom
set title "Gaussian Filter Output " 
plot filename using 1:2 title "Data" with lines , \
     filename using 1:3 title "alpha=0.5" with lines , \
     filename using 1:4 title "alpha=3" with lines , \
     filename using 1:5 title "alpha=10" with lines      