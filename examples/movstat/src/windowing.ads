with System ;
with Interfaces.C ; use Interfaces.C ;
with gsl ;
package windowing is
   function windowingfn( windowsize : size_t ; data : in out gsl.double_array ; params : System.Address ) return double;
   Pragma Convention(C,windowingfn);
end windowing ;