with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_IO ; use Ada.Integer_Text_IO ;
with Ada.Long_Float_Text_IO ; use Ada.Long_Float_Text_IO ;

with gsl.sort_double ;
with gsl.statistics ;
package body windowing is

   function windowingfn( windowsize : size_t ; data : in out gsl.double_array ; params : System.Address ) return double is
        alpha : Long_Float ;
        for alpha'Address use params ;
   begin
        -- Put("Size "); Put(Integer(arg1));
        -- Put(" Param "); Put(param);
        -- New_Line;
        gsl.sort_double.sort(data , 1 , windowsize );
        return gsl.statistics.trmean_from_sorted_data(double(alpha),data,1,windowsize);
   end windowingfn ;

end windowing ;