set term png size 1000,500
set datafile separator ";"
set output output
set key right bottom
set title "Gaussian Noise. Moving window size=11 stats" 
plot filename using 1:2 title "x" with lines , \
     filename using 1:3 title "mean" with lines , \
     filename using 1:4 title "min" with lines , \
     filename using 1:5 title "max" with lines