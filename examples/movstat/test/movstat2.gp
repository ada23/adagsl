set term png size 1000,600
set datafile separator ";"
set output output
set multiplot layout 2,1
set key left top
set title "Time series of piecewise constant variance"
plot filename using 1:2 title "x" with lines 
set title "Estimates using moving window (size=41)"
plot filename using 1:4 title "xmad" with lines , \
     filename using 1:5 title "xiqr" with lines , \
     filename using 1:6 title "xSn" with lines , \
     filename using 1:7 title "xQn" with lines , \
     filename using 1:8 title "xsd" with lines , \
     filename using 1:3 title "variance" with lines , \
          
