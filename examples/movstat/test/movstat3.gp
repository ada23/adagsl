set term png size 1000,500
set datafile separator ";"
set output output
set key left top
set title "Moving Window trimmed mean (custom function)" 
plot filename using 1:2 title "data" with lines , \
     filename using 1:3 title "trimmed mean" with lines 