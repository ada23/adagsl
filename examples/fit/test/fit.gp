set term png size 1000,500
set datafile separator ";"
set output output
set key top left
set grid
set title "Weighted Fit" 
plot filename using 1:4 title "Low" with points pointsize 2 , \
     filename using 1:5 title "High" with points ps 2 , \
     filename using 1:2 title "Estimate" with lines , \
     datafile using 1:2 title "Data" with lines , \
     filenamew using 1:2 title "Estimate - Weighted" with points
    