set term png size 1000,500
set datafile separator ";"
set output output
set key top left
set grid
 
plot filename using 1:2 title "Multi Fit" with points pointsize 2 , \
     td using 1:2 title "Test Data" with lines 
    
    