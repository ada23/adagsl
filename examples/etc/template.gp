set term png size 1000,1000
set datafile separator ";"
set output output
set multiplot layout 4,1
set key left top
set title "Gaussian Example 2 - Signal and gaussian smoothed"
plot filename using 1:2 title "x" with lines , \
     filename using 1:3 title "y" with lines

set title "First Order Gaussian smoothed "
plot filename using 1:4 title "dy" with lines

set title "Second Order Gaussian smoothed "
plot filename using 1:5 title "d2y" with lines lc "red"

set title "Finite difference"
plot filename using 1:6 title "xi" with lines lc "green"
