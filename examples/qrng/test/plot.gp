set term png
set datafile separator ";"
set output 'sobol.png'
plot 'sobol.csv' using 1:2
set output 'halton.png'
plot 'halton.csv' using 1:2
set output 'reversehalton.png'
plot 'reversehalton.csv' using 1:2 w line title "Quasi Random Number generator"
set output 'niederreiter_2.png'
plot 'niederreiter-base-2.csv' using 1:2 w points ls 5 title "Quasi Random Number generator"
