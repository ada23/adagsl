with Ada.Text_Io; use Ada.Text_IO ;
with Ada.Long_Float_Text_IO ; use Ada.Long_Float_Text_IO ;
with Ada.Command_Line ; use Ada.Command_Line;
with Interfaces.C; use Interfaces.C ;
with Interfaces.C.Strings; use Interfaces.C.Strings ;
with gsl ;
with gsl.qrng ;

procedure qrng is

   rng : access gsl.qrng.gsl_qrng ; -- := gsl.qrng.alloc(gsl.qrng.sobol,2) ;
   rn : gsl.qrng.qrng_values_type(1..2) ;
   status : Int ;
   outfile : File_Type ;
   procedure Test is
      filename : String := Value(gsl.qrng.name(rng)) & ".csv";
   begin 
      Create(outfile,Out_File,filename);
      Set_Output(outfile);
      --Put("Generator Name : ");
      --Put(Interfaces.C.Strings.Value( gsl.qrng.name(rng) ));
      --New_Line;
      for i in 1..1024 
      loop
         Status := gsl.qrng.get(rng,rn);
         Put(Long_Float(rn(1)));
         Put( " ; ");
         Put(Long_Float(rn(2)));
         New_Line;
      end loop ;
      Close(outfile);
      Set_Output(Standard_Output);
   end Test ;
begin
   --Put_Line("Quasi Random Number Generators ");
   if Argument_Count = 0
   then
     rng := gsl.qrng.alloc(gsl.qrng.sobol,2) ;
   elsif Argument(1) = "sobol"
   then
      rng := gsl.qrng.alloc(gsl.qrng.sobol,2) ;
   elsif Argument(1) = "niederreiter_2"
   then
      rng := gsl.qrng.alloc(gsl.qrng.niederreiter_2,2) ;
   elsif Argument(1) = "halton"
   then
      rng := gsl.qrng.alloc(gsl.qrng.halton,2) ;
   elsif Argument(1) = "reversehalton"
   then
      rng := gsl.qrng.alloc(gsl.qrng.reversehalton,2) ;
   else
      Put("Unsupported generator "); Put_Line(Argument(1));
      return;
   end if;
   Test;
   gsl.qrng.free(rng);
end qrng;
