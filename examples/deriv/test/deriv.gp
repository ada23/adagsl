set term svg size 1000,1000
set datafile separator ";"
set output output
set yrange [-100:100]
set title "Tangent"
plot f using 1:2 with lines title "Tangent values" , \
     f using 1:3 with lines lc "red" title "Derivative"
