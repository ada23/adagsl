with System;
with Ada.Text_IO;            use Ada.Text_IO;
with Ada.Text_IO.C_Streams;
with Ada.Long_Float_Text_IO; use Ada.Long_Float_Text_IO;
with Ada.Integer_Text_IO;    use Ada.Integer_Text_IO;
with Interfaces.C;           use Interfaces.C;
with Interfaces.C.Strings;   use Interfaces.C.Strings;
with Ada.Strings.Unbounded;  use Ada.Strings.Unbounded;
with Ada.Command_Line;       use Ada.Command_Line;
with Ada.Numerics.Elementary_Functions ; use Ada.Numerics.Elementary_Functions ;

with GNAT.Source_Info; use GNAT.Source_Info;
with gsl;
with gsl.math ;
with gsl.deriv ;

with fns ;

procedure Deriv is
   use gsl.double_text_Io;
   use gsl.double_elementary_functions ;
   Verbose : boolean := True ;
   Status : Int ;
   procedure Test1 is
      myname : String := gnat.Source_Info.enclosing_entity ;
      logfile : File_Type ;
      f : aliased gsl.math.gsl_function ;
      result, abserror : aliased double ;

   begin
      if Verbose
      then
         Put_Line(myname);
      end if ;
      f.c_function := fns.Ex1'access ;
      f.params := System.Null_Address ;
      Put_Line("X = 2.0");
      Status := gsl.deriv.central(f'Access,2.0,1.0e-8,result'access,abserror'access);
      Put("Result = "); Put( result ) ; Put(" Abserror "); Put(abserror ) ; New_Line ;
      Put("Expected = "); Put(1.5 * sqrt(double(2.0)) ); New_Line;
      Put_Line("X = 0.0");
      Status := gsl.deriv.forward(f'Access,0.0,1.0e-8,result'access,abserror'access);
      Put("Result = "); Put( result ) ; Put(" Abserror "); Put(abserror ) ; New_Line ;
   end Test1 ;

   procedure Test2 is
      myname : String := gnat.Source_Info.enclosing_entity ;
      logfile : File_Type ;
      x,y : aliased double ;
      result, abserror : aliased double ;
      f : aliased gsl.math.gsl_function ;

   begin
      if Verbose
      then
         Put_Line(myname);
      end if ;
      Create(logfile,Out_File,myname & ".csv");
      Set_Output(logfile);
      f.c_function := fns.Tangent'Access ;
      f.params := System.Null_Address ;
      x := 0.0 ;
      loop
         Put(x) ; Put(" ; ");
         y := fns.Tangent(x,System.Null_Address) ; 
         Put(y) ; Put( " ; ") ;
         Status := gsl.deriv.central(f'Access,x,1.0e-8,result'access,abserror'access);
         Put( result ) ; Put( " ; ");
         Put( abserror ) ; Put( " ; ");
         New_Line;
         if x > Ada.Numerics.Pi - 0.01
         then
            exit ;
         end if ;
         x := x + 0.01 ;
      end loop ;
      Set_Output(Standard_Output);
      Close(logfile);
      Status := gsl.deriv.forward(f'Access,Ada.Numerics.Pi/2.0,1.0e-8,result'access,abserror'access);
      Put("Forward limit "); Put(result) ;
      Status := gsl.deriv.backward(f'Access,Ada.Numerics.Pi/2.0,1.0e-8,result'access,abserror'access);
      Put(" Backward "); Put(result);
      New_Line;
   end Test2 ;

begin
   if Verbose
   then
      Put_line(Gnat.Source_Info.enclosing_entity);
   end if;
   gsl.double_text_Io.Default_exp := 0;
   Test1 ;
   Test2 ;

end Deriv;
