with Ada.Text_IO;            use Ada.Text_IO;
with Ada.Text_IO.C_Streams;
with Ada.Long_Float_Text_IO; use Ada.Long_Float_Text_IO;
with Ada.Integer_Text_IO;    use Ada.Integer_Text_IO;
with Interfaces.C;           use Interfaces.C;
with Interfaces.C.Strings;   use Interfaces.C.Strings;
with Ada.Strings.Unbounded;  use Ada.Strings.Unbounded;
with Ada.Command_Line;       use Ada.Command_Line;

with GNAT.Source_Info; use GNAT.Source_Info;

with gsl;
with gsl.rng;
with gsl.randist;
with gsl.combination ;

procedure Combination is
   Status : Int ;
   procedure Test1 is
      c : access gsl.combination.gsl_combination := gsl.combination.alloc(9,3);
   begin
      Put_Line(gnat.source_info.enclosing_entity);
      gsl.combination.init_first(c);
      Status := gsl.GSL_SUCCESS ;
      while Status = gsl.GSL_SUCCESS
      loop
         Status := gsl.combination.show(c);
         Status := gsl.combination.next(c);
      end loop ;
   end Test1;
   procedure Test2 is
      c : access gsl.combination.gsl_combination := gsl.combination.alloc(9,3);
   begin
      Put_Line(gnat.source_info.enclosing_entity);
      gsl.combination.init_last(c);
      Status := gsl.GSL_SUCCESS ;
      while Status = gsl.GSL_SUCCESS
      loop
         Status := gsl.combination.show(c);
         Status := gsl.combination.prev(c);
      end loop ;
   end Test2;
   procedure Test3 is
      c : access gsl.combination.gsl_combination ;
      Status : int ;
   begin
      Put_Line(gnat.source_info.enclosing_entity);
      for i in 1..4
      loop
         c := gsl.combination.calloc(4,size_t(i));
         Status := gsl.combination.show(c);
         loop
            Status := gsl.combination.next(c);
            if Status /= gsl.GSL_SUCCESS
            then
               exit ;
            end if ;
            Status := gsl.combination.show(c);
         end loop ;
      end loop ;

   end Test3 ;

begin
   Test1;
   Test2;
   Test3;
end Combination;
