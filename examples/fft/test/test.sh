#!/bin/bash
../../bin/fft
gnuplot -e "filename='fft.T1.csv' ; fftfilename='fft.T1.fft.csv' ; rfftfilename='fft.T1.rfft.csv'; output='fft.png'" fft.gp
gnuplot -e "filename='fft.T2.csv' ; fftfilename='fft.T2.fft.csv' ; rfftfilename='fft.T2.rfft.csv'; output='fft2.png'" fft.gp
gnuplot -e "filename='Fft.T4.csv' ; filtered='Fft.T4.filtered.csv' ; output='fft4.png'" fft4.gp
gnuplot -e "filename='Fft.T5.csv' ; filtered='Fft.T5.filtered.csv' ; output='fft5.png'" fft4.gp
gnuplot -e "filename='Fft.T5.freq.csv' ; output='fftspectrum.png'" fftfreq.gp
