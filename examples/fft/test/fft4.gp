set term png size 1000,1000
set datafile separator ";"
set output output
set title "Real FFT and lowpass filtered"
plot filename using 1:2 notitle with linespoints , \
     filtered using 1:2 notitle with linespoints pt 7
