set term png size 1000,1000
set datafile separator ";"
set output output
set multiplot layout 3,1
set title "Complex Radix 2 fft"
set title "Real values"
set yrange [-0.5:2.0]
plot filename using 1:2 notitle with points ps 2 pt 5
set title "FFT"
plot fftfilename using 1:2 notitle  with points ps 2 pt 5
set title "RFFT"
plot rfftfilename using 1:2 notitle  with points pt 7
