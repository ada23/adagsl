with Text_Io; use Text_Io;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io;
with gsl.errno ;

package body ehandler is

    oldhandler : access procedure
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : int;
      arg4 : int) 
    with
      Convention    => C ;

    procedure Setup is
    begin
        oldhandler := gsl.errno.set_error_handler( handler'access );
    end Setup ;
    procedure Reset is
    begin
        if oldhandler /= null
        then
            oldhandler := gsl.errno.set_error_handler( oldhandler );
        end if ;
    end Reset ;
    procedure handler ( reason : Interfaces.C.Strings.chars_ptr; 
                        file : Interfaces.C.Strings.chars_ptr; 
                        line : int; 
                        errno : int) is
    begin
        Put("Reason "); Put(Value(reason));
        Put(" file "); Put(Value(file));
        Put(" line "); Put(Integer(line));
        Put(" errno "); Put(Integer(errno)); Put(" ");
        Put(Value(gsl.errno.strerror(errno))) ;
        New_Line ;
        raise GSL_ERROR ;
    end handler;

end ehandler ;