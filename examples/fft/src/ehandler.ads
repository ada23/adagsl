with Interfaces.C ; use Interfaces.C ;
with Interfaces.C.Strings ; use Interfaces.C.Strings ;

package ehandler is
    GSL_ERROR : exception ;
    procedure Setup ;
    procedure Reset ;
    procedure handler ( reason : Interfaces.C.Strings.chars_ptr; 
                        file : Interfaces.C.Strings.chars_ptr; 
                        line : int; 
                        errno : int) 
    with
      Convention    => C ;
end ehandler;