with Ada.Text_IO;            use Ada.Text_IO;
with Ada.Text_IO.C_Streams;
with Ada.Long_Float_Text_IO; use Ada.Long_Float_Text_IO;
with Ada.Integer_Text_IO;    use Ada.Integer_Text_IO;
with Interfaces.C;           use Interfaces.C;
with Interfaces.C.Strings;   use Interfaces.C.Strings;
with Ada.Strings.Unbounded;  use Ada.Strings.Unbounded;
with Ada.Command_Line;       use Ada.Command_Line;
with Ada.Numerics.Elementary_Functions ; use Ada.Numerics.Elementary_Functions ;
with Interfaces.C_Streams ;

with GNAT.Source_Info; use GNAT.Source_Info;
with gsl ;
with gsl.histogram ;
with gsl.histogram2d ;
with gsl.rng ;

procedure Histogram is
   verbose : boolean := True ;
   myname : String := gnat.Source_Info.enclosing_entity ;

   procedure RandomNumbers( n : Integer ; low, high : double ) is
      def : access constant gsl.rng.gsl_rng_type := gsl.rng.default ;
      rng : access gsl.rng.gsl_rng := gsl.rng.alloc(def) ;
      rn : double ;
   begin
      rng := gsl.rng.alloc(gsl.rng.env_setup);
      Put(Interfaces.C.Strings.Value( gsl.rng.name(rng) ));
      New_Line;
      for i in 1..n
      loop
         rn := gsl.rng.uniform(rng) * (high - low );
      end loop ;
      gsl.rng.free(rng);
   end RandomNumbers ;

   procedure T1 is
      myname : String := gnat.Source_Info.enclosing_entity ;
      logfilename : aliased String := myname & ".csv" & ASCII.NUL ;
      mode : aliased String := "w" & ASCII.NUL ;

      nbins : size_t := 16 ;
      nvals : Integer := 32 ;
      from : double := 0.0 ;
      to : double := 100.0 ;
      h : access gsl.histogram.gsl_histogram ;
      def : access constant gsl.rng.gsl_rng_type := gsl.rng.default ;
      rng : access gsl.rng.gsl_rng ; -- := gsl.rng.alloc(def) ;
      status : Int ;
      logstr : aliased Interfaces.C_Streams.FILEs :=
                        Interfaces.C_Streams.fopen(logfilename'Address , mode'Address  ) ;
   begin
      if verbose
      then
         Put_Line(myname);
      end if;
      rng := gsl.rng.alloc(gsl.rng.env_setup);

      h := gsl.histogram.alloc (nbins);
      status := gsl.histogram.set_ranges_uniform(h , from , to );
      for i in 1..nvals
      loop
         status := gsl.histogram.increment(h,gsl.rng.uniform(rng) * (to - from ));
      end loop ;

      status := gsl.histogram.fprintf (logstr , h , New_String("%g ; "), New_String("%g ; ")) ;
      gsl.histogram.free (h);
      gsl.rng.free(rng);
      status := Int(Interfaces.C_Streams.fclose(logstr));
   end T1 ;

   procedure T2 is
      myname : String := gnat.Source_Info.enclosing_entity ;
      logfilename : aliased String := myname & ".csv" & ASCII.NUL ;
      mode : aliased String := "w" & ASCII.NUL ;

      nbins : Integer := 32 ;
      nvals : Integer := 1024 ;

      from : double := 0.0 ;
      to : double := 100.0 ;

      h : access gsl.histogram2d.gsl_histogram2d ;
      def : access constant gsl.rng.gsl_rng_type := gsl.rng.default ;
      rng : access gsl.rng.gsl_rng ; -- := gsl.rng.alloc(def) ;
      status : Int ;
      logstr : aliased Interfaces.C_Streams.FILEs := 
                        Interfaces.C_Streams.fopen(logfilename'Address , mode'Address  ) ;
      x, y : double ;
   begin

      h := gsl.histogram2d.calloc(size_t(nbins),size_t(nbins)) ;
      status := gsl.histogram2d.set_ranges_uniform(h , from , to , from , to );

      rng := gsl.rng.alloc(gsl.rng.env_setup);

      for r in 1..nvals
      loop
         for c in 1..nvals
         loop
            x := gsl.rng.uniform(rng) * (to - from );
            y := gsl.rng.uniform(rng) * (to - from );
            status := gsl.histogram2d.increment(h,x,y);
         end loop ;
      end loop ;
      status := gsl.histogram2d.fprintf(logstr,h,New_String("%g ; ") , New_String("%g ; "));
      gsl.histogram2d.free(h) ;
      status := Int(Interfaces.C_Streams.fclose(logstr));
   end T2 ;

begin
   if verbose
   then
      Put_Line(myname);
   end if;
   T1 ;
   T2 ;
end Histogram;
