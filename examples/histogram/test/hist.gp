set term png size 1000,1000
set datafile separator ";"
set output output2d
set title "2d histogram"
splot t2logfile using 2:4:5 
set output output
set title "Histograms"
set title "1 Dimensional histogram"
set boxwidth 3
set style fill solid
set yrange [0:100]
plot t1logfile using 1:3:xtic(2) with boxes
set datafile separator ";"