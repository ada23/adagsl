#!/bin/bash
rm *.txt
rm *.png
export GSL_RNG_SEED=123
export GSL_RNG_TYPE=mrg
../../bin/randist 5.0
gnuplot -e "filename='mrg.txt'; output='mrg.png" plot.gp
export GSL_RNG_TYPE=knuthran
../../bin/randist 4.0
gnuplot -e "filename='knuthran.txt'; output='knuthran.png'" plot.gp
gnuplot -e "filename='knuthran.pdf.txt'; output='knuthran.pdf.png'" plot_pdf.gp
#
../../bin/bigauss
gnuplot plot_bigauss_pdf.gp 
../../bin/gamma
gnuplot -e "filename1='gamma.a1.txt' ; filename2='gamma.a2.txt' ; filename3='gamma.a3.txt' ;output='gamma.pdf.png'" plot_gamma.gp
