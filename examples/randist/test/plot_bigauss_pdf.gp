set dgrid3d 64 64
#set pm3d
set term png
set datafile separator ";"
set output "knuthran.bi.pdf.png"
set contour both
set hidden3d
splot "knuthran.bi.pdf.txt" u 1:2:3 with lines title "Bivariate Gaussian pdf"