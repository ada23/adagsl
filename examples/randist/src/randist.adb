with Ada.Text_Io; use Ada.Text_IO ;
with Ada.Long_Float_Text_IO ; use Ada.Long_Float_Text_IO ;
with Ada.Command_Line; use Ada.Command_Line;

with Interfaces.C; use Interfaces.C ;
with Interfaces.C.Strings; use Interfaces.C.Strings ;
with gsl ;
with gsl.rng ;
with gsl.randist ;

procedure randist is

   rng : access gsl.rng.gsl_rng ; 
   sigma : double := 3.0 ;

   rn : double ;
   outfile : File_Type ;

   procedure Create(fn : String ) is
   begin
      Create(outfile, Out_File, fn );
      Set_Output(outfile);
   end Create ;
   procedure Close is
   begin
      Close(outfile);
      Set_Output(Standard_Output);
   end Close;

   procedure TestGaussian is
   begin 
      Put(Interfaces.C.Strings.Value( gsl.rng.name(rng) ));
      New_Line;
      Create (Value(gsl.rng.name(rng)) & ".txt");
      for i in 1..1024
      loop
         rn := gsl.randist.gaussian(rng,sigma);
         Put(Long_Float(rn));
         New_Line;
      end loop ;
      Close ;
   end TestGaussian ;

   procedure TestGaussianPdf is
      x : double := -6.0 ;
      xdelta : double := 2.0 * abs(x) / 1024.0 ;
      pd : double ;
   begin 
      Put(Interfaces.C.Strings.Value( gsl.rng.name(rng) ));
      New_Line;
      Create (Value(gsl.rng.name(rng)) & ".pdf.txt");
      for i in 1..1024
      loop
         pd := gsl.randist.gaussian_pdf(x,sigma);
         Put(Long_Float(x)); Put(";");
         Put(Long_Float(pd));
         New_Line;
         x := x + xdelta ;
      end loop ;
      Close ;
   end TestGaussianPdf ;

begin
   Put_Line("Default Random Number Generator ");
   Put_Line("Setup from environment");
   rng := gsl.rng.alloc(gsl.rng.env_setup);
   if Argument_Count > 0
   then
      sigma := double'Value(Argument(1));
   end if;
   TestGaussian;
   TestGaussianPdf;
   gsl.rng.free(rng);
end randist ;

