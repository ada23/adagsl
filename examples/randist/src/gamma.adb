with Ada.Text_Io; use Ada.Text_IO ;
with Ada.Long_Float_Text_IO ; use Ada.Long_Float_Text_IO ;
with Ada.Command_Line; use Ada.Command_Line;

with Interfaces.C; use Interfaces.C ;
with Interfaces.C.Strings; use Interfaces.C.Strings ;
with gsl ;
with gsl.rng ;
with gsl.randist ;

procedure gamma is

   --rng : access gsl.rng.gsl_rng ; 

   a1 : double := 1.0 ;
   a2 : double := 2.0 ;
   a3 : double := 3.0 ;

   outfile : File_Type ;

   procedure Create(fn : String ) is
   begin
      Create(outfile, Out_File, fn );
      Set_Output(outfile);
   end Create ;
   procedure Close is
   begin
      Close(outfile);
      Set_Output(Standard_Output);
   end Close;

   procedure TestGammaPdf (a : double ; name : String) is
      x : double := 0.0 ;
      x_delta : double := 5.0 / 1024.0 ;
      density : double ;
   begin 
      -- Put(Interfaces.C.Strings.Value( gsl.rng.name(rng) ));
      -- New_Line;
      Create ( "gamma." & name & ".txt");
      for i in 1..1024
      loop
         density := gsl.randist.gamma_pdf(x , a , 1.0 );
         Put(Long_Float(x)); Put(" ; "); Put(Long_Float(density)); 
         New_Line;
         x := x + x_delta ;
      end loop ;
      Close ;
   end TestGammaPdf ;

begin
   --Put_Line("Default Random Number Generator ");
   --Put_Line("Setup from environment");
   --rng := gsl.rng.alloc(gsl.rng.env_setup);
   if Argument_Count > 0
   then
      a1 := double'Value(Argument(1));
      if Argument_Count > 1
      then
         a2 := double'Value(Argument(2));
         if Argument_Count > 2
         then
            a3 := double'Value(Argument(3));
         end if ;
      end if ;
   end if;
    TestGammaPdf(a1,"a1");
    TestGammaPdf(a2,"a2");
    TestGammaPdf(a3,"a3");
   --gsl.rng.free(rng);
end gamma ;

