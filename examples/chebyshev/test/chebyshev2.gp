set term png size 1000,500
set datafile separator ";"
set output output
set multiplot layout 2,1

set title "Square wave and Chebyshev approximation" 
plot datafile using 1:2 with lines title "square wave" , \
     datafile using 1:3 with points title "order 10", \
     datafile using 1:4 with points title "order 100" pt 6 lc "red" 

set key off
set title "Chebyshev approximation error" 
plot datafile using 1:5 with points title "order 10 err" pt 8 lc "blue" , \
     datafile using 1:6 with points title "order 100 err" pt 8 lc "black" 

