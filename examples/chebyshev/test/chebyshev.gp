set term png size 1000,500
set datafile separator ";"
set output output

set title "Step function and Chebyshev approximation" 
plot datafile using 1:2 with lines title "step function" , \
     datafile using 1:3 with points title "order 10", \
     datafile using 1:4 with points title "order 40" pt 6 lc "red"
