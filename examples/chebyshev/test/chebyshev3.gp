set term png size 1000,1000
set datafile separator ";"
set output output
set multiplot layout 3,1

set title "Chebyshev Raw values" 
plot datafile using 1:2 with lines title "data" , \
     datafile using 1:3 with points title "approximation" 

set title "Chebyshev Integral" 
plot datafile using 1:5 with lines title "Integral"

set title "Chebyshev Derivative" 
set yrange [-200:200]
plot datafile using 1:4 with lines title "Derivative" 
