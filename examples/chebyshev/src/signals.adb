
package body signals is
   function step( x : double ; params : System.Address ) return double is
   begin
        if x <= 0.5
        then
            return 0.0 ;
        else
            return 1.0 ;
        end if ;
    end step ;
   function square( x : double ; params : System.Address ) return double is
   begin
        if x <= 0.25 
        then
            return 1.0 ;
        elsif x <= 0.5
        then
            return -1.0 ;
        elsif x <= 0.75
        then
            return 1.0 ;
        else 
            return -1.0 ;
        end if ;
   end square ;
end signals ;