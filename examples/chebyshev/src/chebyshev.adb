with System ;
with Ada.Text_IO;            use Ada.Text_IO;
with Ada.Text_IO.C_Streams;
with Ada.Long_Float_Text_IO; use Ada.Long_Float_Text_IO;
with Ada.Integer_Text_IO;    use Ada.Integer_Text_IO;
with Interfaces.C;           use Interfaces.C;
with Interfaces.C.Strings;   use Interfaces.C.Strings;
with Ada.Strings.Unbounded;  use Ada.Strings.Unbounded;
with Ada.Command_Line;       use Ada.Command_Line;
with Ada.Numerics.Elementary_Functions ; use Ada.Numerics.Elementary_Functions ;

with GNAT.Source_Info; use GNAT.Source_Info;

with gsl ;
with gsl.version ;
with gsl.math ;
with gsl.chebyshev ;

with signals ;

procedure Chebyshev is
   use gsl.double_text_Io ;
   Verbose : boolean := True ;
   Status : Int ;

   procedure Test1 is
   -- 32.6 Examples - Step Function
      myname : String := gnat.Source_Info.enclosing_entity ;
      logfile : File_Type ;
      N : constant := 1000 ;
      order : constant := 40 ;
      ch : access gsl.chebyshev.gsl_cheb_series 
            := gsl.chebyshev.alloc(order);
      fn : aliased gsl.math.gsl_function ;
      r10, r40 : double ;
      x : double ;
   begin
      if Verbose
      then
         Put_Line(myname);
      end if ;
      fn.c_function := signals.step'access ;
      fn.params := System.Null_Address ;
      Status := gsl.chebyshev.init(ch,fn'access,0.0,1.0);
      Create(logfile,Out_File,myname & ".csv");
      Set_Output(logfile);
      for i in 1..N
      loop
         x := double(i-1)/double(N);
         r10 := gsl.chebyshev.eval_n(ch,10,x);
         r40 := gsl.chebyshev.eval(ch,x);
         Put( x ); Put(" ; ");
         Put( signals.step(x,System.Null_Address) ); Put(" ; ");
         Put( r10 ); Put(" ; ");
         Put( r40 ); Put(" ; ");
         New_Line ;
      end loop ;
      Set_Output(Standard_Output);
      Close(logfile);

      gsl.chebyshev.free(ch);
   end Test1 ;

   procedure Test2 is
   -- Extend. Square Wave + other functions.
      myname : String := gnat.Source_Info.enclosing_entity ;
      logfile : File_Type ;
      N : constant := 1000 ;
      order : constant := 100 ;
      ch : access gsl.chebyshev.gsl_cheb_series 
            := gsl.chebyshev.alloc(order);
      fn : aliased gsl.math.gsl_function ;
      r10, r100 : aliased double ;
      r10_err, r100_err : aliased double ;
      x : double ;
   begin
      if Verbose
      then
         Put_Line(myname);
      end if ;
      fn.c_function := signals.square'access ;
      fn.params := System.Null_Address ;
      Status := gsl.chebyshev.init(ch,fn'access,0.0,1.0);
      Create(logfile,Out_File,myname & ".csv");
      Set_Output(logfile);
      for i in 1..N
      loop
         x := double(i-1)/double(N);
         Status := gsl.chebyshev.eval_n_err(ch,10,x,r10'Access, r10_err'access);
         Status := gsl.chebyshev.eval_err(ch,x,r100'Access,r100_err'Access);
         Put( x ); Put(" ; ");
         Put( signals.square(x,System.Null_Address) ); Put(" ; ");
         Put( r10 ); Put(" ; ");
         Put( r100 ); Put(" ; ");
         Put( r10_err ); Put(" ; ");
         Put( r100_err ); Put(" ; ");
         New_Line ;
      end loop ;
      Set_Output(Standard_Output);
      Close(logfile);

      gsl.chebyshev.free(ch);
   end Test2 ;

   procedure Test3 is
   -- Extend. Square Wave 
   --    Derivative and Integrals
      myname : String := gnat.Source_Info.enclosing_entity ;
      logfile : File_Type ;
      N : constant := 1000 ;
      order : constant := 100 ;
      ch : access gsl.chebyshev.gsl_cheb_series 
            := gsl.chebyshev.alloc(order);
      chderiv : access gsl.chebyshev.gsl_cheb_series 
         := gsl.chebyshev.alloc(order);
      chint : access gsl.chebyshev.gsl_cheb_series 
         := gsl.chebyshev.alloc(order);
      fn : aliased gsl.math.gsl_function ;
      r10, r100 : aliased double ;
      x : double ;
   begin
      if Verbose
      then
         Put_Line(myname);
      end if ;
      fn.c_function := signals.square'access ;
      fn.params := System.Null_Address ;
      Status := gsl.chebyshev.init(ch,fn'access,0.0,1.0);
      Status := gsl.chebyshev.calc_deriv(chderiv,ch);
      Status := gsl.chebyshev.calc_integ(chint,ch);

      Create(logfile,Out_File,myname & ".csv");
      Set_Output(logfile);
      for i in 1..N
      loop
         x := double(i-1)/double(N);
         r100 := gsl.chebyshev.eval(ch,x);
         Put(x) ; Put(" ; ");
         Put(signals.square(x,System.Null_Address)); Put(" ; ");
         Put(r100); Put(" ; ");
         r100 := gsl.chebyshev.eval(chderiv,x);
         Put(r100); Put(" ; ");
         r100 := gsl.chebyshev.eval(chint,x);
         Put(r100); Put(" ; ");
         New_Line;
      end loop ;
      Set_Output(Standard_Output);
      Close(logfile);

      gsl.chebyshev.free(ch);
   end Test3 ;

begin
   if Verbose
   then
      Put_Line(gnat.Source_Info.File);
      gsl.version.Report ;
   end if;
   Test1 ;
   Test2 ;
   Test3 ;
end Chebyshev;
