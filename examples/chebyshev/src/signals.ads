with System ;
with Interfaces.C ; use Interfaces.C ;

package signals is
   function step( x : double ; params : System.Address ) return double;
   Pragma Convention(C,step);
   function square( x : double ; params : System.Address ) return double;
   Pragma Convention(C,square);
end signals ;
