set term png size 1000,500
set datafile separator ";"
set output output
set key off
set grid
set multiplot layout 2,2

set title "Cubic bspline " 
plot cbspline using 2:3 with lines , \
     cbspline using 2:4 with lines , \
     cbspline using 2:5 with lines , \
     cbspline using 2:6 with lines , \
     cbspline using 2:7 with lines , \
     cbspline using 2:8 with lines , \
     cbspline using 2:9 with lines , \
     knotsfile using 2:(0.0) with points pointtype "o" ps 2

set title "Cubic 1st derivative " 
plot cbsplined1 using 2:3 with lines , \
     cbsplined1 using 2:4 with lines , \
     cbsplined1 using 2:5 with lines , \
     cbsplined1 using 2:6 with lines , \
     cbsplined1 using 2:7 with lines , \
     cbsplined1 using 2:8 with lines , \
     cbsplined1 using 2:9 with lines , \
     knotsfile using 2:(0.0) with points pointtype "o" ps 2



set title "Cubic 2nd derivative " 
plot cbsplined2 using 2:3 with lines , \
     cbsplined2 using 2:4 with lines , \
     cbsplined2 using 2:5 with lines , \
     cbsplined2 using 2:6 with lines , \
     cbsplined2 using 2:7 with lines , \
     cbsplined2 using 2:8 with lines , \
     cbsplined2 using 2:9 with lines , \
     knotsfile using 2:(0.0) with points pointtype "o" ps 2



set title "Cubic 3rd derivative " 
plot cbsplined3 using 2:3 with lines , \
     cbsplined3 using 2:4 with lines , \
     cbsplined3 using 2:5 with lines , \
     cbsplined3 using 2:6 with lines , \
     cbsplined3 using 2:7 with lines , \
     cbsplined3 using 2:8 with lines , \
     cbsplined3 using 2:9 with lines , \
     knotsfile using 2:(0.0) with points pointtype "o" ps 2
