set term png size 1000,500
set datafile separator ";"
set output output
set key off
set grid
set multiplot layout 2,2

set title "linear" 
plot spflinear using 1:2 with lines , \
     spflinear using 1:3 with lines , \
     spflinear using 1:4 with lines , \
     spflinear using 1:5 with lines , \
     spflinear using 1:6 with lines , \
     spflinear using 1:7 with lines , \
     spflinear using 1:8 with lines , \
     spflinear using 1:9 with lines , \
     spflinear using 1:10 with lines , \
     spflinear using 1:11 with lines , \
     spflinear using 1:12 with lines , \
     knotsfile using 2:(0.0) with points pointtype 5 ps 2

set title "cubic" 
plot spfcubic using 1:2 with lines , \
     spfcubic using 1:3 with lines , \
     spfcubic using 1:4 with lines , \
     spfcubic using 1:5 with lines , \
     spfcubic using 1:6 with lines , \
     spfcubic using 1:7 with lines , \
     spfcubic using 1:8 with lines , \
     spfcubic using 1:9 with lines , \
     spfcubic using 1:10 with lines , \
     spfcubic using 1:11 with lines , \
     spfcubic using 1:12 with lines , \
     knotsfile using 2:(0.0) with points pointtype 5 ps 2  

set title "quad"
plot spfquad using 1:2 with lines , \
     spfquad using 1:3 with lines , \
     spfquad using 1:4 with lines , \
     spfquad using 1:5 with lines , \
     spfquad using 1:6 with lines , \
     spfquad using 1:7 with lines , \
     spfquad using 1:8 with lines , \
     spfquad using 1:9 with lines , \
     spfquad using 1:10 with lines , \
     spfquad using 1:11 with lines , \
     spfquad using 1:12 with lines , \
     knotsfile using 2:(0.0) with points pointtype 5 ps 2   

set title "quartic"
plot spfquartic using 1:2 with lines , \
     spfquartic using 1:3 with lines , \
     spfquartic using 1:4 with lines , \
     spfquartic using 1:5 with lines , \
     spfquartic using 1:6 with lines , \
     spfquartic using 1:7 with lines , \
     spfquartic using 1:8 with lines , \
     spfquartic using 1:9 with lines , \
     spfquartic using 1:10 with lines , \
     spfquartic using 1:11 with lines , \
     spfquartic using 1:12 with lines , \
     knotsfile using 2:(0.0) with points pointtype 5 ps 2   