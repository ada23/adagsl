#!/bin/bash
rm *.csv
../../bin/bspline
gnuplot -e "knotsfile='bspline.Test1.linear.knots.csv' ; output='bspline.png' ; spflinear='bspline.Test1.linear.spline.csv' ; spfcubic='bspline.Test1.cubic.spline.csv' ; spfquad='bspline.Test1.quad.spline.csv' ; spfquartic='bspline.Test1.quartic.spline.csv' ;" bspline.gp
gnuplot -e "knotsfile='bspline.Test2.knots.csv' ; output='bspline2.png' ; cbspline='bspline.Test2.0.csv' ; cbsplined1='bspline.Test2.1.csv' ; cbsplined2='bspline.Test2.2.csv' ; cbsplined3='bspline.Test2.3.csv' ;" bspline_deriv.gp
