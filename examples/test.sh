#!/bin/bash

cd chebyshev/test ; pwd ;  ./test.sh ; cd ../..
cd complex/test ; pwd ; ./test.sh ; cd ../..         
cd fit/test ; pwd ; ./test.sh ; cd ../..             

#pushd movstat/test ; pwd ; ./test.sh ; popd         
#pushd randist/test ; pwd ; ./test.sh ; popd         
#pushd statistics/test ; pwd ; ./test.sh ; popd
#pushd bspline/test ; pwd ; ./test.sh ; popd
#pushd clarks/test ; pwd ; ./test.sh ; popd          
#pushd deriv/test ; pwd ; ./test.sh ; popd           
#pushd fft/test ; pwd ; ./test.sh ; popd             
#pushd integration/test ; pwd ; ./test.sh ; popd     
#pushd permutation/test ; pwd ; ./test.sh ; popd     
#pushd rng/test ; pwd ; ./test.sh ; popd           
#pushd combination/test ; pwd ; ./test.sh ; popd     
#pushd filter/test ; pwd ; ./test.sh ; popd          
#pushd interp/test ; pwd ; ./test.sh ; popd          
#pushd qrng/test ; pwd ; ./test.sh ; popd            
#pushd sf/test ; pwd ; ./test.sh ; popd              
#cd vector/test ; pwd ; ./test.sh ; cd ../..