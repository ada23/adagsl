with Ada.Text_Io; use Ada.Text_IO ;
with Ada.Long_Float_Text_IO ; use Ada.Long_Float_Text_IO ;
with Interfaces.C; use Interfaces.C ;
with Interfaces.C.Strings;
with gsl ;
with gsl.rng ;

procedure Rng is
   def : access constant gsl.rng.gsl_rng_type := gsl.rng.default ;
   rng : access gsl.rng.gsl_rng := gsl.rng.alloc(def) ;
   rn : double ;
   procedure Test is
      outfile : File_Type ;
   begin 
      Put(Interfaces.C.Strings.Value( gsl.rng.name(rng) ));
      New_Line;
      Create(outfile,Out_File,Interfaces.C.Strings.Value(gsl.rng.name(rng)) & ".txt");
      Set_Output(outfile);
      for i in 1..1024
      loop
         rn := gsl.rng.uniform(rng);
         Put(Long_Float(rn));
         New_Line;
      end loop ;
      Close(outfile);
      Set_Output(Standard_Output);
   end Test ;
begin
   Put_Line("Default Random Number Generator ");
   Test ;
   Put_Line("Setup from environment");
   rng := gsl.rng.alloc(gsl.rng.env_setup);
   Test;
   gsl.rng.free(rng);
end Rng;
