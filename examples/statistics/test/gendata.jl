using OnlineStats
r=rand(Float64,64)
println(r)
println("Maximum ",maximum(r))
println("Minimum ",minimum(r))
println("Mean ",mean(r))
println("Variance ",var(r))
println("Skewness ",skewness(r))
println("Kurtosis ",kurtosis(r))
