with Ada.Text_IO;            use Ada.Text_IO;
with Ada.Text_IO.C_Streams;
with Ada.Long_Float_Text_IO; use Ada.Long_Float_Text_IO;
with Ada.Integer_Text_IO;    use Ada.Integer_Text_IO;
with Interfaces.C;           use Interfaces.C;
with Interfaces.C.Strings;   use Interfaces.C.Strings;
with Ada.Strings.Unbounded;  use Ada.Strings.Unbounded;
with Ada.Command_Line;       use Ada.Command_Line;

with GNAT.Source_Info; use GNAT.Source_Info;

with gsl;
with gsl.rng;
with gsl.randist;
with gsl.permutation;

procedure permutation is

   letters : Unbounded_String := To_Unbounded_String ("0123456789abcdef");
   numbers : Unbounded_String := To_Unbounded_String("0123") ;

   Status  : int;
   procedure show (p : access gsl.permutation.gsl_permutation) is

   begin
      Status :=
        gsl.permutation.fprintf
          (Ada.Text_Io.C_Streams.C_Stream (Ada.Text_Io.Standard_Output), p,
           New_String (" %u "));
      New_Line;
   end show;

   procedure Test1 is
      perm : access gsl.permutation
        .gsl_permutation := -- gsl.permutation.alloc(20) ;
        gsl.permutation.alloc
          (size_t (Ada.Strings.Unbounded.Length (letters)));
      perminv : access gsl.permutation.gsl_permutation :=
        gsl.permutation.alloc (size_t (Length (letters)));
      rng : access gsl.rng.gsl_rng := gsl.rng.alloc (gsl.rng.default);
   begin
      Put_Line (GNAT.Source_Info.Enclosing_Entity);
      gsl.permutation.init (perm);
      show (perm);
      gsl.randist.shuffle
        (rng, perm.data, size_t (Length (letters) + 1), size_t (8));
      Put_Line ("Shuffled");
      Status := gsl.permutation.show (perm);

      declare
         s : String := To_String (letters);
         n : size_t;
      begin
         for sp in s'Range loop
            n := gsl.permutation.get (perm, size_t (sp - 1));
            Put (s (1 + Integer (n)));
         end loop;
         New_Line;
      end;

      Status := gsl.permutation.inverse (perminv, perm);
      Put_Line ("Inverted");
      Status := gsl.permutation.show (perminv);

   end Test1;
   procedure Test2 is
      perm : access gsl.permutation.gsl_permutation
        := gsl.permutation.alloc
          (size_t (Ada.Strings.Unbounded.Length (numbers)));
      Status : Int := gsl.GSL_SUCCESS ;
   begin
      gsl.permutation.init (perm);
      loop
         Status := gsl.permutation.show(perm);
         Status := gsl.permutation.next(perm);
         exit when Status /= gsl.GSL_SUCCESS ;  
      end loop ;
   end Test2;
begin
   if Argument_Count > 0 then
      letters := To_Unbounded_String (Argument (1));
   end if;
   Test1;
   Test2;
end permutation;
