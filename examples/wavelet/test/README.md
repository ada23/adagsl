# Data files

source: [Physionet](https://physionet.org/content/autonomic-aging-cardiovascular/1.0.0/)
tools: [WFDB](https://github.com/bemoody/wfdb)

## Conversion 
```
rdsamp -c -P -v -r 0100 > 0100.csv
```