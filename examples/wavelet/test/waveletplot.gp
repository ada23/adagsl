set term png size 1000,500
set output "waveletplot.png"
set key right bottom
set title "Wavelet transform ECG1" 
plot "Wavelet.T1.csv" using 1:2 title "original" with lines , \
     "Wavelet.T1.csv" using 1:3 title "waveletgen" with lines