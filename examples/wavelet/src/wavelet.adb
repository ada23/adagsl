with Ada.Command_Line; use Ada.Command_Line;
with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io;
with Ada.Float_Text_Io; use Ada.Float_Text_Io;
with GNAT.Source_Info; use GNAT.Source_Info;
with Interfaces.C ; use Interfaces.C;

with csv ;

with gsl ;
with gsl.wavelet ;
with gsl.vector_double ;
with gsl.vector_int ;
with gsl.sort_double ;

procedure Wavelet is
   verbose : boolean := true ;
   procedure T1 is
      myname : constant String := gnat.Source_Info.enclosing_entity ;
      fn : constant String := Argument(2) ; 
      f : csv.File_Type ;
      seglen : size_t := 256 ;
      components : size_t := 48 ;
      offset : size_t := seglen ;
      w : access gsl.wavelet.gsl_wavelet ;
      ws : access gsl.wavelet.gsl_wavelet_workspace  ;
      OutFile : Ada.Text_Io.File_Type ;
      ecg1_orig, ecg1_copy : access gsl.vector_double.gsl_vector ;
      ecg1_abscoeff : access gsl.vector_double.gsl_vector ;
      p : access gsl.vector_int.gsl_vector ;

      ecg1 : double := 0.0 ;
      Status : Int ;

   begin
      csv.Debug := verbose ;
      if verbose
      then 
         Put_Line(myname);
      end if ;
      f := csv.Open( fn , "," , true );
      if verbose
      then
         Put("No of fields ="); Put(csv.No_Columns(f)); New_Line;
         for i in 1..csv.No_Columns(f)
         loop
            Put_Line(csv.Field_Name(f,i));
         end loop ;
         csv.Get_Line(f);
         for i in 1..csv.No_Columns(f)
         loop
            Put(csv.Field_Name(f,i)); Put(" units "); Put_Line(csv.Field(f,i));
         end loop ;
      end if;
      for sk in 1..offset
      loop
         csv.Get_Line(f);
      end loop ;
      -- family: daubechies , number: 4
      w := gsl.wavelet.alloc(gsl.wavelet.gsl_wavelet_daubechies,4);
      ws := gsl.wavelet.workspace_alloc(seglen) ;
      ecg1_orig := gsl.vector_double.alloc(seglen);
      ecg1_copy := gsl.vector_double.alloc(seglen);
      ecg1_abscoeff := gsl.vector_double.alloc(seglen);
      p := gsl.vector_int.alloc(seglen);

      for sample in 1..seglen
      loop
         csv.Get_Line(f) ;
         ecg1 := double'Value( csv.Field(f,2) );
         gsl.vector_double.set( ecg1_orig , size_t(sample-1) , ecg1 );
         gsl.vector_double.set( ecg1_copy , size_t(sample-1) , ecg1 );
      end loop ;
      Status := gsl.wavelet.transform_forward( w , ecg1_copy , 1 , seglen , ws );
      Put_Line("Forward Transform done");

      for sample in 1..seglen
      loop
        ecg1 := gsl.vector_double.get( ecg1_copy , sample - 1 );
        gsl.vector_double.set( ecg1_abscoeff , sample - 1 , ecg1 ) ;
      end loop ;
      Put_Line("Created abscoeef");

      gsl.sort_double.index( p , ecg1_abscoeff , 1 , seglen );

      for i in 1..size_t(seglen-components)
      loop
         gsl.vector_double.set( ecg1_copy , size_t(gsl.vector_int.get(p,i-1)) , 0.0 );
      end loop ;

      Status := gsl.wavelet.transform_inverse (w , ecg1_copy , 1, seglen , ws );
      Put_Line("Inverse transform done");

      Create(outfile,Out_File,myname & ".csv" );
      Set_Output(outfile);
      for i in 1..seglen
      loop
         Put(Integer(i)); Put(" ");
         Put( Float( gsl.vector_double.get( ecg1_orig , i-1)) ); Put(" ");
         Put( Float( gsl.vector_double.get( ecg1_copy , i-1)) );
         New_Line ;
      end loop ;
      Close(outfile);
      gsl.wavelet.workspace_free(ws);
      gsl.wavelet.free(w);
   end T1;
begin
   if Argument(1) = "t1"
   then
      T1 ;
   end if ;
end Wavelet;
