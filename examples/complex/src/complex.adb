with Ada.Text_IO;            use Ada.Text_IO;
with Ada.Text_IO.C_Streams;
with Ada.Long_Float_Text_IO; use Ada.Long_Float_Text_IO;
with Ada.Integer_Text_IO;    use Ada.Integer_Text_IO;
with Interfaces.C;           use Interfaces.C;
with Interfaces.C.Strings;   use Interfaces.C.Strings;
with Ada.Strings.Unbounded;  use Ada.Strings.Unbounded;
with Ada.Command_Line;       use Ada.Command_Line;
with Ada.Numerics.Elementary_Functions ; use Ada.Numerics.Elementary_Functions ;
with Ada.Numerics.Generic_Complex_Types ;
with Ada.Numerics.Generic_Complex_Elementary_Functions ;
with GNAT.Source_Info; use GNAT.Source_Info;

with math ;
with gsl ;
with gsl.complex ;
with gsl.complex.math ;

procedure complex is
   use gsl.double_text_Io ;

   package AComplex is new Ada.Numerics.Generic_Complex_Types( Long_Float );
   package AC renames AComplex ;
   package ACF is new Ada.Numerics.Generic_Complex_Elementary_Functions( AComplex );

   package GSLC renames gsl.double_complex_types ;
   package GSLCF renames gsl.double_complex_functions ;
   package GSLCM renames gsl.complex.math ;
   package GSLCTIO renames gsl.complex_text_io ;

   Verbose : boolean := True ;
   procedure Sep is
   begin
      Put(" ; ");
   end Sep ;
   procedure Complex_Print( c : AComplex.Complex ) is
   begin
      Put( AC.Re(c) ); Sep ;
      Put( AC.Im(c) ); Sep ;
   end Complex_Print ;
   procedure Complex_Print( c : GSLC.Complex ) is
   begin
      Put( GSLC.Re(c) ); Sep ;
      Put( GSLC.Im(c) ); Sep ;      
   end Complex_Print ;

   procedure Complex_Print( c : gsl.complex.gsl_complex ) is
   begin
      Put( gsl.complex.Re(c) ); Sep ;
      Put( gsl.complex.Im(c) ); Sep ;  
      Put( gsl.complex.math.cabs(c)); Sep ;
      Put( gsl.complex.math.arg(c)); Sep;    
   end Complex_Print ;

   procedure T1 is
      myname : String := gnat.Source_Info.enclosing_entity ;
      logfile : File_Type ;
      a, adelta, b : double ;
   begin
      if Verbose
      then
         Put_Line(myname);
      end if ;
      Create(logfile,Out_File,myname & ".csv");
      Set_Output(logfile);
      adelta := 0.1 ;
      a := 0.0 ; 
      b := 1.0 ;
      for i in 1..10
      loop
         for j in 1..10
         loop
            b := b + adelta ;

            declare
               adac : AC.Complex
                     := AC.Compose_From_Cartesian(Long_Float(a),Long_Float(b));
               cc : GSLC.Complex
                     := GSLC.Compose_From_Cartesian(a,b) ;
               cgslc : gsl.complex.gsl_complex
                     := GSLCM.rect(a,b);
               prod : GSLC.Complex
                        := GSLC."*"(cc , cc) ;
               prod2 : gsl.complex.gsl_complex
                        := gsl.complex.math.mul(cgslc,cgslc);
            begin
               Put(a) ; Put(" ; ");
               Put(b) ; Put(" ; ");
               Put(AC."abs"(adac)); Put(" ; ") ;
               Put(AC.argument(adac)); Sep ;
               Put(GSLC."abs"(cc)); Put(" ; ");
               Put(GSLC.argument(cc)); Sep ;
               GSLCTIO.Put(cc); Sep ;
               Put(GSLCM.cabs(cgslc)); Sep ;
               Put(GSLCM.arg(cgslc)); Sep ;
               GSLCTIO.Put(prod); Sep ;
               Put( prod2.Re ) ; Sep ;
               Put( prod2.Im ) ; Sep ;
               New_Line;

            end ;
         end loop ;
         a := a + adelta ;
      end loop ;

      Set_Output(Standard_Output);
      Close(logfile);
   end T1 ;

   procedure T2 is
      myname : String := gnat.Source_Info.enclosing_entity ;
      logfile : File_Type ;
      N : Constant := 100 ;
      x, y : Long_Float := 0.0 ;
      del : Long_Float ; -- := 1.0/Long_Float(N);
      a,b : AComplex.Complex ;
      LineNo : Integer := 0;
   begin
      if Verbose
      then
         Put_Line(myname);
      end if ;
      Create(logfile,Out_File,myname & ".csv");
      Set_Output(logfile);
      del := Ada.Numerics.Pi * 2.0 / Long_Float(N) ;
      x := 0.0 ;
      AComplex.Set_Re(a,0.0);
      for i in 1..N
      loop
         AComplex.Set_Im(a,x);
         LineNo := LineNo + 1 ;
         Put( LineNo ); Sep ;
         Complex_Print(a);
         b := ACF.Exp(a);
         Complex_Print(b);
         New_Line ;
         x := x + del ;
      end loop ;
      Set_Output(Standard_Output);
      Close(logfile);
   end T2 ;


   procedure T3 is
      myname : String := gnat.Source_Info.enclosing_entity ;
      logfile : File_Type ;
      N : Constant := 100 ;
      x, y : double := 0.0 ;
      del : double ; -- := 1.0/Long_Float(N);
      a,b : gsl.complex.gsl_complex ;
      LineNo : Integer := 0;
   begin
      if Verbose
      then
         Put_Line(myname);
      end if ;
      Create(logfile,Out_File,myname & ".csv");
      Set_Output(logfile);
      del := math.M_PI * 2.0 / double(N) ;
      x := 0.0 ;

      --GSLC.Set_Re(a,math.M_PI / 4.0);
      for i in 1..N
      loop
         --GSLC.Set_Im(a,x);
         a := GSLCM.rect( math.M_PI / 2.0 , x + math.M_PI / 2.0);
         LineNo := LineNo + 1 ;
         Put( LineNo ); Sep ;
         Complex_Print(a);
         b := GSLCM.Exp(a);
         Complex_Print(b);
         New_Line ;
         x := x + del ;
      end loop ;
      Set_Output(Standard_Output);
      Close(logfile);
   end T3 ;

begin
   Ada.Long_Float_Text_Io.Default_Exp := 0;
   Ada.Long_Float_Text_Io.Default_Aft := 4 ;
   gsl.double_text_io.Default_Exp := 0;
   gsl.double_text_io.Default_Aft := 4;
   GSLCTIO.Default_Exp := 0;
   GSLCTIO.Default_Aft := 4; 
   T1 ;
   T2 ;
   T3 ;
end complex;
