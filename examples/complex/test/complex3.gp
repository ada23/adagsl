set term png size 1000,1000
set datafile separator ";"
set output output
set multiplot layout 2,2

plot filename using 1:6 title "real"  with impulse lw 2 , \
     filename using 1:6 with points pt 7 notitle

plot filename using 1:7 title "imag" with impulse lw 2  , \
     filename using 1:7 with points pt 7 notitle

plot filename using 1:8 title "abs" with impulse lw 2, \
     filename using 1:8 with points pt 7 notitle

plot filename using 1:9 title "arg" with impulse lw 2  , \
     filename using 1:9 with points pt 7 notitle

