set term png size 1000,1000
set datafile separator ";"
set output output
set multiplot layout 2,1
set title "Complex var"

plot filename using 1:4 title "real" with impulse lw 2 , \
     filename using 1:4 with points pt 7

plot filename using 1:5 title "imag" with impulse lw 2  , \
     filename using 1:5 with points pt 7
