---
title: "explore"
---

## Physiological Dataset Exploration

We can use the WFDB tools to extract the data in a convenient form for exploration.

```{bash}
export PATH=$PATH:$HOME/wfdb/bin
rdsamp -h
```

[CHARIS](https://physionet.org/content/charisdb/1.0.0/ "Dataset sample from physionet") will be used for this exploration.

Convert the data to csv file

```{bash}
pushd $HOME/Downloads
$HOME/wfdb/bin/wfdbdesc charis12
$HOME/wfdb/bin/rdsamp -c -P -t 10.0 -r charis12 > charis12.csv
```

Based on the above, we have 3 signals ABP, ECG and ICP in this dataset sampled at 50 Hz. Let us prepare the data for analysis by extracting 10 seconds worth of data from the dataset.

Load the data and explore.

```{r}
library(tidyverse)
charis12<-read.csv("charis12.csv",header=FALSE,sep=",",nrows=400)
names(charis12) <- c("ts","abp","ecg","icp")
head(charis12)

```

```{r}
library(ggplot2)
library(ggpubr)
library(htmltools)

abpplot <- ggplot(data=charis12) +
           ggtitle("Arterial Blood Pressure") +
           geom_line(aes(x=ts,y=abp,color="ABP")) +
           ylab("mmHg")

ecgplot <- ggplot(data=charis12) +
           ggtitle("Electro Cardiogram") +
           geom_line(aes(x=ts,y=ecg,color="ECG")) +
           ylab("mV")

icpplot <- ggplot(data=charis12) +
           ggtitle("Intra Cranial Pressure") +
           geom_line(aes(x=ts,y=icp,color="ICP")) +
           ylab("mmHg")

fig <- ggarrange(abpplot,icpplot,
                 labels=c("A","B"),
 #                heights=c(20,20,20),
                 ncol=1,nrow=2)
fig

fig <- ggarrange(ecgplot,labels=c("A"),ncol=1,nrow=1)
fig
```
