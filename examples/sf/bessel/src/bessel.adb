with Ada.Text_Io; use Ada.Text_IO ;
with Ada.Long_Float_Text_IO ; use Ada.Long_Float_Text_IO ;
with Interfaces.C; use Interfaces.C ;
with Interfaces.C.Strings;
with gsl ;
with gsl.sf.bessel ;

procedure bessel is
   x : double := 5.0;
   expected : double := -0.17759677131433830434739701;
   y : double := gsl.sf.bessel.JJ0 (x);
begin
   Put("J0(5.0) = "); Put(Long_Float(y) ); New_Line;
   Put("exact   = "); Put(Long_Float(expected)); New_Line;
end bessel ;
