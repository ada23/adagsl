set term png size 1000,500
set datafile separator ";"
set output output
set key top right
set grid
set title "Interpolation 2d - cspline " 
plot filename using 1:2 title "Data" with points pointsize 2, \
     interpfile using 1:2 title "Interpolation" with lines 
    