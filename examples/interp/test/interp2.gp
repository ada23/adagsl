set term png size 1000,500
set datafile separator ";"
set output output
set key top left
set grid
set title "Interpolation 2d - comparison of spline algorithms" 
plot filename using 1:2 title "Data" with points pointsize 2, \
     interpfile using 1:2 title "Cubic" with lines lc "blue" lw 2, \
     interpfile using 1:3 title "Akima" with lines lc "green" lw 2, \
     interpfile using 1:4 title "Steffen" with lines lc "red" lw 2
    