with Ada.Long_Long_Float_Text_Io; Use Ada.Long_Long_Float_Text_Io ;
with Ada.Text_Io; use Ada.Text_Io;
package body angles is

    function R( d : degrees ;
                m : minutes := 0 ;
                s : seconds := 0 ) return radians is
        ts : Long_Long_Float ;
    begin
        ts := Long_Long_Float(( d * minutes_n_degree + m ) * seconds_n_minute + s) ;
        --Put("Seconds "); Put(ts); New_Line;
        return Radians((ts / Long_Long_Float(seconds_n_cycle)) * radians_n_cycle) ;
    exception
        when others => return 0.0;
    end ;

    procedure D( r : radians ;
                 deg : access degrees ;
                 min : access minutes ;
                 sec : access seconds) is
        sectemp : radians ;
        mintemp : radians ;
        cyc : Long_Long_Float := r / radians(radians_n_cycle) ;
    begin
        deg.all := degrees( cyc * Long_Long_Float(degrees_n_cycle) ) ;
        mintemp := r - Long_Long_Float(deg.all) / degrees_n_radian ;
        min.all := minutes( mintemp * minutes_n_radian);
        sectemp := r - Long_Long_Float(deg.all) / degrees_n_radian
                     - Long_Long_Float(min.all) / minutes_n_radian ;
        sec.all := seconds(sectemp * seconds_n_radian);
    end D ;

end angles; 