with Ada.Numerics ;
package angles is

    degrees_n_cycle : constant := 360 ;
    minutes_n_degree : constant := 60 ;
    minutes_n_cycle : constant := minutes_n_degree * degrees_n_cycle ;
    seconds_n_minute : constant := 60 ;
    seconds_n_degree : constant := seconds_n_minute * minutes_n_degree ;
    seconds_n_cycle : constant := seconds_n_degree * degrees_n_cycle ;

    subtype degrees is natural range 0..degrees_n_cycle ;
    subtype minutes is natural range 0..minutes_n_degree ;
    subtype seconds is natural range 0..seconds_n_minute ;

    subtype radians is long_long_float range 0.0 .. 2.0 * Ada.Numerics.Pi ;
    radians_n_cycle : constant radians := radians'Last ;

    degrees_n_radian : constant Long_Long_Float := Long_Long_Float(degrees_n_cycle) / radians_n_cycle ;
    minutes_n_radian : constant Long_Long_Float := Long_Long_Float(minutes_n_cycle) / radians_n_cycle ;
    seconds_n_radian : constant Long_Long_Float := Long_Long_Float(seconds_n_cycle) / radians_n_cycle ;

    function R( d : degrees ;
                m : minutes := 0 ;
                s : seconds := 0 ) return radians ;
    procedure D( r : radians ;
                 deg : access degrees ;
                 min : access minutes ;
                 sec : access seconds);
end angles ;
