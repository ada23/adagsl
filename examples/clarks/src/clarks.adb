with Ada.Text_IO;            use Ada.Text_IO;
with Ada.Text_IO.C_Streams;
with Ada.Long_Float_Text_IO; use Ada.Long_Float_Text_IO;
with Ada.Integer_Text_IO;    use Ada.Integer_Text_IO;
with Interfaces.C;           use Interfaces.C;
with Interfaces.C.Strings;   use Interfaces.C.Strings;
with Ada.Strings.Unbounded;  use Ada.Strings.Unbounded;
with Ada.Command_Line;       use Ada.Command_Line;
with Ada.Numerics.Elementary_Functions ; use Ada.Numerics.Elementary_Functions ;

with GNAT.Source_Info; use GNAT.Source_Info;

with Angles ;

procedure Clarks is
   Verbose : boolean := True ;
   myname : String := gnat.Source_Info.enclosing_entity ;

   procedure T1 is
      myname : String := gnat.Source_Info.enclosing_entity ;
      logfile : File_Type ;
      procedure show_radians( d : angles.degrees ) is
      begin
         Put(Integer(d)) ; Put(" ; "); 
         Put(Long_Float(angles.R(d)));
         New_Line;
      end show_radians ;
   begin
      if Verbose
      then
         Put_Line(myname);
      end if ;
      Show_Radians(1);
      Show_Radians(90);
      show_radians(180);
      Show_Radians(360);
      --Create(logfile,Out_File,myname & ".csv");
      --Set_Output(logfile);
      --Set_Output(Standard_Output);
      --Close(logfile);
   end T1 ;

   procedure T2 is
      myname : String := gnat.Source_Info.enclosing_entity ;
      logfile : File_Type ;
      procedure show_sine( d : angles.degrees ) is
      begin
         Put(Integer(d)) ; Put(" ; "); 
         Put(Long_Float(angles.R(d))); Put(" ; ");
         Put(Long_Float(Ada.Numerics.Elementary_Functions.Sin(Float(angles.R(d)))));
         New_Line;
      end show_sine ;
   begin
      if Verbose
      then
         Put_Line(myname);
      end if ;
      Create(logfile,Out_File,myname & ".csv");
      Set_Output(logfile);
      for d in angles.degrees'Range
      loop
         Show_Sine(d);
      end loop ;
      Set_Output(Standard_Output);
      Close(logfile);
   end T2 ;

   procedure T3 is
      myname : String := gnat.Source_Info.enclosing_entity ;
      logfile : File_Type ;
      procedure Show_Sine_Line( d : angles.degrees ) is
         values : array (angles.minutes'Range) of Long_Float ;
         meandiff : array (1..5) of Long_Float := (others => 0.0);
         varg : float ;
         ptr : angles.minutes;
      begin
         for v in values'Range
         loop
            varg := Float( angles.R(d,v)) ;
            values(v) := Long_Float(Sin(varg)) ;
         end loop ;
         Put( Long_Float(d) , exp => 0 , aft => 0 ); 
         Set_Col(8) ; Put(" | ") ;
         for v in values'Range
         loop
            if v mod 6 = 0
            then
               if v /= values'Last
               then 
                  ptr := v ;
                  Put(values(v) , aft => 4 , exp => 0); Put( " ");
               end if ;
            else
               meandiff( v mod 6 ) :=
                  meandiff(v mod 6) + (values(v) - values(ptr)) ;
            end if ;
         end loop ;
         Put( " | ");
         for md in meandiff'Range
         loop
            Put(Integer(10000.0*meandiff(md)/10.0), width => 2); Put(" ");
         end loop ;
         New_Line ;
      end Show_Sine_Line; 
   begin
      if Verbose
      then
         Put_Line(myname);
      end if ;
      Create(logfile,Out_File,myname & ".txt");
      Set_Output(logfile);
      for d in angles.degrees'Range
      loop
         Show_Sine_Line( d );
      end loop ;
      Set_Output(Standard_Output);
      Close(logfile);
   end T3 ;

   procedure T4 is
      myname : String := gnat.Source_Info.enclosing_entity ;
      logfile : File_Type ;
      procedure Show_Degrees( r : angles.Radians ) is
          dd : aliased angles.degrees ;
          mm : aliased angles.minutes ;
          ss : aliased angles.seconds ;
      begin
          angles.D( r , dd'access , mm'access, ss'access);
          Put( Long_Float(r) , aft => 4 , exp => 0) ; Put(" ; ");
          Put( Integer(dd) , width => 3 ) ; Put(" ; ");
          Put( Integer(mm) , width => 3 ) ; Put(" ; ");
          Put( Integer(ss) , width => 3 ) ; Put(" ; ");
          New_Line ;
      end Show_Degrees;
   begin
      if Verbose
      then
         Put_Line(myname);
      end if ;
      Create(logfile,Out_File,myname & ".csv");
      Set_Output(logfile);
      Show_Degrees( angles.R(10,10,10) );
      Show_Degrees( angles.R(20,20,20) );
      Show_Degrees( angles.R(90,0,0) );
      Show_Degrees( angles.R(180,0,0) );
      Show_Degrees( angles.R(270,0,0) );
      Show_Degrees( angles.R(45,0,0) );
      Set_Output(Standard_Output);
      Close(logfile);
   end T4 ;

begin
   if Verbose
   then
      Put_Line(myname);
   end if ;
   T1 ;
   T2 ;
   T3 ;
   T4 ;
end Clarks;
