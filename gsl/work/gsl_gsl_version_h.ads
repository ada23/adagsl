pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings;

package gsl_gsl_version_h is

   GSL_VERSION : aliased constant String := "2.7.1" & ASCII.NUL;  --  /opt/homebrew/include/gsl/gsl_version.h:18
   GSL_MAJOR_VERSION : constant := 2;  --  /opt/homebrew/include/gsl/gsl_version.h:19
   GSL_MINOR_VERSION : constant := 7;  --  /opt/homebrew/include/gsl/gsl_version.h:20

   gsl_version : Interfaces.C.Strings.chars_ptr  -- /opt/homebrew/include/gsl/gsl_version.h:22
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_version";

end gsl_gsl_version_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
