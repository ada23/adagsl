pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package gsl_gsl_inline_h is

   --  arg-macro: function GSL_RANGE_COND (x)
   --    return x;
end gsl_gsl_inline_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
