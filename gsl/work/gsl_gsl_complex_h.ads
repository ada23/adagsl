pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package gsl_gsl_complex_h is

   --  arg-macro: procedure GSL_COMPLEX_DEFINE (R, C)
   --    typedef struct { R dat(2); } C ;
   --  arg-macro: function GSL_REAL (z)
   --    return (z).dat(0);
   --  arg-macro: function GSL_IMAG (z)
   --    return (z).dat(1);
   --  arg-macro: function GSL_COMPLEX_P (zp)
   --    return (zp).dat;
   --  arg-macro: function GSL_COMPLEX_P_REAL (zp)
   --    return (zp).dat(0);
   --  arg-macro: function GSL_COMPLEX_P_IMAG (zp)
   --    return (zp).dat(1);
   --  arg-macro: function GSL_COMPLEX_EQ (z1, z2)
   --    return ((z1).dat(0) = (z2).dat(0))  and then  ((z1).dat(1) = (z2).dat(1));
   --  arg-macro: procedure GSL_SET_COMPLEX (zp, x, y)
   --    do {(zp).dat(0):=(x); (zp).dat(1):=(y);} while(0)
   --  arg-macro: procedure GSL_SET_REAL (zp, x)
   --    do {(zp).dat(0):=(x);} while(0)
   --  arg-macro: procedure GSL_SET_IMAG (zp, y)
   --    do {(zp).dat(1):=(y);} while(0)
   --  arg-macro: procedure GSL_SET_COMPLEX_PACKED (zp, n, x, y)
   --    do {*((zp)+2*(n)):=(x); *((zp)+(2*(n)+1)):=(y);} while(0)
   type gsl_complex_packed is access all double;  -- /opt/homebrew/include/gsl/gsl_complex.h:38

   type gsl_complex_packed_float is access all float;  -- /opt/homebrew/include/gsl/gsl_complex.h:39

   type gsl_complex_packed_long_double is access all long_double;  -- /opt/homebrew/include/gsl/gsl_complex.h:40

   type gsl_const_complex_packed is access all double;  -- /opt/homebrew/include/gsl/gsl_complex.h:42

   type gsl_const_complex_packed_float is access all float;  -- /opt/homebrew/include/gsl/gsl_complex.h:43

   type gsl_const_complex_packed_long_double is access all long_double;  -- /opt/homebrew/include/gsl/gsl_complex.h:44

   type gsl_complex_packed_array is access all double;  -- /opt/homebrew/include/gsl/gsl_complex.h:48

   type gsl_complex_packed_array_float is access all float;  -- /opt/homebrew/include/gsl/gsl_complex.h:49

   type gsl_complex_packed_array_long_double is access all long_double;  -- /opt/homebrew/include/gsl/gsl_complex.h:50

   type gsl_const_complex_packed_array is access all double;  -- /opt/homebrew/include/gsl/gsl_complex.h:52

   type gsl_const_complex_packed_array_float is access all float;  -- /opt/homebrew/include/gsl/gsl_complex.h:53

   type gsl_const_complex_packed_array_long_double is access all long_double;  -- /opt/homebrew/include/gsl/gsl_complex.h:54

   type gsl_complex_packed_ptr is access all double;  -- /opt/homebrew/include/gsl/gsl_complex.h:62

   type gsl_complex_packed_float_ptr is access all float;  -- /opt/homebrew/include/gsl/gsl_complex.h:63

   type gsl_complex_packed_long_double_ptr is access all long_double;  -- /opt/homebrew/include/gsl/gsl_complex.h:64

   type gsl_const_complex_packed_ptr is access all double;  -- /opt/homebrew/include/gsl/gsl_complex.h:66

   type gsl_const_complex_packed_float_ptr is access all float;  -- /opt/homebrew/include/gsl/gsl_complex.h:67

   type gsl_const_complex_packed_long_double_ptr is access all long_double;  -- /opt/homebrew/include/gsl/gsl_complex.h:68

   type anon_array1747 is array (0 .. 1) of aliased double;
   type gsl_complex is record
      dat : aliased anon_array1747;  -- /opt/homebrew/include/gsl/gsl_complex.h:136
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/gsl/gsl_complex.h:136

   type anon_array1750 is array (0 .. 1) of aliased long_double;
   type gsl_complex_long_double is record
      dat : aliased anon_array1750;  -- /opt/homebrew/include/gsl/gsl_complex.h:137
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/gsl/gsl_complex.h:137

   type anon_array1753 is array (0 .. 1) of aliased float;
   type gsl_complex_float is record
      dat : aliased anon_array1753;  -- /opt/homebrew/include/gsl/gsl_complex.h:138
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/gsl/gsl_complex.h:138

end gsl_gsl_complex_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
