pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
limited with gsl_gsl_sf_result_h;

package gsl_gsl_sf_dawson_h is

   function gsl_sf_dawson_e (x : double; result : access gsl_gsl_sf_result_h.gsl_sf_result_struct) return int  -- /opt/homebrew/include/gsl/gsl_sf_dawson.h:46
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_sf_dawson_e";

   function gsl_sf_dawson (x : double) return double  -- /opt/homebrew/include/gsl/gsl_sf_dawson.h:47
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_sf_dawson";

end gsl_gsl_sf_dawson_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
