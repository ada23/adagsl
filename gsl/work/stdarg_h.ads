pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with System;

package stdarg_h is

   subtype uu_gnuc_va_list is System.Address;  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include/stdarg.h:40

end stdarg_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
