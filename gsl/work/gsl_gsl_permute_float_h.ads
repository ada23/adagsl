pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with sys_utypes_usize_t_h;

package gsl_gsl_permute_float_h is

   function gsl_permute_float
     (p : access sys_utypes_usize_t_h.size_t;
      data : access float;
      stride : sys_utypes_usize_t_h.size_t;
      n : sys_utypes_usize_t_h.size_t) return int  -- /opt/homebrew/include/gsl/gsl_permute_float.h:39
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_permute_float";

   function gsl_permute_float_inverse
     (p : access sys_utypes_usize_t_h.size_t;
      data : access float;
      stride : sys_utypes_usize_t_h.size_t;
      n : sys_utypes_usize_t_h.size_t) return int  -- /opt/homebrew/include/gsl/gsl_permute_float.h:40
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_permute_float_inverse";

end gsl_gsl_permute_float_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
