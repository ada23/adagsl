pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
limited with ustdio_h;
with Interfaces.C.Strings;

package gsl_gsl_ieee_utils_h is

   subtype anon_array1264 is Interfaces.C.char_array (0 .. 23);
   type gsl_ieee_float_rep is record
      sign : aliased int;  -- /opt/homebrew/include/gsl/gsl_ieee_utils.h:45
      mantissa : aliased anon_array1264;  -- /opt/homebrew/include/gsl/gsl_ieee_utils.h:46
      exponent : aliased int;  -- /opt/homebrew/include/gsl/gsl_ieee_utils.h:47
      c_type : aliased int;  -- /opt/homebrew/include/gsl/gsl_ieee_utils.h:48
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/gsl/gsl_ieee_utils.h:49

   subtype anon_array1268 is Interfaces.C.char_array (0 .. 52);
   type gsl_ieee_double_rep is record
      sign : aliased int;  -- /opt/homebrew/include/gsl/gsl_ieee_utils.h:52
      mantissa : aliased anon_array1268;  -- /opt/homebrew/include/gsl/gsl_ieee_utils.h:53
      exponent : aliased int;  -- /opt/homebrew/include/gsl/gsl_ieee_utils.h:54
      c_type : aliased int;  -- /opt/homebrew/include/gsl/gsl_ieee_utils.h:55
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/gsl/gsl_ieee_utils.h:56

   procedure gsl_ieee_printf_float (x : access float)  -- /opt/homebrew/include/gsl/gsl_ieee_utils.h:59
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_ieee_printf_float";

   procedure gsl_ieee_printf_double (x : access double)  -- /opt/homebrew/include/gsl/gsl_ieee_utils.h:60
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_ieee_printf_double";

   procedure gsl_ieee_fprintf_float (stream : access ustdio_h.uu_sFILE; x : access float)  -- /opt/homebrew/include/gsl/gsl_ieee_utils.h:62
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_ieee_fprintf_float";

   procedure gsl_ieee_fprintf_double (stream : access ustdio_h.uu_sFILE; x : access double)  -- /opt/homebrew/include/gsl/gsl_ieee_utils.h:63
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_ieee_fprintf_double";

   procedure gsl_ieee_float_to_rep (x : access float; r : access gsl_ieee_float_rep)  -- /opt/homebrew/include/gsl/gsl_ieee_utils.h:65
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_ieee_float_to_rep";

   procedure gsl_ieee_double_to_rep (x : access double; r : access gsl_ieee_double_rep)  -- /opt/homebrew/include/gsl/gsl_ieee_utils.h:66
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_ieee_double_to_rep";

   procedure gsl_ieee_env_setup  -- /opt/homebrew/include/gsl/gsl_ieee_utils.h:91
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_ieee_env_setup";

   function gsl_ieee_read_mode_string
     (description : Interfaces.C.Strings.chars_ptr;
      precision : access int;
      rounding : access int;
      exception_mask : access int) return int  -- /opt/homebrew/include/gsl/gsl_ieee_utils.h:92
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_ieee_read_mode_string";

   function gsl_ieee_set_mode
     (precision : int;
      rounding : int;
      exception_mask : int) return int  -- /opt/homebrew/include/gsl/gsl_ieee_utils.h:94
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_ieee_set_mode";

end gsl_gsl_ieee_utils_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
