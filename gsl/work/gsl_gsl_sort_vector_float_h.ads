pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
limited with gsl_gsl_vector_float_h;
limited with gsl_gsl_permutation_h;
with sys_utypes_usize_t_h;

package gsl_gsl_sort_vector_float_h is

   procedure gsl_sort_vector_float (v : access gsl_gsl_vector_float_h.gsl_vector_float)  -- /opt/homebrew/include/gsl/gsl_sort_vector_float.h:40
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_sort_vector_float";

   procedure gsl_sort_vector2_float (v1 : access gsl_gsl_vector_float_h.gsl_vector_float; v2 : access gsl_gsl_vector_float_h.gsl_vector_float)  -- /opt/homebrew/include/gsl/gsl_sort_vector_float.h:41
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_sort_vector2_float";

   function gsl_sort_vector_float_index (p : access gsl_gsl_permutation_h.gsl_permutation_struct; v : access constant gsl_gsl_vector_float_h.gsl_vector_float) return int  -- /opt/homebrew/include/gsl/gsl_sort_vector_float.h:42
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_sort_vector_float_index";

   function gsl_sort_vector_float_smallest
     (dest : access float;
      k : sys_utypes_usize_t_h.size_t;
      v : access constant gsl_gsl_vector_float_h.gsl_vector_float) return int  -- /opt/homebrew/include/gsl/gsl_sort_vector_float.h:44
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_sort_vector_float_smallest";

   function gsl_sort_vector_float_largest
     (dest : access float;
      k : sys_utypes_usize_t_h.size_t;
      v : access constant gsl_gsl_vector_float_h.gsl_vector_float) return int  -- /opt/homebrew/include/gsl/gsl_sort_vector_float.h:45
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_sort_vector_float_largest";

   function gsl_sort_vector_float_smallest_index
     (p : access sys_utypes_usize_t_h.size_t;
      k : sys_utypes_usize_t_h.size_t;
      v : access constant gsl_gsl_vector_float_h.gsl_vector_float) return int  -- /opt/homebrew/include/gsl/gsl_sort_vector_float.h:47
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_sort_vector_float_smallest_index";

   function gsl_sort_vector_float_largest_index
     (p : access sys_utypes_usize_t_h.size_t;
      k : sys_utypes_usize_t_h.size_t;
      v : access constant gsl_gsl_vector_float_h.gsl_vector_float) return int  -- /opt/homebrew/include/gsl/gsl_sort_vector_float.h:48
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_sort_vector_float_largest_index";

end gsl_gsl_sort_vector_float_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
