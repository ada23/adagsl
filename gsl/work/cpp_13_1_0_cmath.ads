pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Extensions;

package cpp_13_1_0_cmath is

   function acos (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:89
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4acosf";

   function acos (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:93
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4acose";

   function asin (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:108
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4asinf";

   function asin (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:112
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4asine";

   function atan (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:127
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4atanf";

   function atan (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:131
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4atane";

   function atan2 (uu_y : float; uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:146
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5atan2ff";

   function atan2 (uu_y : long_double; uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:150
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5atan2ee";

   function ceil (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:167
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4ceilf";

   function ceil (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:171
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4ceile";

   function cos (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:186
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt3cosf";

   function cos (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:190
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt3cose";

   function cosh (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:205
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4coshf";

   function cosh (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:209
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4coshe";

   function exp (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:224
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt3expf";

   function exp (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:228
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt3expe";

   function fabs (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:243
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4fabsf";

   function fabs (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:247
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4fabse";

   function floor (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:262
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5floorf";

   function floor (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:266
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5floore";

   function fmod (uu_x : float; uu_y : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:281
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4fmodff";

   function fmod (uu_x : long_double; uu_y : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:285
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4fmodee";

   function frexp (uu_x : float; uu_exp : access int) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:302
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5frexpfPi";

   function frexp (uu_x : long_double; uu_exp : access int) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:306
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5frexpePi";

   function ldexp (uu_x : float; uu_exp : int) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:321
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5ldexpfi";

   function ldexp (uu_x : long_double; uu_exp : int) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:325
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5ldexpei";

   function log (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:340
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt3logf";

   function log (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:344
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt3loge";

   function log10 (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:359
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5log10f";

   function log10 (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:363
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5log10e";

   function modf (uu_x : float; uu_iptr : access float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:378
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4modffPf";

   function modf (uu_x : long_double; uu_iptr : access long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:382
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4modfePe";

   function pow (uu_x : float; uu_y : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:390
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt3powff";

   function pow (uu_x : long_double; uu_y : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:394
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt3powee";

   function sin (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:427
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt3sinf";

   function sin (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:431
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt3sine";

   function sinh (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:446
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4sinhf";

   function sinh (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:450
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4sinhe";

   function sqrt (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:465
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4sqrtf";

   function sqrt (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:469
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4sqrte";

   function tan (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:484
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt3tanf";

   function tan (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:488
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt3tane";

   function tanh (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:503
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4tanhf";

   function tanh (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:507
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4tanhe";

   function fpclassify (uu_x : float) return int  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1097
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt10fpclassifyf";

   function fpclassify (uu_x : double) return int  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1102
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt10fpclassifyd";

   function fpclassify (uu_x : long_double) return int  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1107
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt10fpclassifye";

   function isfinite (uu_x : float) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1122
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt8isfinitef";

   function isfinite (uu_x : double) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1126
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt8isfinited";

   function isfinite (uu_x : long_double) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1130
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt8isfinitee";

   function isinf (uu_x : float) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1144
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5isinff";

   function isinf (uu_x : double) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1152
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5isinfd";

   function isinf (uu_x : long_double) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1157
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5isinfe";

   function isnan (uu_x : float) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1171
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5isnanf";

   function isnan (uu_x : double) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1179
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5isnand";

   function isnan (uu_x : long_double) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1184
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5isnane";

   function isnormal (uu_x : float) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1198
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt8isnormalf";

   function isnormal (uu_x : double) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1202
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt8isnormald";

   function isnormal (uu_x : long_double) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1206
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt8isnormale";

   function signbit (uu_x : float) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1221
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt7signbitf";

   function signbit (uu_x : double) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1225
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt7signbitd";

   function signbit (uu_x : long_double) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1229
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt7signbite";

   function isgreater (uu_x : float; uu_y : float) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1243
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt9isgreaterff";

   function isgreater (uu_x : double; uu_y : double) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1247
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt9isgreaterdd";

   function isgreater (uu_x : long_double; uu_y : long_double) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1251
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt9isgreateree";

   function isgreaterequal (uu_x : float; uu_y : float) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1269
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt14isgreaterequalff";

   function isgreaterequal (uu_x : double; uu_y : double) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1273
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt14isgreaterequaldd";

   function isgreaterequal (uu_x : long_double; uu_y : long_double) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1277
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt14isgreaterequalee";

   function isless (uu_x : float; uu_y : float) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1295
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt6islessff";

   function isless (uu_x : double; uu_y : double) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1299
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt6islessdd";

   function isless (uu_x : long_double; uu_y : long_double) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1303
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt6islessee";

   function islessequal (uu_x : float; uu_y : float) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1321
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt11islessequalff";

   function islessequal (uu_x : double; uu_y : double) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1325
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt11islessequaldd";

   function islessequal (uu_x : long_double; uu_y : long_double) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1329
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt11islessequalee";

   function islessgreater (uu_x : float; uu_y : float) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1347
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt13islessgreaterff";

   function islessgreater (uu_x : double; uu_y : double) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1351
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt13islessgreaterdd";

   function islessgreater (uu_x : long_double; uu_y : long_double) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1355
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt13islessgreateree";

   function isunordered (uu_x : float; uu_y : float) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1373
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt11isunorderedff";

   function isunordered (uu_x : double; uu_y : double) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1377
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt11isunordereddd";

   function isunordered (uu_x : long_double; uu_y : long_double) return Extensions.bool  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:1381
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt11isunorderedee";

   function acosh (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2030
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5acoshf";

   function acosh (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2034
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5acoshe";

   function asinh (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2048
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5asinhf";

   function asinh (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2052
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5asinhe";

   function atanh (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2066
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5atanhf";

   function atanh (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2070
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5atanhe";

   function cbrt (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2084
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4cbrtf";

   function cbrt (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2088
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4cbrte";

   function copysign (uu_x : float; uu_y : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2102
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt8copysignff";

   function copysign (uu_x : long_double; uu_y : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2106
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt8copysignee";

   function erf (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2122
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt3erff";

   function erf (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2126
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt3erfe";

   function erfc (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2140
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4erfcf";

   function erfc (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2144
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4erfce";

   function exp2 (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2158
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4exp2f";

   function exp2 (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2162
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4exp2e";

   function expm1 (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2176
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5expm1f";

   function expm1 (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2180
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5expm1e";

   function fdim (uu_x : float; uu_y : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2194
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4fdimff";

   function fdim (uu_x : long_double; uu_y : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2198
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4fdimee";

   function fma
     (uu_x : float;
      uu_y : float;
      uu_z : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2214
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt3fmafff";

   function fma
     (uu_x : long_double;
      uu_y : long_double;
      uu_z : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2218
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt3fmaeee";

   function fmax (uu_x : float; uu_y : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2234
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4fmaxff";

   function fmax (uu_x : long_double; uu_y : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2238
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4fmaxee";

   function fmin (uu_x : float; uu_y : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2254
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4fminff";

   function fmin (uu_x : long_double; uu_y : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2258
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4fminee";

   function hypot (uu_x : float; uu_y : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2274
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5hypotff";

   function hypot (uu_x : long_double; uu_y : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2278
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5hypotee";

   function ilogb (uu_x : float) return int  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2294
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5ilogbf";

   function ilogb (uu_x : long_double) return int  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2298
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5ilogbe";

   function lgamma (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2313
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt6lgammaf";

   function lgamma (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2317
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt6lgammae";

   function llrint (uu_x : float) return Long_Long_Integer  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2331
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt6llrintf";

   function llrint (uu_x : long_double) return Long_Long_Integer  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2335
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt6llrinte";

   function llround (uu_x : float) return Long_Long_Integer  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2349
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt7llroundf";

   function llround (uu_x : long_double) return Long_Long_Integer  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2353
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt7llrounde";

   function log1p (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2367
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5log1pf";

   function log1p (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2371
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5log1pe";

   function log2 (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2386
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4log2f";

   function log2 (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2390
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4log2e";

   function logb (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2404
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4logbf";

   function logb (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2408
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4logbe";

   function lrint (uu_x : float) return long  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2422
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5lrintf";

   function lrint (uu_x : long_double) return long  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2426
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5lrinte";

   function lround (uu_x : float) return long  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2440
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt6lroundf";

   function lround (uu_x : long_double) return long  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2444
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt6lrounde";

   function nearbyint (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2458
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt9nearbyintf";

   function nearbyint (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2462
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt9nearbyinte";

   function nextafter (uu_x : float; uu_y : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2476
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt9nextafterff";

   function nextafter (uu_x : long_double; uu_y : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2480
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt9nextafteree";

   function nexttoward (uu_x : float; uu_y : long_double) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2496
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt10nexttowardfe";

   function nexttoward (uu_x : long_double; uu_y : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2500
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt10nexttowardee";

   function remainder (uu_x : float; uu_y : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2514
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt9remainderff";

   function remainder (uu_x : long_double; uu_y : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2518
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt9remainderee";

   function remquo
     (uu_x : float;
      uu_y : float;
      uu_pquo : access int) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2534
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt6remquoffPi";

   function remquo
     (uu_x : long_double;
      uu_y : long_double;
      uu_pquo : access int) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2538
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt6remquoeePi";

   function rint (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2554
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4rintf";

   function rint (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2558
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt4rinte";

   function round (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2572
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5roundf";

   function round (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2576
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5rounde";

   function scalbln (uu_x : float; uu_ex : long) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2590
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt7scalblnfl";

   function scalbln (uu_x : long_double; uu_ex : long) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2594
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt7scalblnel";

   function scalbn (uu_x : float; uu_ex : int) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2608
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt6scalbnfi";

   function scalbn (uu_x : long_double; uu_ex : int) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2612
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt6scalbnei";

   function tgamma (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2626
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt6tgammaf";

   function tgamma (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2630
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt6tgammae";

   function trunc (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2644
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5truncf";

   function trunc (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:2648
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5trunce";

   function hypot
     (uu_x : float;
      uu_y : float;
      uu_z : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:3583
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5hypotfff";

   function hypot
     (uu_x : double;
      uu_y : double;
      uu_z : double) return double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:3587
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5hypotddd";

   function hypot
     (uu_x : long_double;
      uu_y : long_double;
      uu_z : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/cmath:3591
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5hypoteee";

end cpp_13_1_0_cmath;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
