pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with sys_utypes_usize_t_h;
limited with ustdio_h;
limited with gsl_gsl_monte_h;
limited with gsl_gsl_rng_h;

package gsl_gsl_monte_vegas_h is

   type gsl_monte_vegas_state is record
      dim : aliased sys_utypes_usize_t_h.size_t;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:48
      bins_max : aliased sys_utypes_usize_t_h.size_t;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:49
      bins : aliased unsigned;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:50
      boxes : aliased unsigned;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:51
      xi : access double;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:52
      xin : access double;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:53
      delx : access double;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:54
      weight : access double;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:55
      vol : aliased double;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:56
      x : access double;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:58
      bin : access int;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:59
      box : access int;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:60
      d : access double;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:63
      alpha : aliased double;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:66
      mode : aliased int;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:67
      verbose : aliased int;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:68
      iterations : aliased unsigned;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:69
      stage : aliased int;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:70
      jac : aliased double;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:73
      wtd_int_sum : aliased double;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:74
      sum_wgts : aliased double;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:75
      chi_sum : aliased double;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:76
      chisq : aliased double;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:77
      result : aliased double;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:79
      sigma : aliased double;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:80
      it_start : aliased unsigned;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:82
      it_num : aliased unsigned;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:83
      samples : aliased unsigned;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:84
      calls_per_box : aliased unsigned;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:85
      ostream : access ustdio_h.uu_sFILE;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:87
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:89

   function gsl_monte_vegas_integrate
     (f : access gsl_gsl_monte_h.gsl_monte_function_struct;
      xl : access double;
      xu : access double;
      dim : sys_utypes_usize_t_h.size_t;
      calls : sys_utypes_usize_t_h.size_t;
      r : access gsl_gsl_rng_h.gsl_rng;
      state : access gsl_monte_vegas_state;
      result : access double;
      abserr : access double) return int  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:91
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_monte_vegas_integrate";

   function gsl_monte_vegas_alloc (dim : sys_utypes_usize_t_h.size_t) return access gsl_monte_vegas_state  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:98
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_monte_vegas_alloc";

   function gsl_monte_vegas_init (state : access gsl_monte_vegas_state) return int  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:100
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_monte_vegas_init";

   procedure gsl_monte_vegas_free (state : access gsl_monte_vegas_state)  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:102
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_monte_vegas_free";

   function gsl_monte_vegas_chisq (state : access constant gsl_monte_vegas_state) return double  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:104
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_monte_vegas_chisq";

   procedure gsl_monte_vegas_runval
     (state : access constant gsl_monte_vegas_state;
      result : access double;
      sigma : access double)  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:105
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_monte_vegas_runval";

   type gsl_monte_vegas_params is record
      alpha : aliased double;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:108
      iterations : aliased sys_utypes_usize_t_h.size_t;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:109
      stage : aliased int;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:110
      mode : aliased int;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:111
      verbose : aliased int;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:112
      ostream : access ustdio_h.uu_sFILE;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:113
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:114

   procedure gsl_monte_vegas_params_get (state : access constant gsl_monte_vegas_state; params : access gsl_monte_vegas_params)  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:116
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_monte_vegas_params_get";

   procedure gsl_monte_vegas_params_set (state : access gsl_monte_vegas_state; params : access constant gsl_monte_vegas_params)  -- /opt/homebrew/include/gsl/gsl_monte_vegas.h:119
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_monte_vegas_params_set";

end gsl_gsl_monte_vegas_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
