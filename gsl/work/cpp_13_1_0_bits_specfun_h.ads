pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package cpp_13_1_0_bits_specfun_h is

   function assoc_laguerref
     (uu_n : unsigned;
      uu_m : unsigned;
      uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:204
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt15assoc_laguerrefjjf";

   function assoc_laguerrel
     (uu_n : unsigned;
      uu_m : unsigned;
      uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:214
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt15assoc_laguerreljje";

   function assoc_legendref
     (uu_l : unsigned;
      uu_m : unsigned;
      uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:265
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt15assoc_legendrefjjf";

   function assoc_legendrel
     (uu_l : unsigned;
      uu_m : unsigned;
      uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:274
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt15assoc_legendreljje";

   function betaf (uu_a : float; uu_b : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:310
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5betafff";

   function betal (uu_a : long_double; uu_b : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:320
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt5betalee";

   function comp_ellint_1f (uu_k : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:356
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt14comp_ellint_1ff";

   function comp_ellint_1l (uu_k : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:366
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt14comp_ellint_1le";

   function comp_ellint_2f (uu_k : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:404
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt14comp_ellint_2ff";

   function comp_ellint_2l (uu_k : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:414
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt14comp_ellint_2le";

   function comp_ellint_3f (uu_k : float; uu_nu : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:451
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt14comp_ellint_3fff";

   function comp_ellint_3l (uu_k : long_double; uu_nu : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:461
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt14comp_ellint_3lee";

   function cyl_bessel_if (uu_nu : float; uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:502
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt13cyl_bessel_ifff";

   function cyl_bessel_il (uu_nu : long_double; uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:512
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt13cyl_bessel_ilee";

   function cyl_bessel_jf (uu_nu : float; uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:548
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt13cyl_bessel_jfff";

   function cyl_bessel_jl (uu_nu : long_double; uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:558
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt13cyl_bessel_jlee";

   function cyl_bessel_kf (uu_nu : float; uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:594
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt13cyl_bessel_kfff";

   function cyl_bessel_kl (uu_nu : long_double; uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:604
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt13cyl_bessel_klee";

   function cyl_neumannf (uu_nu : float; uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:646
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt12cyl_neumannfff";

   function cyl_neumannl (uu_nu : long_double; uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:656
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt12cyl_neumannlee";

   function ellint_1f (uu_k : float; uu_phi : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:694
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt9ellint_1fff";

   function ellint_1l (uu_k : long_double; uu_phi : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:704
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt9ellint_1lee";

   function ellint_2f (uu_k : float; uu_phi : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:742
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt9ellint_2fff";

   function ellint_2l (uu_k : long_double; uu_phi : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:752
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt9ellint_2lee";

   function ellint_3f
     (uu_k : float;
      uu_nu : float;
      uu_phi : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:790
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt9ellint_3ffff";

   function ellint_3l
     (uu_k : long_double;
      uu_nu : long_double;
      uu_phi : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:800
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt9ellint_3leee";

   function expintf (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:842
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt7expintff";

   function expintl (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:852
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt7expintle";

   function hermitef (uu_n : unsigned; uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:883
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt8hermitefjf";

   function hermitel (uu_n : unsigned; uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:893
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt8hermitelje";

   function laguerref (uu_n : unsigned; uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:931
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt9laguerrefjf";

   function laguerrel (uu_n : unsigned; uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:941
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt9laguerrelje";

   function legendref (uu_l : unsigned; uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:975
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt9legendrefjf";

   function legendrel (uu_l : unsigned; uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:985
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt9legendrelje";

   function riemann_zetaf (uu_s : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:1020
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt13riemann_zetaff";

   function riemann_zetal (uu_s : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:1030
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt13riemann_zetale";

   function sph_besself (uu_n : unsigned; uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:1071
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt11sph_besselfjf";

   function sph_bessell (uu_n : unsigned; uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:1081
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt11sph_bessellje";

   function sph_legendref
     (uu_l : unsigned;
      uu_m : unsigned;
      uu_theta : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:1115
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt13sph_legendrefjjf";

   function sph_legendrel
     (uu_l : unsigned;
      uu_m : unsigned;
      uu_theta : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:1126
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt13sph_legendreljje";

   function sph_neumannf (uu_n : unsigned; uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:1162
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt12sph_neumannfjf";

   function sph_neumannl (uu_n : unsigned; uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:1172
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt12sph_neumannlje";

   function airy_aif (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:1217
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZN9__gnu_cxx8airy_aifEf";

   function airy_ail (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:1228
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZN9__gnu_cxx8airy_ailEe";

   function airy_bif (uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:1252
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZN9__gnu_cxx8airy_bifEf";

   function airy_bil (uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:1263
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZN9__gnu_cxx8airy_bilEe";

   function conf_hypergf
     (uu_a : float;
      uu_c : float;
      uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:1293
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZN9__gnu_cxx12conf_hypergfEfff";

   function conf_hypergl
     (uu_a : long_double;
      uu_c : long_double;
      uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:1304
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZN9__gnu_cxx12conf_hyperglEeee";

   function hypergf
     (uu_a : float;
      uu_b : float;
      uu_c : float;
      uu_x : float) return float  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:1341
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZN9__gnu_cxx7hypergfEffff";

   function hypergl
     (uu_a : long_double;
      uu_b : long_double;
      uu_c : long_double;
      uu_x : long_double) return long_double  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/specfun.h:1352
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZN9__gnu_cxx7hyperglEeeee";

end cpp_13_1_0_bits_specfun_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
