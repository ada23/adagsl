pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
limited with ustdio_h;
with System;
with sys_utypes_usize_t_h;
with Interfaces.C.Strings;
limited with gsl_gsl_histogram_h;

package gsl_gsl_ntuple_h is

   type gsl_ntuple is record
      the_file : access ustdio_h.uu_sFILE;  -- /opt/homebrew/include/gsl/gsl_ntuple.h:44
      ntuple_data : System.Address;  -- /opt/homebrew/include/gsl/gsl_ntuple.h:45
      size : aliased sys_utypes_usize_t_h.size_t;  -- /opt/homebrew/include/gsl/gsl_ntuple.h:46
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/gsl/gsl_ntuple.h:47

   type gsl_ntuple_select_fn is record
      c_function : access function (arg1 : System.Address; arg2 : System.Address) return int;  -- /opt/homebrew/include/gsl/gsl_ntuple.h:50
      params : System.Address;  -- /opt/homebrew/include/gsl/gsl_ntuple.h:51
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/gsl/gsl_ntuple.h:52

   type gsl_ntuple_value_fn is record
      c_function : access function (arg1 : System.Address; arg2 : System.Address) return double;  -- /opt/homebrew/include/gsl/gsl_ntuple.h:55
      params : System.Address;  -- /opt/homebrew/include/gsl/gsl_ntuple.h:56
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/gsl/gsl_ntuple.h:57

   function gsl_ntuple_open
     (filename : Interfaces.C.Strings.chars_ptr;
      ntuple_data : System.Address;
      size : sys_utypes_usize_t_h.size_t) return access gsl_ntuple  -- /opt/homebrew/include/gsl/gsl_ntuple.h:60
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_ntuple_open";

   function gsl_ntuple_create
     (filename : Interfaces.C.Strings.chars_ptr;
      ntuple_data : System.Address;
      size : sys_utypes_usize_t_h.size_t) return access gsl_ntuple  -- /opt/homebrew/include/gsl/gsl_ntuple.h:63
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_ntuple_create";

   function gsl_ntuple_write (ntuple : access gsl_ntuple) return int  -- /opt/homebrew/include/gsl/gsl_ntuple.h:65
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_ntuple_write";

   function gsl_ntuple_read (ntuple : access gsl_ntuple) return int  -- /opt/homebrew/include/gsl/gsl_ntuple.h:66
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_ntuple_read";

   function gsl_ntuple_bookdata (ntuple : access gsl_ntuple) return int  -- /opt/homebrew/include/gsl/gsl_ntuple.h:68
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_ntuple_bookdata";

   function gsl_ntuple_project
     (h : access gsl_gsl_histogram_h.gsl_histogram;
      ntuple : access gsl_ntuple;
      value_func : access gsl_ntuple_value_fn;
      select_func : access gsl_ntuple_select_fn) return int  -- /opt/homebrew/include/gsl/gsl_ntuple.h:70
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_ntuple_project";

   function gsl_ntuple_close (ntuple : access gsl_ntuple) return int  -- /opt/homebrew/include/gsl/gsl_ntuple.h:74
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_ntuple_close";

end gsl_gsl_ntuple_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
