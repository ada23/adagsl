pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with stdlib_h;

package cpp_13_1_0_cstdlib is

   function div (uu_i : long; uu_j : long) return stdlib_h.ldiv_t  -- /opt/gcc-13.1.0/include/c++/13.1.0/cstdlib:181
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt3divll";

   function div (uu_n : Long_Long_Integer; uu_d : Long_Long_Integer) return stdlib_h.lldiv_t  -- /opt/gcc-13.1.0/include/c++/13.1.0/cstdlib:217
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZN9__gnu_cxx3divExx";

end cpp_13_1_0_cstdlib;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
