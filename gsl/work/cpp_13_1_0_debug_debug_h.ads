pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package cpp_13_1_0_debug_debug_h is

end cpp_13_1_0_debug_debug_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
