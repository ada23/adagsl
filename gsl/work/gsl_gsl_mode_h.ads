pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package gsl_gsl_mode_h is

   GSL_PREC_DOUBLE : constant := 0;  --  /opt/homebrew/include/gsl/gsl_mode.h:66
   GSL_PREC_SINGLE : constant := 1;  --  /opt/homebrew/include/gsl/gsl_mode.h:67
   GSL_PREC_APPROX : constant := 2;  --  /opt/homebrew/include/gsl/gsl_mode.h:68
   --  arg-macro: function GSL_MODE_PREC (mt)
   --    return (mt) and (unsigned int)7;

   GSL_MODE_DEFAULT : constant := 0;  --  /opt/homebrew/include/gsl/gsl_mode.h:83

   subtype gsl_mode_t is unsigned;  -- /opt/homebrew/include/gsl/gsl_mode.h:50

end gsl_gsl_mode_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
