pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Extensions;
with sys_utypes_usize_t_h;
with System;

package malloc_umalloc_type_h is

   subtype malloc_type_id_t is Extensions.unsigned_long_long;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/malloc/_malloc_type.h:46

   function malloc_type_malloc (size : sys_utypes_usize_t_h.size_t; type_id : malloc_type_id_t) return System.Address  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/malloc/_malloc_type.h:48
   with Import => True, 
        Convention => C, 
        External_Name => "malloc_type_malloc";

   function malloc_type_calloc
     (count : sys_utypes_usize_t_h.size_t;
      size : sys_utypes_usize_t_h.size_t;
      type_id : malloc_type_id_t) return System.Address  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/malloc/_malloc_type.h:49
   with Import => True, 
        Convention => C, 
        External_Name => "malloc_type_calloc";

   procedure malloc_type_free (ptr : System.Address; type_id : malloc_type_id_t)  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/malloc/_malloc_type.h:50
   with Import => True, 
        Convention => C, 
        External_Name => "malloc_type_free";

   function malloc_type_realloc
     (ptr : System.Address;
      size : sys_utypes_usize_t_h.size_t;
      type_id : malloc_type_id_t) return System.Address  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/malloc/_malloc_type.h:51
   with Import => True, 
        Convention => C, 
        External_Name => "malloc_type_realloc";

   function malloc_type_valloc (size : sys_utypes_usize_t_h.size_t; type_id : malloc_type_id_t) return System.Address  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/malloc/_malloc_type.h:52
   with Import => True, 
        Convention => C, 
        External_Name => "malloc_type_valloc";

   function malloc_type_aligned_alloc
     (alignment : sys_utypes_usize_t_h.size_t;
      size : sys_utypes_usize_t_h.size_t;
      type_id : malloc_type_id_t) return System.Address  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/malloc/_malloc_type.h:53
   with Import => True, 
        Convention => C, 
        External_Name => "malloc_type_aligned_alloc";

   function malloc_type_posix_memalign
     (memptr : System.Address;
      alignment : sys_utypes_usize_t_h.size_t;
      size : sys_utypes_usize_t_h.size_t;
      type_id : malloc_type_id_t) return int  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/malloc/_malloc_type.h:54
   with Import => True, 
        Convention => C, 
        External_Name => "malloc_type_posix_memalign";

   type u_malloc_zone_t is null record;   -- incomplete struct

   subtype malloc_zone_t is u_malloc_zone_t;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/malloc/_malloc_type.h:59

   function malloc_type_zone_malloc
     (zone : access malloc_zone_t;
      size : sys_utypes_usize_t_h.size_t;
      type_id : malloc_type_id_t) return System.Address  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/malloc/_malloc_type.h:61
   with Import => True, 
        Convention => C, 
        External_Name => "malloc_type_zone_malloc";

   function malloc_type_zone_calloc
     (zone : access malloc_zone_t;
      count : sys_utypes_usize_t_h.size_t;
      size : sys_utypes_usize_t_h.size_t;
      type_id : malloc_type_id_t) return System.Address  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/malloc/_malloc_type.h:62
   with Import => True, 
        Convention => C, 
        External_Name => "malloc_type_zone_calloc";

   procedure malloc_type_zone_free
     (zone : access malloc_zone_t;
      ptr : System.Address;
      type_id : malloc_type_id_t)  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/malloc/_malloc_type.h:63
   with Import => True, 
        Convention => C, 
        External_Name => "malloc_type_zone_free";

   function malloc_type_zone_realloc
     (zone : access malloc_zone_t;
      ptr : System.Address;
      size : sys_utypes_usize_t_h.size_t;
      type_id : malloc_type_id_t) return System.Address  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/malloc/_malloc_type.h:64
   with Import => True, 
        Convention => C, 
        External_Name => "malloc_type_zone_realloc";

   function malloc_type_zone_valloc
     (zone : access malloc_zone_t;
      size : sys_utypes_usize_t_h.size_t;
      type_id : malloc_type_id_t) return System.Address  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/malloc/_malloc_type.h:65
   with Import => True, 
        Convention => C, 
        External_Name => "malloc_type_zone_valloc";

   function malloc_type_zone_memalign
     (zone : access malloc_zone_t;
      alignment : sys_utypes_usize_t_h.size_t;
      size : sys_utypes_usize_t_h.size_t;
      type_id : malloc_type_id_t) return System.Address  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/malloc/_malloc_type.h:66
   with Import => True, 
        Convention => C, 
        External_Name => "malloc_type_zone_memalign";

end malloc_umalloc_type_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
