pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
limited with gsl_gsl_vector_uchar_h;
limited with gsl_gsl_permutation_h;
with sys_utypes_usize_t_h;

package gsl_gsl_sort_vector_uchar_h is

   procedure gsl_sort_vector_uchar (v : access gsl_gsl_vector_uchar_h.gsl_vector_uchar)  -- /opt/homebrew/include/gsl/gsl_sort_vector_uchar.h:40
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_sort_vector_uchar";

   procedure gsl_sort_vector2_uchar (v1 : access gsl_gsl_vector_uchar_h.gsl_vector_uchar; v2 : access gsl_gsl_vector_uchar_h.gsl_vector_uchar)  -- /opt/homebrew/include/gsl/gsl_sort_vector_uchar.h:41
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_sort_vector2_uchar";

   function gsl_sort_vector_uchar_index (p : access gsl_gsl_permutation_h.gsl_permutation_struct; v : access constant gsl_gsl_vector_uchar_h.gsl_vector_uchar) return int  -- /opt/homebrew/include/gsl/gsl_sort_vector_uchar.h:42
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_sort_vector_uchar_index";

   function gsl_sort_vector_uchar_smallest
     (dest : access unsigned_char;
      k : sys_utypes_usize_t_h.size_t;
      v : access constant gsl_gsl_vector_uchar_h.gsl_vector_uchar) return int  -- /opt/homebrew/include/gsl/gsl_sort_vector_uchar.h:44
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_sort_vector_uchar_smallest";

   function gsl_sort_vector_uchar_largest
     (dest : access unsigned_char;
      k : sys_utypes_usize_t_h.size_t;
      v : access constant gsl_gsl_vector_uchar_h.gsl_vector_uchar) return int  -- /opt/homebrew/include/gsl/gsl_sort_vector_uchar.h:45
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_sort_vector_uchar_largest";

   function gsl_sort_vector_uchar_smallest_index
     (p : access sys_utypes_usize_t_h.size_t;
      k : sys_utypes_usize_t_h.size_t;
      v : access constant gsl_gsl_vector_uchar_h.gsl_vector_uchar) return int  -- /opt/homebrew/include/gsl/gsl_sort_vector_uchar.h:47
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_sort_vector_uchar_smallest_index";

   function gsl_sort_vector_uchar_largest_index
     (p : access sys_utypes_usize_t_h.size_t;
      k : sys_utypes_usize_t_h.size_t;
      v : access constant gsl_gsl_vector_uchar_h.gsl_vector_uchar) return int  -- /opt/homebrew/include/gsl/gsl_sort_vector_uchar.h:48
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_sort_vector_uchar_largest_index";

end gsl_gsl_sort_vector_uchar_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
