pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings;

package math_h is

   FIXINC_WRAP_MATH_H_MATH_EXCEPTION : constant := 1;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:11
   --  unsupported macro: HUGE_VAL __builtin_huge_val()
   --  unsupported macro: HUGE_VALF __builtin_huge_valf()
   --  unsupported macro: HUGE_VALL __builtin_huge_vall()
   --  unsupported macro: NAN __builtin_nanf("0x7fc00000")
   --  unsupported macro: INFINITY HUGE_VALF

   FP_NAN : constant := 1;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:89
   FP_INFINITE : constant := 2;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:90
   FP_ZERO : constant := 3;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:91
   FP_NORMAL : constant := 4;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:92
   FP_SUBNORMAL : constant := 5;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:93
   FP_SUPERNORMAL : constant := 6;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:94

   FP_ILOGB0 : constant := (-2147483647 - 1);  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:118
   FP_ILOGBNAN : constant := (-2147483647 - 1);  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:119

   MATH_ERRNO : constant := 1;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:122
   MATH_ERREXCEPT : constant := 2;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:123
   --  unsupported macro: math_errhandling (__math_errhandling())

   M_E : constant := 2.71828182845904523536028747135266250;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:704
   M_LOG2E : constant := 1.44269504088896340735992468100189214;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:705
   M_LOG10E : constant := 0.434294481903251827651128918916605082;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:706
   M_LN2 : constant := 0.693147180559945309417232121458176568;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:707
   M_LN10 : constant := 2.30258509299404568401799145468436421;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:708
   M_PI : constant := 3.14159265358979323846264338327950288;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:709
   M_PI_2 : constant := 1.57079632679489661923132169163975144;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:710
   M_PI_4 : constant := 0.785398163397448309615660845819875721;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:711
   M_1_PI : constant := 0.318309886183790671537767526745028724;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:712
   M_2_PI : constant := 0.636619772367581343075535053490057448;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:713
   M_2_SQRTPI : constant := 1.12837916709551257389615890312154517;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:714
   M_SQRT2 : constant := 1.41421356237309504880168872420969808;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:715
   M_SQRT1_2 : constant := 0.707106781186547524400844362104849039;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:716

   MAXFLOAT : constant := 16#1.fffffep+127f#;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:718
   --  unsupported macro: FP_SNAN FP_NAN
   --  unsupported macro: FP_QNAN FP_NAN
   --  unsupported macro: HUGE MAXFLOAT

   X_TLOSS : constant := 1.41484755040568800000e+16;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:748
   DOMAIN : constant := 1;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:749
   SING : constant := 2;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:750
   OVERFLOW : constant := 3;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:751
   UNDERFLOW : constant := 4;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:752
   TLOSS : constant := 5;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:753
   PLOSS : constant := 6;  --  /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:754

   subtype float_t is float;  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:59

   subtype double_t is double;  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:60

   --  skipped func __math_errhandling

   --  skipped func __fpclassifyf

   --  skipped func __fpclassifyd

   --  skipped func __fpclassifyl

   --  skipped func __inline_isfinitef

   --  skipped func __inline_isfinited

   --  skipped func __inline_isfinitel

   --  skipped func __inline_isinff

   --  skipped func __inline_isinfd

   --  skipped func __inline_isinfl

   --  skipped func __inline_isnanf

   --  skipped func __inline_isnand

   --  skipped func __inline_isnanl

   --  skipped func __inline_signbitf

   --  skipped func __inline_signbitd

   --  skipped func __inline_signbitl

   --  skipped func __inline_isnormalf

   --  skipped func __inline_isnormald

   --  skipped func __inline_isnormall

   function acosf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:323
   with Import => True, 
        Convention => C, 
        External_Name => "acosf";

   function acos (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:324
   with Import => True, 
        Convention => C, 
        External_Name => "acos";

   function acosl (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:325
   with Import => True, 
        Convention => C, 
        External_Name => "acosl";

   function asinf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:327
   with Import => True, 
        Convention => C, 
        External_Name => "asinf";

   function asin (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:328
   with Import => True, 
        Convention => C, 
        External_Name => "asin";

   function asinl (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:329
   with Import => True, 
        Convention => C, 
        External_Name => "asinl";

   function atanf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:331
   with Import => True, 
        Convention => C, 
        External_Name => "atanf";

   function atan (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:332
   with Import => True, 
        Convention => C, 
        External_Name => "atan";

   function atanl (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:333
   with Import => True, 
        Convention => C, 
        External_Name => "atanl";

   function atan2f (arg1 : float; arg2 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:335
   with Import => True, 
        Convention => C, 
        External_Name => "atan2f";

   function atan2 (arg1 : double; arg2 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:336
   with Import => True, 
        Convention => C, 
        External_Name => "atan2";

   function atan2l (arg1 : long_double; arg2 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:337
   with Import => True, 
        Convention => C, 
        External_Name => "atan2l";

   function cosf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:339
   with Import => True, 
        Convention => C, 
        External_Name => "cosf";

   function cos (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:340
   with Import => True, 
        Convention => C, 
        External_Name => "cos";

   function cosl (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:341
   with Import => True, 
        Convention => C, 
        External_Name => "cosl";

   function sinf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:343
   with Import => True, 
        Convention => C, 
        External_Name => "sinf";

   function sin (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:344
   with Import => True, 
        Convention => C, 
        External_Name => "sin";

   function sinl (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:345
   with Import => True, 
        Convention => C, 
        External_Name => "sinl";

   function tanf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:347
   with Import => True, 
        Convention => C, 
        External_Name => "tanf";

   function tan (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:348
   with Import => True, 
        Convention => C, 
        External_Name => "tan";

   function tanl (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:349
   with Import => True, 
        Convention => C, 
        External_Name => "tanl";

   function acoshf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:351
   with Import => True, 
        Convention => C, 
        External_Name => "acoshf";

   function acosh (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:352
   with Import => True, 
        Convention => C, 
        External_Name => "acosh";

   function acoshl (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:353
   with Import => True, 
        Convention => C, 
        External_Name => "acoshl";

   function asinhf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:355
   with Import => True, 
        Convention => C, 
        External_Name => "asinhf";

   function asinh (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:356
   with Import => True, 
        Convention => C, 
        External_Name => "asinh";

   function asinhl (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:357
   with Import => True, 
        Convention => C, 
        External_Name => "asinhl";

   function atanhf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:359
   with Import => True, 
        Convention => C, 
        External_Name => "atanhf";

   function atanh (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:360
   with Import => True, 
        Convention => C, 
        External_Name => "atanh";

   function atanhl (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:361
   with Import => True, 
        Convention => C, 
        External_Name => "atanhl";

   function coshf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:363
   with Import => True, 
        Convention => C, 
        External_Name => "coshf";

   function cosh (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:364
   with Import => True, 
        Convention => C, 
        External_Name => "cosh";

   function coshl (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:365
   with Import => True, 
        Convention => C, 
        External_Name => "coshl";

   function sinhf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:367
   with Import => True, 
        Convention => C, 
        External_Name => "sinhf";

   function sinh (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:368
   with Import => True, 
        Convention => C, 
        External_Name => "sinh";

   function sinhl (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:369
   with Import => True, 
        Convention => C, 
        External_Name => "sinhl";

   function tanhf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:371
   with Import => True, 
        Convention => C, 
        External_Name => "tanhf";

   function tanh (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:372
   with Import => True, 
        Convention => C, 
        External_Name => "tanh";

   function tanhl (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:373
   with Import => True, 
        Convention => C, 
        External_Name => "tanhl";

   function expf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:375
   with Import => True, 
        Convention => C, 
        External_Name => "expf";

   function exp (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:376
   with Import => True, 
        Convention => C, 
        External_Name => "exp";

   function expl (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:377
   with Import => True, 
        Convention => C, 
        External_Name => "expl";

   function exp2f (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:379
   with Import => True, 
        Convention => C, 
        External_Name => "exp2f";

   function exp2 (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:380
   with Import => True, 
        Convention => C, 
        External_Name => "exp2";

   function exp2l (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:381
   with Import => True, 
        Convention => C, 
        External_Name => "exp2l";

   function expm1f (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:383
   with Import => True, 
        Convention => C, 
        External_Name => "expm1f";

   function expm1 (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:384
   with Import => True, 
        Convention => C, 
        External_Name => "expm1";

   function expm1l (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:385
   with Import => True, 
        Convention => C, 
        External_Name => "expm1l";

   function logf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:387
   with Import => True, 
        Convention => C, 
        External_Name => "logf";

   function log (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:388
   with Import => True, 
        Convention => C, 
        External_Name => "log";

   function logl (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:389
   with Import => True, 
        Convention => C, 
        External_Name => "logl";

   function log10f (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:391
   with Import => True, 
        Convention => C, 
        External_Name => "log10f";

   function log10 (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:392
   with Import => True, 
        Convention => C, 
        External_Name => "log10";

   function log10l (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:393
   with Import => True, 
        Convention => C, 
        External_Name => "log10l";

   function log2f (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:395
   with Import => True, 
        Convention => C, 
        External_Name => "log2f";

   function log2 (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:396
   with Import => True, 
        Convention => C, 
        External_Name => "log2";

   function log2l (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:397
   with Import => True, 
        Convention => C, 
        External_Name => "log2l";

   function log1pf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:399
   with Import => True, 
        Convention => C, 
        External_Name => "log1pf";

   function log1p (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:400
   with Import => True, 
        Convention => C, 
        External_Name => "log1p";

   function log1pl (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:401
   with Import => True, 
        Convention => C, 
        External_Name => "log1pl";

   function logbf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:403
   with Import => True, 
        Convention => C, 
        External_Name => "logbf";

   function logb (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:404
   with Import => True, 
        Convention => C, 
        External_Name => "logb";

   function logbl (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:405
   with Import => True, 
        Convention => C, 
        External_Name => "logbl";

   function modff (arg1 : float; arg2 : access float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:407
   with Import => True, 
        Convention => C, 
        External_Name => "modff";

   function modf (arg1 : double; arg2 : access double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:408
   with Import => True, 
        Convention => C, 
        External_Name => "modf";

   function modfl (arg1 : long_double; arg2 : access long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:409
   with Import => True, 
        Convention => C, 
        External_Name => "modfl";

   function ldexpf (arg1 : float; arg2 : int) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:411
   with Import => True, 
        Convention => C, 
        External_Name => "ldexpf";

   function ldexp (arg1 : double; arg2 : int) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:412
   with Import => True, 
        Convention => C, 
        External_Name => "ldexp";

   function ldexpl (arg1 : long_double; arg2 : int) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:413
   with Import => True, 
        Convention => C, 
        External_Name => "ldexpl";

   function frexpf (arg1 : float; arg2 : access int) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:415
   with Import => True, 
        Convention => C, 
        External_Name => "frexpf";

   function frexp (arg1 : double; arg2 : access int) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:416
   with Import => True, 
        Convention => C, 
        External_Name => "frexp";

   function frexpl (arg1 : long_double; arg2 : access int) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:417
   with Import => True, 
        Convention => C, 
        External_Name => "frexpl";

   function ilogbf (arg1 : float) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:419
   with Import => True, 
        Convention => C, 
        External_Name => "ilogbf";

   function ilogb (arg1 : double) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:420
   with Import => True, 
        Convention => C, 
        External_Name => "ilogb";

   function ilogbl (arg1 : long_double) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:421
   with Import => True, 
        Convention => C, 
        External_Name => "ilogbl";

   function scalbnf (arg1 : float; arg2 : int) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:423
   with Import => True, 
        Convention => C, 
        External_Name => "scalbnf";

   function scalbn (arg1 : double; arg2 : int) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:424
   with Import => True, 
        Convention => C, 
        External_Name => "scalbn";

   function scalbnl (arg1 : long_double; arg2 : int) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:425
   with Import => True, 
        Convention => C, 
        External_Name => "scalbnl";

   function scalblnf (arg1 : float; arg2 : long) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:427
   with Import => True, 
        Convention => C, 
        External_Name => "scalblnf";

   function scalbln (arg1 : double; arg2 : long) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:428
   with Import => True, 
        Convention => C, 
        External_Name => "scalbln";

   function scalblnl (arg1 : long_double; arg2 : long) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:429
   with Import => True, 
        Convention => C, 
        External_Name => "scalblnl";

   function fabsf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:431
   with Import => True, 
        Convention => C, 
        External_Name => "fabsf";

   function fabs (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:432
   with Import => True, 
        Convention => C, 
        External_Name => "fabs";

   function fabsl (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:433
   with Import => True, 
        Convention => C, 
        External_Name => "fabsl";

   function cbrtf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:435
   with Import => True, 
        Convention => C, 
        External_Name => "cbrtf";

   function cbrt (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:436
   with Import => True, 
        Convention => C, 
        External_Name => "cbrt";

   function cbrtl (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:437
   with Import => True, 
        Convention => C, 
        External_Name => "cbrtl";

   function hypotf (arg1 : float; arg2 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:439
   with Import => True, 
        Convention => C, 
        External_Name => "hypotf";

   function hypot (arg1 : double; arg2 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:440
   with Import => True, 
        Convention => C, 
        External_Name => "hypot";

   function hypotl (arg1 : long_double; arg2 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:441
   with Import => True, 
        Convention => C, 
        External_Name => "hypotl";

   function powf (arg1 : float; arg2 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:443
   with Import => True, 
        Convention => C, 
        External_Name => "powf";

   function pow (arg1 : double; arg2 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:444
   with Import => True, 
        Convention => C, 
        External_Name => "pow";

   function powl (arg1 : long_double; arg2 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:445
   with Import => True, 
        Convention => C, 
        External_Name => "powl";

   function sqrtf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:447
   with Import => True, 
        Convention => C, 
        External_Name => "sqrtf";

   function sqrt (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:448
   with Import => True, 
        Convention => C, 
        External_Name => "sqrt";

   function sqrtl (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:449
   with Import => True, 
        Convention => C, 
        External_Name => "sqrtl";

   function erff (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:451
   with Import => True, 
        Convention => C, 
        External_Name => "erff";

   function erf (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:452
   with Import => True, 
        Convention => C, 
        External_Name => "erf";

   function erfl (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:453
   with Import => True, 
        Convention => C, 
        External_Name => "erfl";

   function erfcf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:455
   with Import => True, 
        Convention => C, 
        External_Name => "erfcf";

   function erfc (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:456
   with Import => True, 
        Convention => C, 
        External_Name => "erfc";

   function erfcl (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:457
   with Import => True, 
        Convention => C, 
        External_Name => "erfcl";

   function lgammaf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:462
   with Import => True, 
        Convention => C, 
        External_Name => "lgammaf";

   function lgamma (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:463
   with Import => True, 
        Convention => C, 
        External_Name => "lgamma";

   function lgammal (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:464
   with Import => True, 
        Convention => C, 
        External_Name => "lgammal";

   function tgammaf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:466
   with Import => True, 
        Convention => C, 
        External_Name => "tgammaf";

   function tgamma (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:467
   with Import => True, 
        Convention => C, 
        External_Name => "tgamma";

   function tgammal (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:468
   with Import => True, 
        Convention => C, 
        External_Name => "tgammal";

   function ceilf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:470
   with Import => True, 
        Convention => C, 
        External_Name => "ceilf";

   function ceil (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:471
   with Import => True, 
        Convention => C, 
        External_Name => "ceil";

   function ceill (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:472
   with Import => True, 
        Convention => C, 
        External_Name => "ceill";

   function floorf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:474
   with Import => True, 
        Convention => C, 
        External_Name => "floorf";

   function floor (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:475
   with Import => True, 
        Convention => C, 
        External_Name => "floor";

   function floorl (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:476
   with Import => True, 
        Convention => C, 
        External_Name => "floorl";

   function nearbyintf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:478
   with Import => True, 
        Convention => C, 
        External_Name => "nearbyintf";

   function nearbyint (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:479
   with Import => True, 
        Convention => C, 
        External_Name => "nearbyint";

   function nearbyintl (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:480
   with Import => True, 
        Convention => C, 
        External_Name => "nearbyintl";

   function rintf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:482
   with Import => True, 
        Convention => C, 
        External_Name => "rintf";

   function rint (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:483
   with Import => True, 
        Convention => C, 
        External_Name => "rint";

   function rintl (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:484
   with Import => True, 
        Convention => C, 
        External_Name => "rintl";

   function lrintf (arg1 : float) return long  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:486
   with Import => True, 
        Convention => C, 
        External_Name => "lrintf";

   function lrint (arg1 : double) return long  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:487
   with Import => True, 
        Convention => C, 
        External_Name => "lrint";

   function lrintl (arg1 : long_double) return long  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:488
   with Import => True, 
        Convention => C, 
        External_Name => "lrintl";

   function roundf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:490
   with Import => True, 
        Convention => C, 
        External_Name => "roundf";

   function round (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:491
   with Import => True, 
        Convention => C, 
        External_Name => "round";

   function roundl (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:492
   with Import => True, 
        Convention => C, 
        External_Name => "roundl";

   function lroundf (arg1 : float) return long  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:494
   with Import => True, 
        Convention => C, 
        External_Name => "lroundf";

   function lround (arg1 : double) return long  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:495
   with Import => True, 
        Convention => C, 
        External_Name => "lround";

   function lroundl (arg1 : long_double) return long  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:496
   with Import => True, 
        Convention => C, 
        External_Name => "lroundl";

   function llrintf (arg1 : float) return Long_Long_Integer  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:501
   with Import => True, 
        Convention => C, 
        External_Name => "llrintf";

   function llrint (arg1 : double) return Long_Long_Integer  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:502
   with Import => True, 
        Convention => C, 
        External_Name => "llrint";

   function llrintl (arg1 : long_double) return Long_Long_Integer  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:503
   with Import => True, 
        Convention => C, 
        External_Name => "llrintl";

   function llroundf (arg1 : float) return Long_Long_Integer  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:505
   with Import => True, 
        Convention => C, 
        External_Name => "llroundf";

   function llround (arg1 : double) return Long_Long_Integer  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:506
   with Import => True, 
        Convention => C, 
        External_Name => "llround";

   function llroundl (arg1 : long_double) return Long_Long_Integer  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:507
   with Import => True, 
        Convention => C, 
        External_Name => "llroundl";

   function truncf (arg1 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:510
   with Import => True, 
        Convention => C, 
        External_Name => "truncf";

   function trunc (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:511
   with Import => True, 
        Convention => C, 
        External_Name => "trunc";

   function truncl (arg1 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:512
   with Import => True, 
        Convention => C, 
        External_Name => "truncl";

   function fmodf (arg1 : float; arg2 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:514
   with Import => True, 
        Convention => C, 
        External_Name => "fmodf";

   function fmod (arg1 : double; arg2 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:515
   with Import => True, 
        Convention => C, 
        External_Name => "fmod";

   function fmodl (arg1 : long_double; arg2 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:516
   with Import => True, 
        Convention => C, 
        External_Name => "fmodl";

   function remainderf (arg1 : float; arg2 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:518
   with Import => True, 
        Convention => C, 
        External_Name => "remainderf";

   function remainder (arg1 : double; arg2 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:519
   with Import => True, 
        Convention => C, 
        External_Name => "remainder";

   function remainderl (arg1 : long_double; arg2 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:520
   with Import => True, 
        Convention => C, 
        External_Name => "remainderl";

   function remquof
     (arg1 : float;
      arg2 : float;
      arg3 : access int) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:522
   with Import => True, 
        Convention => C, 
        External_Name => "remquof";

   function remquo
     (arg1 : double;
      arg2 : double;
      arg3 : access int) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:523
   with Import => True, 
        Convention => C, 
        External_Name => "remquo";

   function remquol
     (arg1 : long_double;
      arg2 : long_double;
      arg3 : access int) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:524
   with Import => True, 
        Convention => C, 
        External_Name => "remquol";

   function copysignf (arg1 : float; arg2 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:526
   with Import => True, 
        Convention => C, 
        External_Name => "copysignf";

   function copysign (arg1 : double; arg2 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:527
   with Import => True, 
        Convention => C, 
        External_Name => "copysign";

   function copysignl (arg1 : long_double; arg2 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:528
   with Import => True, 
        Convention => C, 
        External_Name => "copysignl";

   function nanf (arg1 : Interfaces.C.Strings.chars_ptr) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:530
   with Import => True, 
        Convention => C, 
        External_Name => "nanf";

   function nan (arg1 : Interfaces.C.Strings.chars_ptr) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:531
   with Import => True, 
        Convention => C, 
        External_Name => "nan";

   function nanl (arg1 : Interfaces.C.Strings.chars_ptr) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:532
   with Import => True, 
        Convention => C, 
        External_Name => "nanl";

   function nextafterf (arg1 : float; arg2 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:534
   with Import => True, 
        Convention => C, 
        External_Name => "nextafterf";

   function nextafter (arg1 : double; arg2 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:535
   with Import => True, 
        Convention => C, 
        External_Name => "nextafter";

   function nextafterl (arg1 : long_double; arg2 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:536
   with Import => True, 
        Convention => C, 
        External_Name => "nextafterl";

   function nexttoward (arg1 : double; arg2 : long_double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:538
   with Import => True, 
        Convention => C, 
        External_Name => "nexttoward";

   function nexttowardf (arg1 : float; arg2 : long_double) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:539
   with Import => True, 
        Convention => C, 
        External_Name => "nexttowardf";

   function nexttowardl (arg1 : long_double; arg2 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:540
   with Import => True, 
        Convention => C, 
        External_Name => "nexttowardl";

   function fdimf (arg1 : float; arg2 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:542
   with Import => True, 
        Convention => C, 
        External_Name => "fdimf";

   function fdim (arg1 : double; arg2 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:543
   with Import => True, 
        Convention => C, 
        External_Name => "fdim";

   function fdiml (arg1 : long_double; arg2 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:544
   with Import => True, 
        Convention => C, 
        External_Name => "fdiml";

   function fmaxf (arg1 : float; arg2 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:546
   with Import => True, 
        Convention => C, 
        External_Name => "fmaxf";

   function fmax (arg1 : double; arg2 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:547
   with Import => True, 
        Convention => C, 
        External_Name => "fmax";

   function fmaxl (arg1 : long_double; arg2 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:548
   with Import => True, 
        Convention => C, 
        External_Name => "fmaxl";

   function fminf (arg1 : float; arg2 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:550
   with Import => True, 
        Convention => C, 
        External_Name => "fminf";

   function fmin (arg1 : double; arg2 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:551
   with Import => True, 
        Convention => C, 
        External_Name => "fmin";

   function fminl (arg1 : long_double; arg2 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:552
   with Import => True, 
        Convention => C, 
        External_Name => "fminl";

   function fmaf
     (arg1 : float;
      arg2 : float;
      arg3 : float) return float  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:554
   with Import => True, 
        Convention => C, 
        External_Name => "fmaf";

   function fma
     (arg1 : double;
      arg2 : double;
      arg3 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:555
   with Import => True, 
        Convention => C, 
        External_Name => "fma";

   function fmal
     (arg1 : long_double;
      arg2 : long_double;
      arg3 : long_double) return long_double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:556
   with Import => True, 
        Convention => C, 
        External_Name => "fmal";

   --  skipped func __inff

   --  skipped func __inf

   --  skipped func __infl

   --  skipped func __nan

   --  skipped func __exp10f

   --  skipped func __exp10

   --  skipped func __cospif

   --  skipped func __cospi

   --  skipped func __sinpif

   --  skipped func __sinpi

   --  skipped func __tanpif

   --  skipped func __tanpi

   type uu_float2 is record
      uu_sinval : aliased float;  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:659
      uu_cosval : aliased float;  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:659
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:659

   type uu_double2 is record
      uu_sinval : aliased double;  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:660
      uu_cosval : aliased double;  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:660
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:660

   --  skipped func __sincosf_stret

   --  skipped func __sincos_stret

   --  skipped func __sincospif_stret

   --  skipped func __sincospi_stret

   --  skipped func __sincosf

   --  skipped func __sincos

   --  skipped func __sincospif

   --  skipped func __sincospi

   function j0 (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:693
   with Import => True, 
        Convention => C, 
        External_Name => "j0";

   function j1 (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:694
   with Import => True, 
        Convention => C, 
        External_Name => "j1";

   function jn (arg1 : int; arg2 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:695
   with Import => True, 
        Convention => C, 
        External_Name => "jn";

   function y0 (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:696
   with Import => True, 
        Convention => C, 
        External_Name => "y0";

   function y1 (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:697
   with Import => True, 
        Convention => C, 
        External_Name => "y1";

   function yn (arg1 : int; arg2 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:698
   with Import => True, 
        Convention => C, 
        External_Name => "yn";

   function scalb (arg1 : double; arg2 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:699
   with Import => True, 
        Convention => C, 
        External_Name => "scalb";

   signgam : aliased int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:700
   with Import => True, 
        Convention => C, 
        External_Name => "signgam";

   function rinttol (arg1 : double) return long  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:758
   with Import => True, 
        Convention => C, 
        External_Name => "rinttol";

   function roundtol (arg1 : double) return long  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:761
   with Import => True, 
        Convention => C, 
        External_Name => "roundtol";

   function drem (arg1 : double; arg2 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:764
   with Import => True, 
        Convention => C, 
        External_Name => "drem";

   function finite (arg1 : double) return int  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:767
   with Import => True, 
        Convention => C, 
        External_Name => "finite";

   function gamma (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:770
   with Import => True, 
        Convention => C, 
        External_Name => "gamma";

   function significand (arg1 : double) return double  -- /opt/gcc-13.1.0/lib/gcc/x86_64-apple-darwin21/13.1.0/include-fixed/math.h:773
   with Import => True, 
        Convention => C, 
        External_Name => "significand";

end math_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
