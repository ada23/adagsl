pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package gsl_gsl_const_h is

end gsl_gsl_const_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
