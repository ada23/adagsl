pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
limited with gsl_gsl_permutation_h;
limited with gsl_gsl_matrix_complex_float_h;

package gsl_gsl_permute_matrix_complex_float_h is

   function gsl_permute_matrix_complex_float (p : access constant gsl_gsl_permutation_h.gsl_permutation_struct; A : access gsl_gsl_matrix_complex_float_h.gsl_matrix_complex_float) return int  -- /opt/homebrew/include/gsl/gsl_permute_matrix_complex_float.h:40
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_permute_matrix_complex_float";

end gsl_gsl_permute_matrix_complex_float_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
