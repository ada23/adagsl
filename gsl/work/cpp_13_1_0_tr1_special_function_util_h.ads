pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package cpp_13_1_0_tr1_special_function_util_h is

end cpp_13_1_0_tr1_special_function_util_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
