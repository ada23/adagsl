pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package gsl_gsl_fft_h is

   subtype gsl_fft_direction is int;
   gsl_fft_direction_gsl_fft_forward : constant gsl_fft_direction := -1;
   gsl_fft_direction_gsl_fft_backward : constant gsl_fft_direction := 1;  -- /opt/homebrew/include/gsl/gsl_fft.h:41

end gsl_gsl_fft_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
