pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with sys_utypes_usize_t_h;
limited with ustdio_h;
with Interfaces.C.Strings;

package gsl_gsl_combination_h is

   type gsl_combination_struct is record
      n : aliased sys_utypes_usize_t_h.size_t;  -- /opt/homebrew/include/gsl/gsl_combination.h:44
      k : aliased sys_utypes_usize_t_h.size_t;  -- /opt/homebrew/include/gsl/gsl_combination.h:45
      data : access sys_utypes_usize_t_h.size_t;  -- /opt/homebrew/include/gsl/gsl_combination.h:46
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/gsl/gsl_combination.h:42

   subtype gsl_combination is gsl_combination_struct;  -- /opt/homebrew/include/gsl/gsl_combination.h:49

   function gsl_combination_alloc (n : sys_utypes_usize_t_h.size_t; k : sys_utypes_usize_t_h.size_t) return access gsl_combination  -- /opt/homebrew/include/gsl/gsl_combination.h:51
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_combination_alloc";

   function gsl_combination_calloc (n : sys_utypes_usize_t_h.size_t; k : sys_utypes_usize_t_h.size_t) return access gsl_combination  -- /opt/homebrew/include/gsl/gsl_combination.h:52
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_combination_calloc";

   procedure gsl_combination_init_first (c : access gsl_combination)  -- /opt/homebrew/include/gsl/gsl_combination.h:53
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_combination_init_first";

   procedure gsl_combination_init_last (c : access gsl_combination)  -- /opt/homebrew/include/gsl/gsl_combination.h:54
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_combination_init_last";

   procedure gsl_combination_free (c : access gsl_combination)  -- /opt/homebrew/include/gsl/gsl_combination.h:55
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_combination_free";

   function gsl_combination_memcpy (dest : access gsl_combination; src : access constant gsl_combination) return int  -- /opt/homebrew/include/gsl/gsl_combination.h:56
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_combination_memcpy";

   function gsl_combination_fread (stream : access ustdio_h.uu_sFILE; c : access gsl_combination) return int  -- /opt/homebrew/include/gsl/gsl_combination.h:58
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_combination_fread";

   function gsl_combination_fwrite (stream : access ustdio_h.uu_sFILE; c : access constant gsl_combination) return int  -- /opt/homebrew/include/gsl/gsl_combination.h:59
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_combination_fwrite";

   function gsl_combination_fscanf (stream : access ustdio_h.uu_sFILE; c : access gsl_combination) return int  -- /opt/homebrew/include/gsl/gsl_combination.h:60
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_combination_fscanf";

   function gsl_combination_fprintf
     (stream : access ustdio_h.uu_sFILE;
      c : access constant gsl_combination;
      format : Interfaces.C.Strings.chars_ptr) return int  -- /opt/homebrew/include/gsl/gsl_combination.h:61
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_combination_fprintf";

   function gsl_combination_n (c : access constant gsl_combination) return sys_utypes_usize_t_h.size_t  -- /opt/homebrew/include/gsl/gsl_combination.h:63
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_combination_n";

   function gsl_combination_k (c : access constant gsl_combination) return sys_utypes_usize_t_h.size_t  -- /opt/homebrew/include/gsl/gsl_combination.h:64
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_combination_k";

   function gsl_combination_data (c : access constant gsl_combination) return access sys_utypes_usize_t_h.size_t  -- /opt/homebrew/include/gsl/gsl_combination.h:65
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_combination_data";

   function gsl_combination_valid (c : access gsl_combination) return int  -- /opt/homebrew/include/gsl/gsl_combination.h:67
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_combination_valid";

   function gsl_combination_next (c : access gsl_combination) return int  -- /opt/homebrew/include/gsl/gsl_combination.h:68
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_combination_next";

   function gsl_combination_prev (c : access gsl_combination) return int  -- /opt/homebrew/include/gsl/gsl_combination.h:69
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_combination_prev";

   function gsl_combination_get (c : access constant gsl_combination; i : sys_utypes_usize_t_h.size_t) return sys_utypes_usize_t_h.size_t  -- /opt/homebrew/include/gsl/gsl_combination.h:71
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_combination_get";

end gsl_gsl_combination_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
