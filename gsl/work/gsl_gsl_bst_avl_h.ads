pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with System;
limited with gsl_gsl_bst_types_h;
with sys_utypes_usize_t_h;

package gsl_gsl_bst_avl_h is

   GSL_BST_AVL_MAX_HEIGHT : constant := 32;  --  /opt/homebrew/include/gsl/gsl_bst_avl.h:39

   type gsl_bst_avl_node;
   type anon_array11899 is array (0 .. 1) of access gsl_bst_avl_node;
   type gsl_bst_avl_node is record
      avl_link : anon_array11899;  -- /opt/homebrew/include/gsl/gsl_bst_avl.h:45
      avl_data : System.Address;  -- /opt/homebrew/include/gsl/gsl_bst_avl.h:46
      avl_balance : aliased signed_char;  -- /opt/homebrew/include/gsl/gsl_bst_avl.h:47
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/gsl/gsl_bst_avl.h:43

   type gsl_bst_avl_table is record
      avl_root : access gsl_bst_avl_node;  -- /opt/homebrew/include/gsl/gsl_bst_avl.h:53
      avl_compare : access function
           (arg1 : System.Address;
            arg2 : System.Address;
            arg3 : System.Address) return int;  -- /opt/homebrew/include/gsl/gsl_bst_avl.h:54
      avl_param : System.Address;  -- /opt/homebrew/include/gsl/gsl_bst_avl.h:55
      avl_alloc : access constant gsl_gsl_bst_types_h.gsl_bst_allocator;  -- /opt/homebrew/include/gsl/gsl_bst_avl.h:56
      avl_count : aliased sys_utypes_usize_t_h.size_t;  -- /opt/homebrew/include/gsl/gsl_bst_avl.h:57
      avl_generation : aliased unsigned_long;  -- /opt/homebrew/include/gsl/gsl_bst_avl.h:58
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/gsl/gsl_bst_avl.h:59

   type anon_array11910 is array (0 .. 31) of access gsl_bst_avl_node;
   type gsl_bst_avl_traverser is record
      avl_table : access constant gsl_bst_avl_table;  -- /opt/homebrew/include/gsl/gsl_bst_avl.h:64
      avl_node : access gsl_bst_avl_node;  -- /opt/homebrew/include/gsl/gsl_bst_avl.h:65
      avl_stack : anon_array11910;  -- /opt/homebrew/include/gsl/gsl_bst_avl.h:66
      avl_height : aliased sys_utypes_usize_t_h.size_t;  -- /opt/homebrew/include/gsl/gsl_bst_avl.h:67
      avl_generation : aliased unsigned_long;  -- /opt/homebrew/include/gsl/gsl_bst_avl.h:68
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/gsl/gsl_bst_avl.h:69

end gsl_gsl_bst_avl_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
