pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package sys_utypes_uu_int8_t_h is

   subtype u_int8_t is unsigned_char;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/_types/_u_int8_t.h:30

end sys_utypes_uu_int8_t_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
