pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with sys_utypes_usize_t_h;
limited with ustdio_h;
with Interfaces.C.Strings;

package gsl_gsl_block_complex_float_h is

   type gsl_block_complex_float_struct is record
      size : aliased sys_utypes_usize_t_h.size_t;  -- /opt/homebrew/include/gsl/gsl_block_complex_float.h:40
      data : access float;  -- /opt/homebrew/include/gsl/gsl_block_complex_float.h:41
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/gsl/gsl_block_complex_float.h:38

   subtype gsl_block_complex_float is gsl_block_complex_float_struct;  -- /opt/homebrew/include/gsl/gsl_block_complex_float.h:44

   function gsl_block_complex_float_alloc (n : sys_utypes_usize_t_h.size_t) return access gsl_block_complex_float  -- /opt/homebrew/include/gsl/gsl_block_complex_float.h:46
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_block_complex_float_alloc";

   function gsl_block_complex_float_calloc (n : sys_utypes_usize_t_h.size_t) return access gsl_block_complex_float  -- /opt/homebrew/include/gsl/gsl_block_complex_float.h:47
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_block_complex_float_calloc";

   procedure gsl_block_complex_float_free (b : access gsl_block_complex_float)  -- /opt/homebrew/include/gsl/gsl_block_complex_float.h:48
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_block_complex_float_free";

   function gsl_block_complex_float_fread (stream : access ustdio_h.uu_sFILE; b : access gsl_block_complex_float) return int  -- /opt/homebrew/include/gsl/gsl_block_complex_float.h:50
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_block_complex_float_fread";

   function gsl_block_complex_float_fwrite (stream : access ustdio_h.uu_sFILE; b : access constant gsl_block_complex_float) return int  -- /opt/homebrew/include/gsl/gsl_block_complex_float.h:51
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_block_complex_float_fwrite";

   function gsl_block_complex_float_fscanf (stream : access ustdio_h.uu_sFILE; b : access gsl_block_complex_float) return int  -- /opt/homebrew/include/gsl/gsl_block_complex_float.h:52
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_block_complex_float_fscanf";

   function gsl_block_complex_float_fprintf
     (stream : access ustdio_h.uu_sFILE;
      b : access constant gsl_block_complex_float;
      format : Interfaces.C.Strings.chars_ptr) return int  -- /opt/homebrew/include/gsl/gsl_block_complex_float.h:53
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_block_complex_float_fprintf";

   function gsl_block_complex_float_raw_fread
     (stream : access ustdio_h.uu_sFILE;
      b : access float;
      n : sys_utypes_usize_t_h.size_t;
      stride : sys_utypes_usize_t_h.size_t) return int  -- /opt/homebrew/include/gsl/gsl_block_complex_float.h:55
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_block_complex_float_raw_fread";

   function gsl_block_complex_float_raw_fwrite
     (stream : access ustdio_h.uu_sFILE;
      b : access float;
      n : sys_utypes_usize_t_h.size_t;
      stride : sys_utypes_usize_t_h.size_t) return int  -- /opt/homebrew/include/gsl/gsl_block_complex_float.h:56
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_block_complex_float_raw_fwrite";

   function gsl_block_complex_float_raw_fscanf
     (stream : access ustdio_h.uu_sFILE;
      b : access float;
      n : sys_utypes_usize_t_h.size_t;
      stride : sys_utypes_usize_t_h.size_t) return int  -- /opt/homebrew/include/gsl/gsl_block_complex_float.h:57
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_block_complex_float_raw_fscanf";

   function gsl_block_complex_float_raw_fprintf
     (stream : access ustdio_h.uu_sFILE;
      b : access float;
      n : sys_utypes_usize_t_h.size_t;
      stride : sys_utypes_usize_t_h.size_t;
      format : Interfaces.C.Strings.chars_ptr) return int  -- /opt/homebrew/include/gsl/gsl_block_complex_float.h:58
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_block_complex_float_raw_fprintf";

   function gsl_block_complex_float_size (b : access constant gsl_block_complex_float) return sys_utypes_usize_t_h.size_t  -- /opt/homebrew/include/gsl/gsl_block_complex_float.h:60
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_block_complex_float_size";

   function gsl_block_complex_float_data (b : access constant gsl_block_complex_float) return access float  -- /opt/homebrew/include/gsl/gsl_block_complex_float.h:61
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_block_complex_float_data";

end gsl_gsl_block_complex_float_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
