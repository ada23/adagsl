pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package i386_signal_h is

   subtype sig_atomic_t is int;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/i386/signal.h:41

end i386_signal_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
