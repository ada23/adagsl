pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package gsl_gsl_types_h is

   --  unsupported macro: GSL_VAR extern
end gsl_gsl_types_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
