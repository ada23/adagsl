pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings;
with sys_utypes_usize_t_h;
with System;

package gsl_gsl_qrng_h is

   type gsl_qrng_type is record
      name : Interfaces.C.Strings.chars_ptr;  -- /opt/homebrew/include/gsl/gsl_qrng.h:30
      max_dimension : aliased unsigned;  -- /opt/homebrew/include/gsl/gsl_qrng.h:31
      state_size : access function (arg1 : unsigned) return sys_utypes_usize_t_h.size_t;  -- /opt/homebrew/include/gsl/gsl_qrng.h:32
      init_state : access function (arg1 : System.Address; arg2 : unsigned) return int;  -- /opt/homebrew/include/gsl/gsl_qrng.h:33
      get : access function
           (arg1 : System.Address;
            arg2 : unsigned;
            arg3 : access double) return int;  -- /opt/homebrew/include/gsl/gsl_qrng.h:34
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/gsl/gsl_qrng.h:36

   type gsl_qrng is record
      c_type : access constant gsl_qrng_type;  -- /opt/homebrew/include/gsl/gsl_qrng.h:44
      dimension : aliased unsigned;  -- /opt/homebrew/include/gsl/gsl_qrng.h:45
      state_size : aliased sys_utypes_usize_t_h.size_t;  -- /opt/homebrew/include/gsl/gsl_qrng.h:46
      state : System.Address;  -- /opt/homebrew/include/gsl/gsl_qrng.h:47
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/gsl/gsl_qrng.h:49

   gsl_qrng_niederreiter_2 : access constant gsl_qrng_type  -- /opt/homebrew/include/gsl/gsl_qrng.h:54
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_qrng_niederreiter_2";

   gsl_qrng_sobol : access constant gsl_qrng_type  -- /opt/homebrew/include/gsl/gsl_qrng.h:55
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_qrng_sobol";

   gsl_qrng_halton : access constant gsl_qrng_type  -- /opt/homebrew/include/gsl/gsl_qrng.h:56
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_qrng_halton";

   gsl_qrng_reversehalton : access constant gsl_qrng_type  -- /opt/homebrew/include/gsl/gsl_qrng.h:57
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_qrng_reversehalton";

   function gsl_qrng_alloc (T : access constant gsl_qrng_type; dimension : unsigned) return access gsl_qrng  -- /opt/homebrew/include/gsl/gsl_qrng.h:64
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_qrng_alloc";

   function gsl_qrng_memcpy (dest : access gsl_qrng; src : access constant gsl_qrng) return int  -- /opt/homebrew/include/gsl/gsl_qrng.h:68
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_qrng_memcpy";

   function gsl_qrng_clone (q : access constant gsl_qrng) return access gsl_qrng  -- /opt/homebrew/include/gsl/gsl_qrng.h:72
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_qrng_clone";

   procedure gsl_qrng_free (q : access gsl_qrng)  -- /opt/homebrew/include/gsl/gsl_qrng.h:76
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_qrng_free";

   procedure gsl_qrng_init (q : access gsl_qrng)  -- /opt/homebrew/include/gsl/gsl_qrng.h:80
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_qrng_init";

   function gsl_qrng_name (q : access constant gsl_qrng) return Interfaces.C.Strings.chars_ptr  -- /opt/homebrew/include/gsl/gsl_qrng.h:84
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_qrng_name";

   function gsl_qrng_size (q : access constant gsl_qrng) return sys_utypes_usize_t_h.size_t  -- /opt/homebrew/include/gsl/gsl_qrng.h:90
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_qrng_size";

   function gsl_qrng_state (q : access constant gsl_qrng) return System.Address  -- /opt/homebrew/include/gsl/gsl_qrng.h:93
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_qrng_state";

   function gsl_qrng_get (q : access constant gsl_qrng; x : access double) return int  -- /opt/homebrew/include/gsl/gsl_qrng.h:97
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_qrng_get";

end gsl_gsl_qrng_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
