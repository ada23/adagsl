pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package cpp_13_1_0_bits_utility_h is

   package Class_in_place_t is
      type in_place_t is limited record
         null;
      end record
      with Import => True,
           Convention => CPP;

      function New_in_place_t return in_place_t;  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/utility.h:194
      pragma CPP_Constructor (New_in_place_t, "_ZNSt10in_place_tC1Ev");
   end;
   use Class_in_place_t;
   in_place : aliased constant in_place_t  -- /opt/gcc-13.1.0/include/c++/13.1.0/bits/utility.h:197
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt8in_place";

end cpp_13_1_0_bits_utility_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
