pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with System;

package cpp_13_1_0_x86_64_apple_darwin21_bits_cppconfig_h is

   subtype size_t is unsigned_long;  -- /opt/gcc-13.1.0/include/c++/13.1.0/x86_64-apple-darwin21/bits/c++config.h:308

   subtype ptrdiff_t is long;  -- /opt/gcc-13.1.0/include/c++/13.1.0/x86_64-apple-darwin21/bits/c++config.h:309

   subtype nullptr_t is System.Address;  -- /opt/gcc-13.1.0/include/c++/13.1.0/x86_64-apple-darwin21/bits/c++config.h:312

   --  skipped func __terminate

   procedure c_terminate  -- /opt/gcc-13.1.0/include/c++/13.1.0/x86_64-apple-darwin21/bits/c++config.h:321
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt9terminatev";

   --  skipped func __is_constant_evaluated

end cpp_13_1_0_x86_64_apple_darwin21_bits_cppconfig_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
