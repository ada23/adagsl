pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with sys_utypes_usize_t_h;
with gsl_gsl_cblas_h;

package gsl_gsl_blas_types_h is

   subtype CBLAS_INDEX_t is sys_utypes_usize_t_h.size_t;  -- /opt/homebrew/include/gsl/gsl_blas_types.h:42

   subtype CBLAS_ORDER_t is gsl_gsl_cblas_h.CBLAS_ORDER;  -- /opt/homebrew/include/gsl/gsl_blas_types.h:43

   subtype CBLAS_TRANSPOSE_t is gsl_gsl_cblas_h.CBLAS_TRANSPOSE;  -- /opt/homebrew/include/gsl/gsl_blas_types.h:44

   subtype CBLAS_UPLO_t is gsl_gsl_cblas_h.CBLAS_UPLO;  -- /opt/homebrew/include/gsl/gsl_blas_types.h:45

   subtype CBLAS_DIAG_t is gsl_gsl_cblas_h.CBLAS_DIAG;  -- /opt/homebrew/include/gsl/gsl_blas_types.h:46

   subtype CBLAS_SIDE_t is gsl_gsl_cblas_h.CBLAS_SIDE;  -- /opt/homebrew/include/gsl/gsl_blas_types.h:47

end gsl_gsl_blas_types_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
