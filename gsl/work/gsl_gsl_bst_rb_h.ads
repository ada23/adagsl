pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with System;
limited with gsl_gsl_bst_types_h;
with sys_utypes_usize_t_h;

package gsl_gsl_bst_rb_h is

   GSL_BST_RB_MAX_HEIGHT : constant := 48;  --  /opt/homebrew/include/gsl/gsl_bst_rb.h:39

   type gsl_bst_rb_node;
   type anon_array11914 is array (0 .. 1) of access gsl_bst_rb_node;
   type gsl_bst_rb_node is record
      rb_link : anon_array11914;  -- /opt/homebrew/include/gsl/gsl_bst_rb.h:45
      rb_data : System.Address;  -- /opt/homebrew/include/gsl/gsl_bst_rb.h:46
      rb_color : aliased unsigned_char;  -- /opt/homebrew/include/gsl/gsl_bst_rb.h:47
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/gsl/gsl_bst_rb.h:43

   type gsl_bst_rb_table is record
      rb_root : access gsl_bst_rb_node;  -- /opt/homebrew/include/gsl/gsl_bst_rb.h:53
      rb_compare : access function
           (arg1 : System.Address;
            arg2 : System.Address;
            arg3 : System.Address) return int;  -- /opt/homebrew/include/gsl/gsl_bst_rb.h:54
      rb_param : System.Address;  -- /opt/homebrew/include/gsl/gsl_bst_rb.h:55
      rb_alloc : access constant gsl_gsl_bst_types_h.gsl_bst_allocator;  -- /opt/homebrew/include/gsl/gsl_bst_rb.h:56
      rb_count : aliased sys_utypes_usize_t_h.size_t;  -- /opt/homebrew/include/gsl/gsl_bst_rb.h:57
      rb_generation : aliased unsigned_long;  -- /opt/homebrew/include/gsl/gsl_bst_rb.h:58
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/gsl/gsl_bst_rb.h:59

   type anon_array11921 is array (0 .. 47) of access gsl_bst_rb_node;
   type gsl_bst_rb_traverser is record
      rb_table : access constant gsl_bst_rb_table;  -- /opt/homebrew/include/gsl/gsl_bst_rb.h:64
      rb_node : access gsl_bst_rb_node;  -- /opt/homebrew/include/gsl/gsl_bst_rb.h:65
      rb_stack : anon_array11921;  -- /opt/homebrew/include/gsl/gsl_bst_rb.h:66
      rb_height : aliased sys_utypes_usize_t_h.size_t;  -- /opt/homebrew/include/gsl/gsl_bst_rb.h:68
      rb_generation : aliased unsigned_long;  -- /opt/homebrew/include/gsl/gsl_bst_rb.h:69
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/gsl/gsl_bst_rb.h:70

end gsl_gsl_bst_rb_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
