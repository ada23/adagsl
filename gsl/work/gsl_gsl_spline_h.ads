pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
limited with gsl_gsl_interp_h;
with sys_utypes_usize_t_h;
with Interfaces.C.Strings;

package gsl_gsl_spline_h is

   type gsl_spline is record
      interp : access gsl_gsl_interp_h.gsl_interp;  -- /opt/homebrew/include/gsl/gsl_spline.h:40
      x : access double;  -- /opt/homebrew/include/gsl/gsl_spline.h:41
      y : access double;  -- /opt/homebrew/include/gsl/gsl_spline.h:42
      size : aliased sys_utypes_usize_t_h.size_t;  -- /opt/homebrew/include/gsl/gsl_spline.h:43
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/gsl/gsl_spline.h:44

   function gsl_spline_alloc (T : access constant gsl_gsl_interp_h.gsl_interp_type; size : sys_utypes_usize_t_h.size_t) return access gsl_spline  -- /opt/homebrew/include/gsl/gsl_spline.h:47
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_spline_alloc";

   function gsl_spline_init
     (spline : access gsl_spline;
      xa : access double;
      ya : access double;
      size : sys_utypes_usize_t_h.size_t) return int  -- /opt/homebrew/include/gsl/gsl_spline.h:50
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_spline_init";

   function gsl_spline_name (spline : access constant gsl_spline) return Interfaces.C.Strings.chars_ptr  -- /opt/homebrew/include/gsl/gsl_spline.h:52
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_spline_name";

   function gsl_spline_min_size (spline : access constant gsl_spline) return unsigned  -- /opt/homebrew/include/gsl/gsl_spline.h:53
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_spline_min_size";

   function gsl_spline_eval_e
     (spline : access constant gsl_spline;
      x : double;
      a : access gsl_gsl_interp_h.gsl_interp_accel;
      y : access double) return int  -- /opt/homebrew/include/gsl/gsl_spline.h:57
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_spline_eval_e";

   function gsl_spline_eval
     (spline : access constant gsl_spline;
      x : double;
      a : access gsl_gsl_interp_h.gsl_interp_accel) return double  -- /opt/homebrew/include/gsl/gsl_spline.h:61
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_spline_eval";

   function gsl_spline_eval_deriv_e
     (spline : access constant gsl_spline;
      x : double;
      a : access gsl_gsl_interp_h.gsl_interp_accel;
      y : access double) return int  -- /opt/homebrew/include/gsl/gsl_spline.h:64
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_spline_eval_deriv_e";

   function gsl_spline_eval_deriv
     (spline : access constant gsl_spline;
      x : double;
      a : access gsl_gsl_interp_h.gsl_interp_accel) return double  -- /opt/homebrew/include/gsl/gsl_spline.h:70
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_spline_eval_deriv";

   function gsl_spline_eval_deriv2_e
     (spline : access constant gsl_spline;
      x : double;
      a : access gsl_gsl_interp_h.gsl_interp_accel;
      y : access double) return int  -- /opt/homebrew/include/gsl/gsl_spline.h:75
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_spline_eval_deriv2_e";

   function gsl_spline_eval_deriv2
     (spline : access constant gsl_spline;
      x : double;
      a : access gsl_gsl_interp_h.gsl_interp_accel) return double  -- /opt/homebrew/include/gsl/gsl_spline.h:81
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_spline_eval_deriv2";

   function gsl_spline_eval_integ_e
     (spline : access constant gsl_spline;
      a : double;
      b : double;
      acc : access gsl_gsl_interp_h.gsl_interp_accel;
      y : access double) return int  -- /opt/homebrew/include/gsl/gsl_spline.h:86
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_spline_eval_integ_e";

   function gsl_spline_eval_integ
     (spline : access constant gsl_spline;
      a : double;
      b : double;
      acc : access gsl_gsl_interp_h.gsl_interp_accel) return double  -- /opt/homebrew/include/gsl/gsl_spline.h:92
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_spline_eval_integ";

   procedure gsl_spline_free (spline : access gsl_spline)  -- /opt/homebrew/include/gsl/gsl_spline.h:97
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_spline_free";

end gsl_gsl_spline_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
