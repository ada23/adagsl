pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with sys_utypes_usize_t_h;
with System;

package gsl_gsl_bst_types_h is

   --  skipped function type gsl_bst_cmp_function

   type gsl_bst_allocator is record
      alloc : access function (arg1 : sys_utypes_usize_t_h.size_t; arg2 : System.Address) return System.Address;  -- /opt/homebrew/include/gsl/gsl_bst_types.h:42
      free : access procedure (arg1 : System.Address; arg2 : System.Address);  -- /opt/homebrew/include/gsl/gsl_bst_types.h:43
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/gsl/gsl_bst_types.h:44

end gsl_gsl_bst_types_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
