pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package cpp_13_1_0_tr1_hypergeometric_tcc is

end cpp_13_1_0_tr1_hypergeometric_tcc;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
