pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package utypes_uuintmax_t_h is

   subtype uintmax_t is unsigned_long;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/_types/_uintmax_t.h:32

end utypes_uuintmax_t_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
