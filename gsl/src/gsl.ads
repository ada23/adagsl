with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings;
with Interfaces.C_Streams;
with Ada.Text_IO;  use Ada.Text_IO;
with Ada.Text_IO.Complex_IO;
with Ada.Numerics.Generic_Elementary_Functions;
with Ada.Numerics.Generic_Complex_Types;
with Ada.Numerics.Generic_Complex_Elementary_Functions;

with Ada.Numerics.Long_Real_Arrays; use Ada.Numerics.Long_Real_Arrays;
with Ada.Numerics.Real_Arrays;

package gsl is

   package ICS renames Interfaces.C_Streams;
   function Error (errno : int) return String;

   type double_array is array (Natural range <>) of double;
   type double_matrix is array (Natural range <>, Natural range <>) of double;

   function To_Ada
     (c : double_array; len : size_t)
      return Ada.Numerics.Long_Real_Arrays.Real_Vector;
   procedure To_C
     (a   : Ada.Numerics.Long_Real_Arrays.Real_Vector; c : out double_array;
      len : out size_t);

   procedure Sep;
   subtype Long_Float_array is double_array;

   GSL_SUCCESS : constant := 0;

   package Int_Text_Io is new Ada.Text_IO.Integer_IO (int);
   package double_text_Io is new Ada.Text_IO.Float_IO (double);
   package size_t_text_Io is new Ada.Text_IO.Modular_IO (size_t);
   package double_elementary_functions is new Ada.Numerics
     .Generic_Elementary_Functions
     (double);
   package double_complex_types is new Ada.Numerics.Generic_Complex_Types
     (double);
   package double_complex_functions is new Ada.Numerics
     .Generic_Complex_Elementary_Functions
     (double_complex_types);
   package complex_text_io is new Ada.Text_IO.Complex_IO
     (double_complex_types);
end gsl;
