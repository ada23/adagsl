pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C;
with Interfaces.C.Strings;
with Interfaces.C_Streams;
with System;

package gsl.rng is
   --package ICS renames Interfaces.C_Streams;
   --  skipped anonymous struct anon_anon_20

   type gsl_rng_type is record
      name : Interfaces.C.Strings.chars_ptr;  -- /usr/include/gsl/gsl_rng.h:41
      max  : aliased unsigned_long;  -- /usr/include/gsl/gsl_rng.h:42
      min  : aliased unsigned_long;  -- /usr/include/gsl/gsl_rng.h:43
      size : aliased size_t;  -- /usr/include/gsl/gsl_rng.h:44
      set  : access procedure
        (arg1 : System.Address;
         arg2 : unsigned_long);  -- /usr/include/gsl/gsl_rng.h:45
      get : access function
        (arg1 : System.Address)
         return unsigned_long;  -- /usr/include/gsl/gsl_rng.h:46
      get_double : access function
        (arg1 : System.Address)
         return double;  -- /usr/include/gsl/gsl_rng.h:47
   end record with
      Convention => C_Pass_By_Copy;  -- /usr/include/gsl/gsl_rng.h:49

      --  skipped anonymous struct anon_anon_21

   type gsl_rng is record
      c_type : access constant gsl_rng_type;  -- /usr/include/gsl/gsl_rng.h:53
      state  : System.Address;  -- /usr/include/gsl/gsl_rng.h:54
   end record with
      Convention => C_Pass_By_Copy;  -- /usr/include/gsl/gsl_rng.h:56

   borosh13 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:62
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_borosh13";

   coveyou : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:63
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_coveyou";

   cmrg : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:64
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_cmrg";

   fishman18 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:65
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_fishman18";

   fishman20 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:66
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_fishman20";

   fishman2x : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:67
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_fishman2x";

   gfsr4 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:68
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_gfsr4";

   knuthran : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:69
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_knuthran";

   knuthran2 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:70
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_knuthran2";

   knuthran2002 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:71
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_knuthran2002";

   lecuyer21 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:72
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_lecuyer21";

   minstd : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:73
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_minstd";

   mrg : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:74
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_mrg";

   mt19937 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:75
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_mt19937";

   mt19937_1999 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:76
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_mt19937_1999";

   mt19937_1998 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:77
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_mt19937_1998";

   r250 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:78
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_r250";

   ran0 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:79
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_ran0";

   ran1 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:80
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_ran1";

   ran2 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:81
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_ran2";

   ran3 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:82
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_ran3";

   rand : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:83
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_rand";

   rand48 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:84
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_rand48";

   random128_bsd : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:85
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_random128_bsd";

   random128_glibc2 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:86
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_random128_glibc2";

   random128_libc5 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:87
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_random128_libc5";

   random256_bsd : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:88
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_random256_bsd";

   random256_glibc2 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:89
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_random256_glibc2";

   random256_libc5 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:90
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_random256_libc5";

   random32_bsd : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:91
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_random32_bsd";

   random32_glibc2 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:92
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_random32_glibc2";

   random32_libc5 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:93
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_random32_libc5";

   random64_bsd : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:94
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_random64_bsd";

   random64_glibc2 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:95
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_random64_glibc2";

   random64_libc5 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:96
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_random64_libc5";

   random8_bsd : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:97
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_random8_bsd";

   random8_glibc2 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:98
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_random8_glibc2";

   random8_libc5 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:99
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_random8_libc5";

   random_bsd : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:100
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_random_bsd";

   random_glibc2 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:101
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_random_glibc2";

   random_libc5 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:102
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_random_libc5";

   randu : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:103
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_randu";

   ranf : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:104
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_ranf";

   ranlux : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:105
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_ranlux";

   ranlux389 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:106
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_ranlux389";

   ranlxd1 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:107
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_ranlxd1";

   ranlxd2 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:108
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_ranlxd2";

   ranlxs0 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:109
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_ranlxs0";

   ranlxs1 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:110
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_ranlxs1";

   ranlxs2 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:111
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_ranlxs2";

   ranmar : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:112
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_ranmar";

   slatec : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:113
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_slatec";

   taus : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:114
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_taus";

   taus2 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:115
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_taus2";

   taus113 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:116
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_taus113";

   transputer : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:117
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_transputer";

   tt800 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:118
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_tt800";

   uni : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:119
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_uni";

   uni32 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:120
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_uni32";

   vax : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:121
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_vax";

   waterman14 : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:122
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_waterman14";

   zuf : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:123
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_zuf";

   function types_setup return System
     .Address  -- /usr/include/gsl/gsl_rng.h:125
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_types_setup";

   default : access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:127
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_default";

   default_seed : aliased unsigned_long  -- /usr/include/gsl/gsl_rng.h:128
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_default_seed";

   function alloc
     (T : access constant gsl_rng_type)
      return access gsl_rng  -- /usr/include/gsl/gsl_rng.h:130
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_alloc";

   function memcpy
     (dest : access gsl_rng;
      src  : access constant gsl_rng)
      return int  -- /usr/include/gsl/gsl_rng.h:131
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_memcpy";

   function clone
     (r : access constant gsl_rng)
      return access gsl_rng  -- /usr/include/gsl/gsl_rng.h:132
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_clone";

   procedure free
     (r : access gsl_rng)  -- /usr/include/gsl/gsl_rng.h:134
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_free";

   procedure set
     (r    : access constant gsl_rng;
      seed : unsigned_long)  -- /usr/include/gsl/gsl_rng.h:136
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_set";

   function max
     (r : access constant gsl_rng)
      return unsigned_long  -- /usr/include/gsl/gsl_rng.h:137
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_max";

   function min
     (r : access constant gsl_rng)
      return unsigned_long  -- /usr/include/gsl/gsl_rng.h:138
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_min";

   function name
     (r : access constant gsl_rng) return Interfaces.C.Strings
     .chars_ptr  -- /usr/include/gsl/gsl_rng.h:139
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_name";

   function fread
     (stream : ICS.FILEs;
      r      : access gsl_rng)
      return int  -- /usr/include/gsl/gsl_rng.h:141
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_fread";

   function fwrite
     (stream : ICS.FILEs;
      r      : access constant gsl_rng)
      return int  -- /usr/include/gsl/gsl_rng.h:142
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_fwrite";

   function size
     (r : access constant gsl_rng)
      return size_t  -- /usr/include/gsl/gsl_rng.h:144
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_size";

   function state
     (r : access constant gsl_rng) return System
     .Address  -- /usr/include/gsl/gsl_rng.h:145
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_state";

   procedure print_state
     (r : access constant gsl_rng)  -- /usr/include/gsl/gsl_rng.h:147
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_print_state";

   function env_setup
      return access constant gsl_rng_type  -- /usr/include/gsl/gsl_rng.h:149
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_env_setup";

   function get
     (r : access constant gsl_rng)
      return unsigned_long  -- /usr/include/gsl/gsl_rng.h:151
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_get";

   function uniform
     (r : access constant gsl_rng)
      return double  -- /usr/include/gsl/gsl_rng.h:152
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_uniform";

   function uniform_pos
     (r : access constant gsl_rng)
      return double  -- /usr/include/gsl/gsl_rng.h:153
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_uniform_pos";

   function uniform_int
     (r : access constant gsl_rng;
      n : unsigned_long)
      return unsigned_long  -- /usr/include/gsl/gsl_rng.h:154
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rng_uniform_int";

end gsl.rng;
