with System;
package gsl.vector is
   type gsl_vector is record
      size   : aliased size_t;  -- /usr/include/gsl/gsl_vector_double.h:44
      stride : aliased size_t;  -- /usr/include/gsl/gsl_vector_double.h:45
      data   : System.Address;  -- /usr/include/gsl/gsl_vector_double.h:46
      block  : System.Address;  -- /usr/include/gsl/gsl_vector_double.h:47
      owner  : aliased int;  -- /usr/include/gsl/gsl_vector_double.h:48
   end record with
      Convention => C_Pass_By_Copy;  -- /usr/include/gsl/gsl_vector_double.h:50

end gsl.vector;
