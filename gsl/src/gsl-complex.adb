with gsl.complex.math; use gsl.complex.math;
package body gsl.complex is

   overriding function Re (c : gsl_complex) return double is
   begin
      return double_complex_types.Re (double_complex_types.Complex (c));
   end Re;

   overriding function Im (c : gsl_complex) return double is
   begin
      return double_complex_types.Im (double_complex_types.Complex (c));
   end Im;

   function To_Ada
     (c : gsl_complex) return Ada.Numerics.Long_Complex_Types.Complex
   is
      result : Ada.Numerics.Long_Complex_Types.Complex;
   begin
      result.Re := Long_Float (Re (c));
      return result;
   end To_Ada;

   function To_C
     (a : Ada.Numerics.Long_Complex_Types.Complex) return gsl_complex
   is
      result : gsl_complex;
   begin
      result.Re := double (a.Re);
      result.Im := double (a.Im);
      return result;
   end To_C;

   procedure Print (c : gsl_complex) is
      use double_text_io;
   begin
      Put (Re (c));
      Sep;
      Put (Im (c));
      Sep;
      Put (cabs (c));
      Sep;
      Put (arg (c));
      Sep;
   end Print;

end gsl.complex;
