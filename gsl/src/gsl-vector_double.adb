with Ada.Text_IO; use Ada.Text_IO;

package body gsl.vector_double is
   use double_text_Io;
   use size_t_text_Io;
   procedure WriteCSV
     (v  : access constant gsl_vector; fn : String;
      v1 : access constant gsl_vector := null;
      v2 : access constant gsl_vector := null;
      v3 : access constant gsl_vector := null;
      v4 : access constant gsl_vector := null;
      v5 : access constant gsl_vector := null;
      v6 : access constant gsl_vector := null)
   is
      f : File_Type;
   begin
      Create (f, Out_File, fn);
      Set_Output (f);
      for i in 1 .. v.size loop
         Put (i);
         Put (" ; ");
         Put (get (v, i - 1));
         Put (" ; ");
         if v1 /= null then
            Put (get (v1, i - 1));
            Put (" ; ");
         end if;
         if v2 /= null then
            Put (get (v2, i - 1));
            Put (" ; ");
         end if;
         if v3 /= null then
            Put (get (v3, i - 1));
            Put (" ; ");
         end if;
         if v4 /= null then
            Put (get (v4, i - 1));
            Put (" ; ");
         end if;
         if v5 /= null then
            Put (get (v5, i - 1));
            Put (" ; ");
         end if;
         if v6 /= null then
            Put (get (v6, i - 1));
            Put (" ; ");
         end if;
         New_Line;
      end loop;
      Set_Output (Standard_Output);
      Close (f);
   end WriteCSV;

   function fprintf
     (v : access constant gsl_vector; filename : String; format : String)
      return int
   is
      cfilename : aliased String := filename & ASCII.nul;
      cmode     : aliased String := "w" & ASCII.nul;
      file      : Interfaces.C_Streams.FILEs;
      Status    : int;
      Status2   : Integer;
   begin
      file    := Interfaces.C_Streams.fopen (cfilename'Address, cmode'Address);
      Status  := fprintf (file, v, Interfaces.C.Strings.New_String (format));
      Status2 := Interfaces.C_Streams.fclose (file);
      return Status;
   end fprintf;

   function To_Ada
     (c : access constant gsl_vector)
      return Ada.Numerics.Long_Real_Arrays.Real_Vector
   is
      av : Ada.Numerics.Long_Real_Arrays.Real_Vector (1 .. Integer (c.size));
   begin
      for i in 1 .. Integer (c.size) loop
         av (i) := Long_Float (vector_double.get (c, size_t (i)));
      end loop;
      return av;
   end To_Ada;

   function To_C
     (a : Ada.Numerics.Long_Real_Arrays.Real_Vector) return access gsl_vector
   is
      cout : access gsl_vector;
   begin

      cout := alloc (size_t (a'Length));
      for i in 1 .. a'Length loop
         Set (cout, size_t (i), double (a (i)));
      end loop;
      return cout;
   end To_C;

   function To_Ada
     (c : access constant gsl_vector)
      return Ada.Numerics.Real_Arrays.Real_Vector
   is
      av : Ada.Numerics.Real_Arrays.Real_Vector (1 .. Integer (c.size));
   begin
      for i in 1 .. Integer (c.size) loop
         av (i) := Float (get (c, size_t (i - 1)));
      end loop;
      return av;
   end To_Ada;

   function To_C
     (a : Ada.Numerics.Real_Arrays.Real_Vector) return access gsl_vector
   is
      cout : access gsl_vector;
   begin

      cout := alloc (size_t (a'Length));
      for i in 1 .. a'Length loop
         Set (cout, size_t (i), double (a (i)));
      end loop;
      return cout;
   end To_C;


   procedure Set( from : access gsl.vector_double.gsl_vector ; to : in out double_array ) is
   begin
      if to'Length /= from.size
      then
         raise Program_Error with "Incompatible sizes" ;
      end if ;
      declare
         f : array(1..from.size) of double ;
         for f'Address use from.data.all'Address ;
      begin
         for i in 1..from.size
         loop
            to(to'First+Integer(i)-1) := f(i);
         end loop ;
      end ;
   end Set ;

   procedure Set( from : double_array ; to : access gsl.vector_double.gsl_vector ) is
   begin
      if to.size /= from'Length
      then
         raise Program_Error with "Incompatible sizes" ;
      end if ;
      declare
         t : array(1..to.size) of double ;
         for t'address use to.data.all'Address ;
      begin
         for i in 1..from'Length
         loop
            t(size_t(i)) := from(from'First+Integer(i)-1);
         end loop ;
      end ;
   end Set ;

begin
   double_text_Io.Default_exp := 0;
   double_text_Io.Default_aft := 4;
end gsl.vector_double;
