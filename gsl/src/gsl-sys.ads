pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

package gsl.sys is

   function log1p
     (x : double)
      return double  -- /usr/include/gsl/gsl_sys.h:35
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_log1p";

   function expm1
     (x : double)
      return double  -- /usr/include/gsl/gsl_sys.h:36
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_expm1";

   function hypot
     (x : double;
      y : double)
      return double  -- /usr/include/gsl/gsl_sys.h:37
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_hypot";

   function hypot3
     (x : double;
      y : double;
      z : double)
      return double  -- /usr/include/gsl/gsl_sys.h:38
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_hypot3";

   function acosh
     (x : double)
      return double  -- /usr/include/gsl/gsl_sys.h:39
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_acosh";

   function asinh
     (x : double)
      return double  -- /usr/include/gsl/gsl_sys.h:40
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_asinh";

   function atanh
     (x : double)
      return double  -- /usr/include/gsl/gsl_sys.h:41
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_atanh";

   function isnan
     (x : double)
      return int  -- /usr/include/gsl/gsl_sys.h:43
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_isnan";

   function isinf
     (x : double)
      return int  -- /usr/include/gsl/gsl_sys.h:44
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_isinf";

   function finite
     (x : double)
      return int  -- /usr/include/gsl/gsl_sys.h:45
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_finite";

   function nan
      return double  -- /usr/include/gsl/gsl_sys.h:47
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_nan";

   function posinf
      return double  -- /usr/include/gsl/gsl_sys.h:48
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_posinf";

   function neginf
      return double  -- /usr/include/gsl/gsl_sys.h:49
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_neginf";

   function fdiv
     (x : double;
      y : double)
      return double  -- /usr/include/gsl/gsl_sys.h:50
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fdiv";

   function coerce_double
     (x : double)
      return double  -- /usr/include/gsl/gsl_sys.h:52
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_coerce_double";

   function coerce_float
     (x : float)
      return float  -- /usr/include/gsl/gsl_sys.h:53
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_coerce_float";

   function coerce_long_double
     (x : long_double)
      return long_double  -- /usr/include/gsl/gsl_sys.h:54
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_coerce_long_double";

   function ldexp
     (x : double;
      e : int)
      return double  -- /usr/include/gsl/gsl_sys.h:56
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ldexp";

   function frexp
     (x : double;
      e : access int)
      return double  -- /usr/include/gsl/gsl_sys.h:57
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_frexp";

   function fcmp
     (x1      : double;
      x2      : double;
      epsilon : double)
      return int  -- /usr/include/gsl/gsl_sys.h:59
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fcmp";

end gsl.sys;
