pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C.Strings;

package gsl.histogram is

   type gsl_histogram is record
      n : aliased size_t;  -- /opt/homebrew/include/gsl/gsl_histogram.h:39
      c_range : access double;  -- /opt/homebrew/include/gsl/gsl_histogram.h:40
      bin : access double;  -- /opt/homebrew/include/gsl/gsl_histogram.h:41
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/gsl/gsl_histogram.h:42

   type gsl_histogram_pdf is record
      n : aliased size_t;  -- /opt/homebrew/include/gsl/gsl_histogram.h:45
      c_range : access double;  -- /opt/homebrew/include/gsl/gsl_histogram.h:46
      sum : access double;  -- /opt/homebrew/include/gsl/gsl_histogram.h:47
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/gsl/gsl_histogram.h:48

   function alloc (n : size_t) return access gsl_histogram  -- /opt/homebrew/include/gsl/gsl_histogram.h:50
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_alloc";

   function calloc (n : size_t) return access gsl_histogram  -- /opt/homebrew/include/gsl/gsl_histogram.h:52
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_calloc";

   function calloc_uniform
     (n : size_t;
      xmin : double;
      xmax : double) return access gsl_histogram  -- /opt/homebrew/include/gsl/gsl_histogram.h:53
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_calloc_uniform";

   procedure free (h : access gsl_histogram)  -- /opt/homebrew/include/gsl/gsl_histogram.h:54
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_free";

   function increment (h : access gsl_histogram; x : double) return int  -- /opt/homebrew/include/gsl/gsl_histogram.h:55
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_increment";

   function accumulate
     (h : access gsl_histogram;
      x : double;
      weight : double) return int  -- /opt/homebrew/include/gsl/gsl_histogram.h:56
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_accumulate";

   function find
     (h : access constant gsl_histogram;
      x : double;
      i : access size_t) return int  -- /opt/homebrew/include/gsl/gsl_histogram.h:57
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_find";

   function get (h : access constant gsl_histogram; i : size_t) return double  -- /opt/homebrew/include/gsl/gsl_histogram.h:60
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_get";

   function get_range
     (h : access constant gsl_histogram;
      i : size_t;
      lower : access double;
      upper : access double) return int  -- /opt/homebrew/include/gsl/gsl_histogram.h:61
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_get_range";

   function max (h : access constant gsl_histogram) return double  -- /opt/homebrew/include/gsl/gsl_histogram.h:64
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_max";

   function min (h : access constant gsl_histogram) return double  -- /opt/homebrew/include/gsl/gsl_histogram.h:65
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_min";

   function bins (h : access constant gsl_histogram) return size_t  -- /opt/homebrew/include/gsl/gsl_histogram.h:66
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_bins";

   procedure reset (h : access gsl_histogram)  -- /opt/homebrew/include/gsl/gsl_histogram.h:68
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_reset";

   function calloc_range (n : size_t; c_range : access double) return access gsl_histogram  -- /opt/homebrew/include/gsl/gsl_histogram.h:70
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_calloc_range";

   function set_ranges
     (h : access gsl_histogram;
      c_range : access double;
      size : size_t) return int  -- /opt/homebrew/include/gsl/gsl_histogram.h:73
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_set_ranges";

   function set_ranges_uniform
     (h : access gsl_histogram;
      xmin : double;
      xmax : double) return int  -- /opt/homebrew/include/gsl/gsl_histogram.h:75
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_set_ranges_uniform";

   function memcpy (dest : access gsl_histogram; source : access constant gsl_histogram) return int  -- /opt/homebrew/include/gsl/gsl_histogram.h:80
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_memcpy";

   function clone (source : access constant gsl_histogram) return access gsl_histogram  -- /opt/homebrew/include/gsl/gsl_histogram.h:83
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_clone";

   function max_val (h : access constant gsl_histogram) return double  -- /opt/homebrew/include/gsl/gsl_histogram.h:85
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_max_val";

   function max_bin (h : access constant gsl_histogram) return size_t  -- /opt/homebrew/include/gsl/gsl_histogram.h:87
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_max_bin";

   function min_val (h : access constant gsl_histogram) return double  -- /opt/homebrew/include/gsl/gsl_histogram.h:89
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_min_val";

   function min_bin (h : access constant gsl_histogram) return size_t  -- /opt/homebrew/include/gsl/gsl_histogram.h:91
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_min_bin";

   function equal_bins_p (h1 : access constant gsl_histogram; h2 : access constant gsl_histogram) return int  -- /opt/homebrew/include/gsl/gsl_histogram.h:94
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_equal_bins_p";

   function add (h1 : access gsl_histogram; h2 : access constant gsl_histogram) return int  -- /opt/homebrew/include/gsl/gsl_histogram.h:97
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_add";

   function sub (h1 : access gsl_histogram; h2 : access constant gsl_histogram) return int  -- /opt/homebrew/include/gsl/gsl_histogram.h:100
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_sub";

   function mul (h1 : access gsl_histogram; h2 : access constant gsl_histogram) return int  -- /opt/homebrew/include/gsl/gsl_histogram.h:103
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_mul";

   function div (h1 : access gsl_histogram; h2 : access constant gsl_histogram) return int  -- /opt/homebrew/include/gsl/gsl_histogram.h:106
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_div";

   function scale (h : access gsl_histogram; scale : double) return int  -- /opt/homebrew/include/gsl/gsl_histogram.h:109
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_scale";

   function shift (h : access gsl_histogram; shift : double) return int  -- /opt/homebrew/include/gsl/gsl_histogram.h:112
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_shift";

   function sigma (h : access constant gsl_histogram) return double  -- /opt/homebrew/include/gsl/gsl_histogram.h:115
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_sigma";

   function mean (h : access constant gsl_histogram) return double  -- /opt/homebrew/include/gsl/gsl_histogram.h:117
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_mean";

   function sum (h : access constant gsl_histogram) return double  -- /opt/homebrew/include/gsl/gsl_histogram.h:119
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_sum";

   function fwrite (stream : ICS.Files; h : access constant gsl_histogram) return int  -- /opt/homebrew/include/gsl/gsl_histogram.h:121
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_fwrite";

   function fread (stream : ICS.Files; h : access gsl_histogram) return int  -- /opt/homebrew/include/gsl/gsl_histogram.h:122
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_fread";

   function fprintf
     (stream : ICS.Files;
      h : access constant gsl_histogram;
      range_format : Interfaces.C.Strings.chars_ptr;
      bin_format : Interfaces.C.Strings.chars_ptr) return int  -- /opt/homebrew/include/gsl/gsl_histogram.h:123
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_fprintf";

   function fscanf (stream : ICS.Files; h : access gsl_histogram) return int  -- /opt/homebrew/include/gsl/gsl_histogram.h:125
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_fscanf";

   function pdf_alloc (n : size_t) return access gsl_histogram_pdf  -- /opt/homebrew/include/gsl/gsl_histogram.h:127
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_pdf_alloc";

   function pdf_init (p : access gsl_histogram_pdf; h : access constant gsl_histogram) return int  -- /opt/homebrew/include/gsl/gsl_histogram.h:128
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_pdf_init";

   procedure pdf_free (p : access gsl_histogram_pdf)  -- /opt/homebrew/include/gsl/gsl_histogram.h:129
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_pdf_free";

   function pdf_sample (p : access constant gsl_histogram_pdf; r : double) return double  -- /opt/homebrew/include/gsl/gsl_histogram.h:130
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram_pdf_sample";

end gsl.histogram ;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
