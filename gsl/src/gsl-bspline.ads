pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with gsl.vector_double;
with gsl.matrix_double;

package gsl.bspline is

   --  skipped anonymous struct anon_anon_157

   type gsl_bspline_workspace is record
      k      : aliased size_t;  -- /usr/include/gsl/gsl_bspline.h:43
      km1    : aliased size_t;  -- /usr/include/gsl/gsl_bspline.h:44
      l      : aliased size_t;  -- /usr/include/gsl/gsl_bspline.h:45
      nbreak : aliased size_t;  -- /usr/include/gsl/gsl_bspline.h:46
      n      : aliased size_t;  -- /usr/include/gsl/gsl_bspline.h:47
      knots  : access gsl.vector_double
        .gsl_vector;  -- /usr/include/gsl/gsl_bspline.h:49
      deltal : access gsl.vector_double
        .gsl_vector;  -- /usr/include/gsl/gsl_bspline.h:50
      deltar : access gsl.vector_double
        .gsl_vector;  -- /usr/include/gsl/gsl_bspline.h:51
      B : access gsl.vector_double
        .gsl_vector;  -- /usr/include/gsl/gsl_bspline.h:52
      A : access gsl.matrix_double
        .gsl_matrix;  -- /usr/include/gsl/gsl_bspline.h:55
      dB : access gsl.matrix_double
        .gsl_matrix;  -- /usr/include/gsl/gsl_bspline.h:56
   end record with
      Convention => C_Pass_By_Copy;  -- /usr/include/gsl/gsl_bspline.h:57

   function alloc
     (k      : size_t;
      nbreak : size_t)
      return access gsl_bspline_workspace  -- /usr/include/gsl/gsl_bspline.h:60
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_bspline_alloc";

   procedure free
     (w : access gsl_bspline_workspace)  -- /usr/include/gsl/gsl_bspline.h:62
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_bspline_free";

   function ncontrol (w : access gsl_bspline_workspace) return size_t;

   function ncoeffs
     (w : access gsl_bspline_workspace)
      return size_t  -- /usr/include/gsl/gsl_bspline.h:64
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_bspline_ncoeffs";

   function order
     (w : access gsl_bspline_workspace)
      return size_t  -- /usr/include/gsl/gsl_bspline.h:65
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_bspline_order";

   function nbreak
     (w : access gsl_bspline_workspace)
      return size_t  -- /usr/include/gsl/gsl_bspline.h:66
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_bspline_nbreak";

   function breakpoint
     (i : size_t;
      w : access gsl_bspline_workspace)
      return double  -- /usr/include/gsl/gsl_bspline.h:67
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_bspline_breakpoint";

   function greville_abscissa
     (i : size_t;
      w : access gsl_bspline_workspace)
      return double  -- /usr/include/gsl/gsl_bspline.h:68
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_bspline_greville_abscissa";

   function knots
     (breakpts : access constant gsl.vector_double.gsl_vector;
      w        : access gsl_bspline_workspace)
      return int  -- /usr/include/gsl/gsl_bspline.h:71
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_bspline_knots";

   function knots_uniform
     (a : double;
      b : double;
      w : access gsl_bspline_workspace)
      return int  -- /usr/include/gsl/gsl_bspline.h:73
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_bspline_knots_uniform";

   function knots_greville
     (abscissae : access constant gsl.vector_double.gsl_vector;
      w         : access gsl_bspline_workspace;
      abserr    : access double)
      return int  -- /usr/include/gsl/gsl_bspline.h:77
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_bspline_knots_greville";

   function eval
     (x : double; B : access gsl.vector_double.gsl_vector;
      w : access gsl_bspline_workspace)
      return int  -- /usr/include/gsl/gsl_bspline.h:82
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_bspline_eval";

   function eval_nonzero
     (x      : double; Bk : access gsl.vector_double.gsl_vector;
      istart : access size_t; iend : access size_t;
      w      : access gsl_bspline_workspace)
      return int  -- /usr/include/gsl/gsl_bspline.h:86
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_bspline_eval_nonzero";

   function deriv_eval
     (x : double; nderiv : size_t; dB : access gsl.matrix_double.gsl_matrix;
      w : access gsl_bspline_workspace)
      return int  -- /usr/include/gsl/gsl_bspline.h:93
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_bspline_deriv_eval";

   function deriv_eval_nonzero
     (x : double; nderiv : size_t; dB : access gsl.matrix_double.gsl_matrix;
      istart : access size_t; iend : access size_t;
      w      : access gsl_bspline_workspace)
      return int  -- /usr/include/gsl/gsl_bspline.h:99
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_bspline_deriv_eval_nonzero";

-- void gsl_bspline_eval_basis (const double u, double * B, gsl_bspline_workspace * w)

end gsl.bspline;
