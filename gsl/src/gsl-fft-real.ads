pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with gsl.complex;
with gsl.vector_double;

package gsl.fft.real is

   function radix2_transform
     (data   : double_array;
      stride : size_t;
      n      : size_t)
      return int  -- /usr/include/gsl/gsl_fft_real.h:41
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fft_real_radix2_transform";

   function radix2_transform
     (data   : access double;
      stride : size_t;
      n      : size_t)
      return int  -- /usr/include/gsl/gsl_fft_real.h:41
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fft_real_radix2_transform";

   function radix2_transform
     (data : access constant gsl.vector_double.gsl_vector)
      return int; -- /usr/include/gsl/gsl_fft_real.h:41

   --  skipped anonymous struct anon_anon_71

   type gsl_fft_real_wavetable_array10114 is array (0 .. 63) of aliased size_t;
   type gsl_fft_real_wavetable_array10116 is
     array (0 .. 63) of access gsl.complex.gsl_complex;
   type gsl_fft_real_wavetable is record
      n       : aliased size_t;  -- /usr/include/gsl/gsl_fft_real.h:45
      nf      : aliased size_t;  -- /usr/include/gsl/gsl_fft_real.h:46
      factor : aliased gsl_fft_real_wavetable_array10114;  -- /usr/include/gsl/gsl_fft_real.h:47
      twiddle : gsl_fft_real_wavetable_array10116;  -- /usr/include/gsl/gsl_fft_real.h:48
      trig    : access gsl.complex
        .gsl_complex;  -- /usr/include/gsl/gsl_fft_real.h:49
   end record with
      Convention => C_Pass_By_Copy;  -- /usr/include/gsl/gsl_fft_real.h:51

      --  skipped anonymous struct anon_anon_72

   type gsl_fft_real_workspace is record
      n       : aliased size_t;  -- /usr/include/gsl/gsl_fft_real.h:55
      scratch : access double;  -- /usr/include/gsl/gsl_fft_real.h:56
   end record with
      Convention => C_Pass_By_Copy;  -- /usr/include/gsl/gsl_fft_real.h:58

   function wavetable_alloc
     (n : size_t)
      return access gsl_fft_real_wavetable  -- /usr/include/gsl/gsl_fft_real.h:60
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fft_real_wavetable_alloc";

   procedure wavetable_free
     (wavetable : access gsl_fft_real_wavetable)  -- /usr/include/gsl/gsl_fft_real.h:62
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fft_real_wavetable_free";

   function workspace_alloc
     (n : size_t)
      return access gsl_fft_real_workspace  -- /usr/include/gsl/gsl_fft_real.h:64
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fft_real_workspace_alloc";

   procedure workspace_free
     (workspace : access gsl_fft_real_workspace)  -- /usr/include/gsl/gsl_fft_real.h:66
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fft_real_workspace_free";

   function transform
     (data      : double_array; stride : size_t; n : size_t;
      wavetable : access constant gsl_fft_real_wavetable;
      work      : access gsl_fft_real_workspace)
      return int  -- /usr/include/gsl/gsl_fft_real.h:69
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fft_real_transform";

   function transform
     (data      : access double; stride : size_t; n : size_t;
      wavetable : access constant gsl_fft_real_wavetable;
      work      : access gsl_fft_real_workspace)
      return int  -- /usr/include/gsl/gsl_fft_real.h:69
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fft_real_transform";

   function transform
     (data      : access constant gsl.vector_double.gsl_vector;
      wavetable : access constant gsl_fft_real_wavetable;
      work      : access gsl_fft_real_workspace) return int;

   function unpack
     (real_coefficient : double_array; complex_coefficient : double_array;
      stride           : size_t;
      n                : size_t)
      return int  -- /usr/include/gsl/gsl_fft_real.h:74
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fft_real_unpack";

end gsl.fft.real;
