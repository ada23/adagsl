with Ada.Text_IO; use Ada.Text_IO;
package body gsl.version is
   use Interfaces.C.Strings;
   procedure Report is
   begin
      Put ("Interface Version ");
      Put (GSL_VERSION);
      New_Line;
      Put ("Library Version ");
      Put (Value (library));
      New_Line;
   end Report;

end gsl.version;
