pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");
package gsl.block_double is

   type gsl_block_struct is record
      size : aliased size_t;  -- /usr/include/gsl/gsl_block_double.h:40
      data : access double;  -- /usr/include/gsl/gsl_block_double.h:41
   end record with
      Convention => C_Pass_By_Copy;  -- /usr/include/gsl/gsl_block_double.h:38

   subtype gsl_block is
     gsl_block_struct;  -- /usr/include/gsl/gsl_block_double.h:44

   function alloc
     (n : size_t)
      return access gsl_block  -- /usr/include/gsl/gsl_block_double.h:46
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_block_alloc";

   function calloc
     (n : size_t)
      return access gsl_block  -- /usr/include/gsl/gsl_block_double.h:47
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_block_calloc";

   procedure gsl_block_free
     (b : access gsl_block)  -- /usr/include/gsl/gsl_block_double.h:48
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_block_free";

   function fread
     (stream : ICS.FILEs;
      b      : access gsl_block)
      return int  -- /usr/include/gsl/gsl_block_double.h:50
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_block_fread";

   function fwrite
     (stream : ICS.FILEs;
      b      : access constant gsl_block)
      return int  -- /usr/include/gsl/gsl_block_double.h:51
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_block_fwrite";

   function fscanf
     (stream : ICS.FILEs;
      b      : access gsl_block)
      return int  -- /usr/include/gsl/gsl_block_double.h:52
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_block_fscanf";

   function fprintf
     (stream : ICS.FILEs; b : access constant gsl_block;
      format : Interfaces.C.Strings.chars_ptr)
      return int  -- /usr/include/gsl/gsl_block_double.h:53
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_block_fprintf";

   function raw_fread
     (stream : ICS.FILEs; b : access double; n : size_t;
      stride : size_t)
      return int  -- /usr/include/gsl/gsl_block_double.h:55
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_block_raw_fread";

   function raw_fwrite
     (stream : ICS.FILEs; b : access double; n : size_t;
      stride : size_t)
      return int  -- /usr/include/gsl/gsl_block_double.h:56
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_block_raw_fwrite";

   function raw_fscanf
     (stream : ICS.FILEs; b : access double; n : size_t;
      stride : size_t)
      return int  -- /usr/include/gsl/gsl_block_double.h:57
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_block_raw_fscanf";

   function raw_fprintf
     (stream : ICS.FILEs; b : access double; n : size_t;
      stride : size_t;
      format : Interfaces.C.Strings.chars_ptr)
      return int  -- /usr/include/gsl/gsl_block_double.h:58
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_block_raw_fprintf";

   function size
     (b : access constant gsl_block)
      return size_t  -- /usr/include/gsl/gsl_block_double.h:60
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_block_size";

   function data
     (b : access constant gsl_block)
      return access double  -- /usr/include/gsl/gsl_block_double.h:61
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_block_data";

end gsl.block_double;
