package body gsl.math is
   function GSL_SIGN (x : double) return Int is
   begin
      if x >= 0.0 then
         return 1;
      else
         return -1;
      end if;
   end GSL_SIGN;
end gsl.math;
