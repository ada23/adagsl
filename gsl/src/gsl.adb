with Interfaces.C.Strings; use Interfaces.C.Strings;
with Ada.Text_IO;          use Ada.Text_IO;
package body gsl is

   function gsl_strerror
     (gsl_errno : int) return Interfaces.C.Strings
     .chars_ptr  -- ../gsl/gsl_errno.h:83
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_strerror";

   function Error (errno : int) return String is
   begin
      return Value (gsl_strerror (errno));
   end Error;

   function To_Ada
     (c : double_array; len : size_t)
      return Ada.Numerics.Long_Real_Arrays.Real_Vector
   is
      av : Ada.Numerics.Long_Real_Arrays.Real_Vector (1 .. Integer (len));
   begin
      for i in 1 .. Integer (len) loop
         av (i) := Long_Float (c (i - 1));
      end loop;
      return av;
   end To_Ada;

   procedure To_C
     (a   : Ada.Numerics.Long_Real_Arrays.Real_Vector; c : out double_array;
      len : out size_t)
   is
   begin
      len := a'Length;
      for ar in a'Range loop
         c (ar - 1) := double (a (ar));
      end loop;
   end To_C;

   procedure Sep is
   begin
      Put (" ; ");
   end Sep;
end gsl;
