pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

package gsl.rstat is

   --  skipped anonymous struct anon_anon_17

   type gsl_rstat_quantile_workspace_array2316 is
     array (0 .. 4) of aliased double;
   type gsl_rstat_quantile_workspace_array2317 is
     array (0 .. 4) of aliased int;
   type gsl_rstat_quantile_workspace is record
      p    : aliased double;  -- /usr/include/gsl/gsl_rstat.h:39
      q : aliased gsl_rstat_quantile_workspace_array2316;  -- /usr/include/gsl/gsl_rstat.h:40
      npos : aliased gsl_rstat_quantile_workspace_array2317;  -- /usr/include/gsl/gsl_rstat.h:41
      np : aliased gsl_rstat_quantile_workspace_array2316;  -- /usr/include/gsl/gsl_rstat.h:42
      dnp : aliased gsl_rstat_quantile_workspace_array2316;  -- /usr/include/gsl/gsl_rstat.h:43
      n    : aliased size_t;  -- /usr/include/gsl/gsl_rstat.h:44
   end record with
      Convention => C_Pass_By_Copy;  -- /usr/include/gsl/gsl_rstat.h:45

   function quantile_alloc
     (p : double)
      return access gsl_rstat_quantile_workspace  -- /usr/include/gsl/gsl_rstat.h:47
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rstat_quantile_alloc";

   procedure quantile_free
     (w : access gsl_rstat_quantile_workspace)  -- /usr/include/gsl/gsl_rstat.h:48
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rstat_quantile_free";

   function quantile_reset
     (w : access gsl_rstat_quantile_workspace)
      return int  -- /usr/include/gsl/gsl_rstat.h:49
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rstat_quantile_reset";

   function quantile_add
     (x : double;
      w : access gsl_rstat_quantile_workspace)
      return int  -- /usr/include/gsl/gsl_rstat.h:50
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rstat_quantile_add";

   function quantile_get
     (w : access gsl_rstat_quantile_workspace)
      return double  -- /usr/include/gsl/gsl_rstat.h:51
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rstat_quantile_get";

      --  skipped anonymous struct anon_anon_18

   type gsl_rstat_workspace is record
      min                : aliased double;  -- /usr/include/gsl/gsl_rstat.h:55
      max                : aliased double;  -- /usr/include/gsl/gsl_rstat.h:56
      mean               : aliased double;  -- /usr/include/gsl/gsl_rstat.h:57
      M2                 : aliased double;  -- /usr/include/gsl/gsl_rstat.h:58
      M3                 : aliased double;  -- /usr/include/gsl/gsl_rstat.h:59
      M4                 : aliased double;  -- /usr/include/gsl/gsl_rstat.h:60
      n                  : aliased size_t;  -- /usr/include/gsl/gsl_rstat.h:61
      median_workspace_p : access gsl_rstat_quantile_workspace;  -- /usr/include/gsl/gsl_rstat.h:62
   end record with
      Convention => C_Pass_By_Copy;  -- /usr/include/gsl/gsl_rstat.h:63

   function alloc
      return access gsl_rstat_workspace  -- /usr/include/gsl/gsl_rstat.h:65
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rstat_alloc";

   procedure free
     (w : access gsl_rstat_workspace)  -- /usr/include/gsl/gsl_rstat.h:66
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rstat_free";

   function n
     (w : access constant gsl_rstat_workspace)
      return size_t  -- /usr/include/gsl/gsl_rstat.h:67
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rstat_n";

   function add
     (x : double;
      w : access gsl_rstat_workspace)
      return int  -- /usr/include/gsl/gsl_rstat.h:68
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rstat_add";

   function min
     (w : access constant gsl_rstat_workspace)
      return double  -- /usr/include/gsl/gsl_rstat.h:69
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rstat_min";

   function max
     (w : access constant gsl_rstat_workspace)
      return double  -- /usr/include/gsl/gsl_rstat.h:70
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rstat_max";

   function mean
     (w : access constant gsl_rstat_workspace)
      return double  -- /usr/include/gsl/gsl_rstat.h:71
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rstat_mean";

   function variance
     (w : access constant gsl_rstat_workspace)
      return double  -- /usr/include/gsl/gsl_rstat.h:72
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rstat_variance";

   function sd
     (w : access constant gsl_rstat_workspace)
      return double  -- /usr/include/gsl/gsl_rstat.h:73
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rstat_sd";

   function rms
     (w : access constant gsl_rstat_workspace)
      return double  -- /usr/include/gsl/gsl_rstat.h:74
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rstat_rms";

   function sd_mean
     (w : access constant gsl_rstat_workspace)
      return double  -- /usr/include/gsl/gsl_rstat.h:75
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rstat_sd_mean";

   function median
     (w : access gsl_rstat_workspace)
      return double  -- /usr/include/gsl/gsl_rstat.h:76
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rstat_median";

   function skew
     (w : access constant gsl_rstat_workspace)
      return double  -- /usr/include/gsl/gsl_rstat.h:77
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rstat_skew";

   function kurtosis
     (w : access constant gsl_rstat_workspace)
      return double  -- /usr/include/gsl/gsl_rstat.h:78
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rstat_kurtosis";

   function reset
     (w : access gsl_rstat_workspace)
      return int  -- /usr/include/gsl/gsl_rstat.h:79
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_rstat_reset";

end gsl.rstat;
