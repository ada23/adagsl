pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

package gsl.const.cgsm is

   GSL_CONST_CGSM_SPEED_OF_LIGHT : constant double :=
     (2.997_924_58e10);  --  /usr/include/gsl/gsl_const_cgsm.h:24
   GSL_CONST_CGSM_GRAVITATIONAL_CONSTANT : constant double :=
     (6.673e-8);  --  /usr/include/gsl/gsl_const_cgsm.h:25
   GSL_CONST_CGSM_PLANCKS_CONSTANT_H : constant double :=
     (6.626_068_96e-27);  --  /usr/include/gsl/gsl_const_cgsm.h:26
   GSL_CONST_CGSM_PLANCKS_CONSTANT_HBAR : constant double :=
     (1.054_571_628_25e-27);  --  /usr/include/gsl/gsl_const_cgsm.h:27
   GSL_CONST_CGSM_ASTRONOMICAL_UNIT : constant double :=
     (1.495_978_706_91e13);  --  /usr/include/gsl/gsl_const_cgsm.h:28
   GSL_CONST_CGSM_LIGHT_YEAR : constant double :=
     (9.460_536_207_07e17);  --  /usr/include/gsl/gsl_const_cgsm.h:29
   GSL_CONST_CGSM_PARSEC : constant double :=
     (3.085_677_581_35e18);  --  /usr/include/gsl/gsl_const_cgsm.h:30
   GSL_CONST_CGSM_GRAV_ACCEL : constant double :=
     (9.806_65e2);  --  /usr/include/gsl/gsl_const_cgsm.h:31
   GSL_CONST_CGSM_ELECTRON_VOLT : constant double :=
     (1.602_176_487e-12);  --  /usr/include/gsl/gsl_const_cgsm.h:32
   GSL_CONST_CGSM_MASS_ELECTRON : constant double :=
     (9.109_381_88e-28);  --  /usr/include/gsl/gsl_const_cgsm.h:33
   GSL_CONST_CGSM_MASS_MUON : constant double :=
     (1.883_531_09e-25);  --  /usr/include/gsl/gsl_const_cgsm.h:34
   GSL_CONST_CGSM_MASS_PROTON : constant double :=
     (1.672_621_58e-24);  --  /usr/include/gsl/gsl_const_cgsm.h:35
   GSL_CONST_CGSM_MASS_NEUTRON : constant double :=
     (1.674_927_16e-24);  --  /usr/include/gsl/gsl_const_cgsm.h:36
   GSL_CONST_CGSM_RYDBERG : constant double :=
     (2.179_871_969_68e-11);  --  /usr/include/gsl/gsl_const_cgsm.h:37
   GSL_CONST_CGSM_BOLTZMANN : constant double :=
     (1.380_650_4e-16);  --  /usr/include/gsl/gsl_const_cgsm.h:38
   GSL_CONST_CGSM_MOLAR_GAS : constant double :=
     (8.314_472e7);  --  /usr/include/gsl/gsl_const_cgsm.h:39
   GSL_CONST_CGSM_STANDARD_GAS_VOLUME : constant double :=
     (2.271_098_1e4);  --  /usr/include/gsl/gsl_const_cgsm.h:40
   GSL_CONST_CGSM_MINUTE : constant double :=
     (6.0e1);  --  /usr/include/gsl/gsl_const_cgsm.h:41
   GSL_CONST_CGSM_HOUR : constant double :=
     (3.6e3);  --  /usr/include/gsl/gsl_const_cgsm.h:42
   GSL_CONST_CGSM_DAY : constant double :=
     (8.64e4);  --  /usr/include/gsl/gsl_const_cgsm.h:43
   GSL_CONST_CGSM_WEEK : constant double :=
     (6.048e5);  --  /usr/include/gsl/gsl_const_cgsm.h:44
   GSL_CONST_CGSM_INCH : constant double :=
     (2.54e0);  --  /usr/include/gsl/gsl_const_cgsm.h:45
   GSL_CONST_CGSM_FOOT : constant double :=
     (3.048e1);  --  /usr/include/gsl/gsl_const_cgsm.h:46
   GSL_CONST_CGSM_YARD : constant double :=
     (9.144e1);  --  /usr/include/gsl/gsl_const_cgsm.h:47
   GSL_CONST_CGSM_MILE : constant double :=
     (1.609_344e5);  --  /usr/include/gsl/gsl_const_cgsm.h:48
   GSL_CONST_CGSM_NAUTICAL_MILE : constant double :=
     (1.852e5);  --  /usr/include/gsl/gsl_const_cgsm.h:49
   GSL_CONST_CGSM_FATHOM : constant double :=
     (1.828_8e2);  --  /usr/include/gsl/gsl_const_cgsm.h:50
   GSL_CONST_CGSM_MIL : constant double :=
     (2.54e-3);  --  /usr/include/gsl/gsl_const_cgsm.h:51
   GSL_CONST_CGSM_POINT : constant double :=
     (3.527_777_777_78e-2);  --  /usr/include/gsl/gsl_const_cgsm.h:52
   GSL_CONST_CGSM_TEXPOINT : constant double :=
     (3.514_598_035_15e-2);  --  /usr/include/gsl/gsl_const_cgsm.h:53
   GSL_CONST_CGSM_MICRON : constant double :=
     (1.0e-4);  --  /usr/include/gsl/gsl_const_cgsm.h:54
   GSL_CONST_CGSM_ANGSTROM : constant double :=
     (1.0e-8);  --  /usr/include/gsl/gsl_const_cgsm.h:55
   GSL_CONST_CGSM_HECTARE : constant double :=
     (1.0e8);  --  /usr/include/gsl/gsl_const_cgsm.h:56
   GSL_CONST_CGSM_ACRE : constant double :=
     (4.046_856_422_41e7);  --  /usr/include/gsl/gsl_const_cgsm.h:57
   GSL_CONST_CGSM_BARN : constant double :=
     (1.0e-24);  --  /usr/include/gsl/gsl_const_cgsm.h:58
   GSL_CONST_CGSM_LITER : constant double :=
     (1.0e3);  --  /usr/include/gsl/gsl_const_cgsm.h:59
   GSL_CONST_CGSM_US_GALLON : constant double :=
     (3.785_411_784_02e3);  --  /usr/include/gsl/gsl_const_cgsm.h:60
   GSL_CONST_CGSM_QUART : constant double :=
     (9.463_529_460_04e2);  --  /usr/include/gsl/gsl_const_cgsm.h:61
   GSL_CONST_CGSM_PINT : constant double :=
     (4.731_764_730_02e2);  --  /usr/include/gsl/gsl_const_cgsm.h:62
   GSL_CONST_CGSM_CUP : constant double :=
     (2.365_882_365_01e2);  --  /usr/include/gsl/gsl_const_cgsm.h:63
   GSL_CONST_CGSM_FLUID_OUNCE : constant double :=
     (2.957_352_956_26e1);  --  /usr/include/gsl/gsl_const_cgsm.h:64
   GSL_CONST_CGSM_TABLESPOON : constant double :=
     (1.478_676_478_13e1);  --  /usr/include/gsl/gsl_const_cgsm.h:65
   GSL_CONST_CGSM_TEASPOON : constant double :=
     (4.928_921_593_75e0);  --  /usr/include/gsl/gsl_const_cgsm.h:66
   GSL_CONST_CGSM_CANADIAN_GALLON : constant double :=
     (4.546_09e3);  --  /usr/include/gsl/gsl_const_cgsm.h:67
   GSL_CONST_CGSM_UK_GALLON : constant double :=
     (4.546_092e3);  --  /usr/include/gsl/gsl_const_cgsm.h:68
   GSL_CONST_CGSM_MILES_PER_HOUR : constant double :=
     (4.470_4e1);  --  /usr/include/gsl/gsl_const_cgsm.h:69
   GSL_CONST_CGSM_KILOMETERS_PER_HOUR : constant double :=
     (2.777_777_777_78e1);  --  /usr/include/gsl/gsl_const_cgsm.h:70
   GSL_CONST_CGSM_KNOT : constant double :=
     (5.144_444_444_44e1);  --  /usr/include/gsl/gsl_const_cgsm.h:71
   GSL_CONST_CGSM_POUND_MASS : constant double :=
     (4.535_923_7e2);  --  /usr/include/gsl/gsl_const_cgsm.h:72
   GSL_CONST_CGSM_OUNCE_MASS : constant double :=
     (2.834_952_312_5e1);  --  /usr/include/gsl/gsl_const_cgsm.h:73
   GSL_CONST_CGSM_TON : constant double :=
     (9.071_847_4e5);  --  /usr/include/gsl/gsl_const_cgsm.h:74
   GSL_CONST_CGSM_METRIC_TON : constant double :=
     (1.0e6);  --  /usr/include/gsl/gsl_const_cgsm.h:75
   GSL_CONST_CGSM_UK_TON : constant double :=
     (1.016_046_908_8e6);  --  /usr/include/gsl/gsl_const_cgsm.h:76
   GSL_CONST_CGSM_TROY_OUNCE : constant double :=
     (3.110_347_5e1);  --  /usr/include/gsl/gsl_const_cgsm.h:77
   GSL_CONST_CGSM_CARAT : constant double :=
     (2.0e-1);  --  /usr/include/gsl/gsl_const_cgsm.h:78
   GSL_CONST_CGSM_UNIFIED_ATOMIC_MASS : constant double :=
     (1.660_538_782e-24);  --  /usr/include/gsl/gsl_const_cgsm.h:79
   GSL_CONST_CGSM_GRAM_FORCE : constant double :=
     (9.806_65e2);  --  /usr/include/gsl/gsl_const_cgsm.h:80
   GSL_CONST_CGSM_POUND_FORCE : constant double :=
     (4.448_221_615_26e5);  --  /usr/include/gsl/gsl_const_cgsm.h:81
   GSL_CONST_CGSM_KILOPOUND_FORCE : constant double :=
     (4.448_221_615_26e8);  --  /usr/include/gsl/gsl_const_cgsm.h:82
   GSL_CONST_CGSM_POUNDAL : constant double :=
     (1.382_55e4);  --  /usr/include/gsl/gsl_const_cgsm.h:83
   GSL_CONST_CGSM_CALORIE : constant double :=
     (4.186_8e7);  --  /usr/include/gsl/gsl_const_cgsm.h:84
   GSL_CONST_CGSM_BTU : constant double :=
     (1.055_055_852_62e10);  --  /usr/include/gsl/gsl_const_cgsm.h:85
   GSL_CONST_CGSM_THERM : constant double :=
     (1.055_06e15);  --  /usr/include/gsl/gsl_const_cgsm.h:86
   GSL_CONST_CGSM_HORSEPOWER : constant double :=
     (7.457e9);  --  /usr/include/gsl/gsl_const_cgsm.h:87
   GSL_CONST_CGSM_BAR : constant double :=
     (1.0e6);  --  /usr/include/gsl/gsl_const_cgsm.h:88
   GSL_CONST_CGSM_STD_ATMOSPHERE : constant double :=
     (1.013_25e6);  --  /usr/include/gsl/gsl_const_cgsm.h:89
   GSL_CONST_CGSM_TORR : constant double :=
     (1.333_223_684_21e3);  --  /usr/include/gsl/gsl_const_cgsm.h:90
   GSL_CONST_CGSM_METER_OF_MERCURY : constant double :=
     (1.333_223_684_21e6);  --  /usr/include/gsl/gsl_const_cgsm.h:91
   GSL_CONST_CGSM_INCH_OF_MERCURY : constant double :=
     (3.386_388_157_89e4);  --  /usr/include/gsl/gsl_const_cgsm.h:92
   GSL_CONST_CGSM_INCH_OF_WATER : constant double :=
     (2.490_889e3);  --  /usr/include/gsl/gsl_const_cgsm.h:93
   GSL_CONST_CGSM_PSI : constant double :=
     (6.894_757_293_17e4);  --  /usr/include/gsl/gsl_const_cgsm.h:94
   GSL_CONST_CGSM_POISE : constant double :=
     (1.0e0);  --  /usr/include/gsl/gsl_const_cgsm.h:95
   GSL_CONST_CGSM_STOKES : constant double :=
     (1.0e0);  --  /usr/include/gsl/gsl_const_cgsm.h:96
   GSL_CONST_CGSM_STILB : constant double :=
     (1.0e0);  --  /usr/include/gsl/gsl_const_cgsm.h:97
   GSL_CONST_CGSM_LUMEN : constant double :=
     (1.0e0);  --  /usr/include/gsl/gsl_const_cgsm.h:98
   GSL_CONST_CGSM_LUX : constant double :=
     (1.0e-4);  --  /usr/include/gsl/gsl_const_cgsm.h:99
   GSL_CONST_CGSM_PHOT : constant double :=
     (1.0e0);  --  /usr/include/gsl/gsl_const_cgsm.h:100
   GSL_CONST_CGSM_FOOTCANDLE : constant double :=
     (1.076e-3);  --  /usr/include/gsl/gsl_const_cgsm.h:101
   GSL_CONST_CGSM_LAMBERT : constant double :=
     (1.0e0);  --  /usr/include/gsl/gsl_const_cgsm.h:102
   GSL_CONST_CGSM_FOOTLAMBERT : constant double :=
     (1.076_391_04e-3);  --  /usr/include/gsl/gsl_const_cgsm.h:103
   GSL_CONST_CGSM_CURIE : constant double :=
     (3.7e10);  --  /usr/include/gsl/gsl_const_cgsm.h:104
   GSL_CONST_CGSM_ROENTGEN : constant double :=
     (2.58e-8);  --  /usr/include/gsl/gsl_const_cgsm.h:105
   GSL_CONST_CGSM_RAD : constant double :=
     (1.0e2);  --  /usr/include/gsl/gsl_const_cgsm.h:106
   GSL_CONST_CGSM_SOLAR_MASS : constant double :=
     (1.988_92e33);  --  /usr/include/gsl/gsl_const_cgsm.h:107
   GSL_CONST_CGSM_BOHR_RADIUS : constant double :=
     (5.291_772_083e-9);  --  /usr/include/gsl/gsl_const_cgsm.h:108
   GSL_CONST_CGSM_NEWTON : constant double :=
     (1.0e5);  --  /usr/include/gsl/gsl_const_cgsm.h:109
   GSL_CONST_CGSM_DYNE : constant double :=
     (1.0e0);  --  /usr/include/gsl/gsl_const_cgsm.h:110
   GSL_CONST_CGSM_JOULE : constant double :=
     (1.0e7);  --  /usr/include/gsl/gsl_const_cgsm.h:111
   GSL_CONST_CGSM_ERG : constant double :=
     (1.0e0);  --  /usr/include/gsl/gsl_const_cgsm.h:112
   GSL_CONST_CGSM_STEFAN_BOLTZMANN_CONSTANT : constant double :=
     (5.670_400_473_74e-5);  --  /usr/include/gsl/gsl_const_cgsm.h:113
   GSL_CONST_CGSM_THOMSON_CROSS_SECTION : constant double :=
     (6.652_458_936_99e-25);  --  /usr/include/gsl/gsl_const_cgsm.h:114
   GSL_CONST_CGSM_BOHR_MAGNETON : constant double :=
     (9.274_008_99e-21);  --  /usr/include/gsl/gsl_const_cgsm.h:115
   GSL_CONST_CGSM_NUCLEAR_MAGNETON : constant double :=
     (5.050_783_17e-24);  --  /usr/include/gsl/gsl_const_cgsm.h:116
   GSL_CONST_CGSM_ELECTRON_MAGNETIC_MOMENT : constant double :=
     (9.284_763_62e-21);  --  /usr/include/gsl/gsl_const_cgsm.h:117
   GSL_CONST_CGSM_PROTON_MAGNETIC_MOMENT : constant double :=
     (1.410_606_633e-23);  --  /usr/include/gsl/gsl_const_cgsm.h:118
   GSL_CONST_CGSM_FARADAY : constant double :=
     (9.648_534_297_75e3);  --  /usr/include/gsl/gsl_const_cgsm.h:119
   GSL_CONST_CGSM_ELECTRON_CHARGE : constant double :=
     (1.602_176_487e-20);  --  /usr/include/gsl/gsl_const_cgsm.h:120

end gsl.const.cgsm;
