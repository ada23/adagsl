pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

package gsl.const.cgs is

   GSL_CONST_CGS_SPEED_OF_LIGHT : constant double :=
     (2.997_924_58e10);  --  /usr/include/gsl/gsl_const_cgs.h:24
   GSL_CONST_CGS_GRAVITATIONAL_CONSTANT : constant double :=
     (6.673e-8);  --  /usr/include/gsl/gsl_const_cgs.h:25
   GSL_CONST_CGS_PLANCKS_CONSTANT_H : constant double :=
     (6.626_068_96e-27);  --  /usr/include/gsl/gsl_const_cgs.h:26
   GSL_CONST_CGS_PLANCKS_CONSTANT_HBAR : constant double :=
     (1.054_571_628_25e-27);  --  /usr/include/gsl/gsl_const_cgs.h:27
   GSL_CONST_CGS_ASTRONOMICAL_UNIT : constant double :=
     (1.495_978_706_91e13);  --  /usr/include/gsl/gsl_const_cgs.h:28
   GSL_CONST_CGS_LIGHT_YEAR : constant double :=
     (9.460_536_207_07e17);  --  /usr/include/gsl/gsl_const_cgs.h:29
   GSL_CONST_CGS_PARSEC : constant double :=
     (3.085_677_581_35e18);  --  /usr/include/gsl/gsl_const_cgs.h:30
   GSL_CONST_CGS_GRAV_ACCEL : constant double :=
     (9.806_65e2);  --  /usr/include/gsl/gsl_const_cgs.h:31
   GSL_CONST_CGS_ELECTRON_VOLT : constant double :=
     (1.602_176_487e-12);  --  /usr/include/gsl/gsl_const_cgs.h:32
   GSL_CONST_CGS_MASS_ELECTRON : constant double :=
     (9.109_381_88e-28);  --  /usr/include/gsl/gsl_const_cgs.h:33
   GSL_CONST_CGS_MASS_MUON : constant double :=
     (1.883_531_09e-25);  --  /usr/include/gsl/gsl_const_cgs.h:34
   GSL_CONST_CGS_MASS_PROTON : constant double :=
     (1.672_621_58e-24);  --  /usr/include/gsl/gsl_const_cgs.h:35
   GSL_CONST_CGS_MASS_NEUTRON : constant double :=
     (1.674_927_16e-24);  --  /usr/include/gsl/gsl_const_cgs.h:36
   GSL_CONST_CGS_RYDBERG : constant double :=
     (2.179_871_969_68e-11);  --  /usr/include/gsl/gsl_const_cgs.h:37
   GSL_CONST_CGS_BOLTZMANN : constant double :=
     (1.380_650_4e-16);  --  /usr/include/gsl/gsl_const_cgs.h:38
   GSL_CONST_CGS_MOLAR_GAS : constant double :=
     (8.314_472e7);  --  /usr/include/gsl/gsl_const_cgs.h:39
   GSL_CONST_CGS_STANDARD_GAS_VOLUME : constant double :=
     (2.271_098_1e4);  --  /usr/include/gsl/gsl_const_cgs.h:40
   GSL_CONST_CGS_MINUTE : constant double :=
     (6.0e1);  --  /usr/include/gsl/gsl_const_cgs.h:41
   GSL_CONST_CGS_HOUR : constant double :=
     (3.6e3);  --  /usr/include/gsl/gsl_const_cgs.h:42
   GSL_CONST_CGS_DAY : constant double :=
     (8.64e4);  --  /usr/include/gsl/gsl_const_cgs.h:43
   GSL_CONST_CGS_WEEK : constant double :=
     (6.048e5);  --  /usr/include/gsl/gsl_const_cgs.h:44
   GSL_CONST_CGS_INCH : constant double :=
     (2.54e0);  --  /usr/include/gsl/gsl_const_cgs.h:45
   GSL_CONST_CGS_FOOT : constant double :=
     (3.048e1);  --  /usr/include/gsl/gsl_const_cgs.h:46
   GSL_CONST_CGS_YARD : constant double :=
     (9.144e1);  --  /usr/include/gsl/gsl_const_cgs.h:47
   GSL_CONST_CGS_MILE : constant double :=
     (1.609_344e5);  --  /usr/include/gsl/gsl_const_cgs.h:48
   GSL_CONST_CGS_NAUTICAL_MILE : constant double :=
     (1.852e5);  --  /usr/include/gsl/gsl_const_cgs.h:49
   GSL_CONST_CGS_FATHOM : constant double :=
     (1.828_8e2);  --  /usr/include/gsl/gsl_const_cgs.h:50
   GSL_CONST_CGS_MIL : constant double :=
     (2.54e-3);  --  /usr/include/gsl/gsl_const_cgs.h:51
   GSL_CONST_CGS_POINT : constant double :=
     (3.527_777_777_78e-2);  --  /usr/include/gsl/gsl_const_cgs.h:52
   GSL_CONST_CGS_TEXPOINT : constant double :=
     (3.514_598_035_15e-2);  --  /usr/include/gsl/gsl_const_cgs.h:53
   GSL_CONST_CGS_MICRON : constant double :=
     (1.0e-4);  --  /usr/include/gsl/gsl_const_cgs.h:54
   GSL_CONST_CGS_ANGSTROM : constant double :=
     (1.0e-8);  --  /usr/include/gsl/gsl_const_cgs.h:55
   GSL_CONST_CGS_HECTARE : constant double :=
     (1.0e8);  --  /usr/include/gsl/gsl_const_cgs.h:56
   GSL_CONST_CGS_ACRE : constant double :=
     (4.046_856_422_41e7);  --  /usr/include/gsl/gsl_const_cgs.h:57
   GSL_CONST_CGS_BARN : constant double :=
     (1.0e-24);  --  /usr/include/gsl/gsl_const_cgs.h:58
   GSL_CONST_CGS_LITER : constant double :=
     (1.0e3);  --  /usr/include/gsl/gsl_const_cgs.h:59
   GSL_CONST_CGS_US_GALLON : constant double :=
     (3.785_411_784_02e3);  --  /usr/include/gsl/gsl_const_cgs.h:60
   GSL_CONST_CGS_QUART : constant double :=
     (9.463_529_460_04e2);  --  /usr/include/gsl/gsl_const_cgs.h:61
   GSL_CONST_CGS_PINT : constant double :=
     (4.731_764_730_02e2);  --  /usr/include/gsl/gsl_const_cgs.h:62
   GSL_CONST_CGS_CUP : constant double :=
     (2.365_882_365_01e2);  --  /usr/include/gsl/gsl_const_cgs.h:63
   GSL_CONST_CGS_FLUID_OUNCE : constant double :=
     (2.957_352_956_26e1);  --  /usr/include/gsl/gsl_const_cgs.h:64
   GSL_CONST_CGS_TABLESPOON : constant double :=
     (1.478_676_478_13e1);  --  /usr/include/gsl/gsl_const_cgs.h:65
   GSL_CONST_CGS_TEASPOON : constant double :=
     (4.928_921_593_75e0);  --  /usr/include/gsl/gsl_const_cgs.h:66
   GSL_CONST_CGS_CANADIAN_GALLON : constant double :=
     (4.546_09e3);  --  /usr/include/gsl/gsl_const_cgs.h:67
   GSL_CONST_CGS_UK_GALLON : constant double :=
     (4.546_092e3);  --  /usr/include/gsl/gsl_const_cgs.h:68
   GSL_CONST_CGS_MILES_PER_HOUR : constant double :=
     (4.470_4e1);  --  /usr/include/gsl/gsl_const_cgs.h:69
   GSL_CONST_CGS_KILOMETERS_PER_HOUR : constant double :=
     (2.777_777_777_78e1);  --  /usr/include/gsl/gsl_const_cgs.h:70
   GSL_CONST_CGS_KNOT : constant double :=
     (5.144_444_444_44e1);  --  /usr/include/gsl/gsl_const_cgs.h:71
   GSL_CONST_CGS_POUND_MASS : constant double :=
     (4.535_923_7e2);  --  /usr/include/gsl/gsl_const_cgs.h:72
   GSL_CONST_CGS_OUNCE_MASS : constant double :=
     (2.834_952_312_5e1);  --  /usr/include/gsl/gsl_const_cgs.h:73
   GSL_CONST_CGS_TON : constant double :=
     (9.071_847_4e5);  --  /usr/include/gsl/gsl_const_cgs.h:74
   GSL_CONST_CGS_METRIC_TON : constant double :=
     (1.0e6);  --  /usr/include/gsl/gsl_const_cgs.h:75
   GSL_CONST_CGS_UK_TON : constant double :=
     (1.016_046_908_8e6);  --  /usr/include/gsl/gsl_const_cgs.h:76
   GSL_CONST_CGS_TROY_OUNCE : constant double :=
     (3.110_347_5e1);  --  /usr/include/gsl/gsl_const_cgs.h:77
   GSL_CONST_CGS_CARAT : constant double :=
     (2.0e-1);  --  /usr/include/gsl/gsl_const_cgs.h:78
   GSL_CONST_CGS_UNIFIED_ATOMIC_MASS : constant double :=
     (1.660_538_782e-24);  --  /usr/include/gsl/gsl_const_cgs.h:79
   GSL_CONST_CGS_GRAM_FORCE : constant double :=
     (9.806_65e2);  --  /usr/include/gsl/gsl_const_cgs.h:80
   GSL_CONST_CGS_POUND_FORCE : constant double :=
     (4.448_221_615_26e5);  --  /usr/include/gsl/gsl_const_cgs.h:81
   GSL_CONST_CGS_KILOPOUND_FORCE : constant double :=
     (4.448_221_615_26e8);  --  /usr/include/gsl/gsl_const_cgs.h:82
   GSL_CONST_CGS_POUNDAL : constant double :=
     (1.382_55e4);  --  /usr/include/gsl/gsl_const_cgs.h:83
   GSL_CONST_CGS_CALORIE : constant double :=
     (4.186_8e7);  --  /usr/include/gsl/gsl_const_cgs.h:84
   GSL_CONST_CGS_BTU : constant double :=
     (1.055_055_852_62e10);  --  /usr/include/gsl/gsl_const_cgs.h:85
   GSL_CONST_CGS_THERM : constant double :=
     (1.055_06e15);  --  /usr/include/gsl/gsl_const_cgs.h:86
   GSL_CONST_CGS_HORSEPOWER : constant double :=
     (7.457e9);  --  /usr/include/gsl/gsl_const_cgs.h:87
   GSL_CONST_CGS_BAR : constant double :=
     (1.0e6);  --  /usr/include/gsl/gsl_const_cgs.h:88
   GSL_CONST_CGS_STD_ATMOSPHERE : constant double :=
     (1.013_25e6);  --  /usr/include/gsl/gsl_const_cgs.h:89
   GSL_CONST_CGS_TORR : constant double :=
     (1.333_223_684_21e3);  --  /usr/include/gsl/gsl_const_cgs.h:90
   GSL_CONST_CGS_METER_OF_MERCURY : constant double :=
     (1.333_223_684_21e6);  --  /usr/include/gsl/gsl_const_cgs.h:91
   GSL_CONST_CGS_INCH_OF_MERCURY : constant double :=
     (3.386_388_157_89e4);  --  /usr/include/gsl/gsl_const_cgs.h:92
   GSL_CONST_CGS_INCH_OF_WATER : constant double :=
     (2.490_889e3);  --  /usr/include/gsl/gsl_const_cgs.h:93
   GSL_CONST_CGS_PSI : constant double :=
     (6.894_757_293_17e4);  --  /usr/include/gsl/gsl_const_cgs.h:94
   GSL_CONST_CGS_POISE : constant double :=
     (1.0e0);  --  /usr/include/gsl/gsl_const_cgs.h:95
   GSL_CONST_CGS_STOKES : constant double :=
     (1.0e0);  --  /usr/include/gsl/gsl_const_cgs.h:96
   GSL_CONST_CGS_STILB : constant double :=
     (1.0e0);  --  /usr/include/gsl/gsl_const_cgs.h:97
   GSL_CONST_CGS_LUMEN : constant double :=
     (1.0e0);  --  /usr/include/gsl/gsl_const_cgs.h:98
   GSL_CONST_CGS_LUX : constant double :=
     (1.0e-4);  --  /usr/include/gsl/gsl_const_cgs.h:99
   GSL_CONST_CGS_PHOT : constant double :=
     (1.0e0);  --  /usr/include/gsl/gsl_const_cgs.h:100
   GSL_CONST_CGS_FOOTCANDLE : constant double :=
     (1.076e-3);  --  /usr/include/gsl/gsl_const_cgs.h:101
   GSL_CONST_CGS_LAMBERT : constant double :=
     (1.0e0);  --  /usr/include/gsl/gsl_const_cgs.h:102
   GSL_CONST_CGS_FOOTLAMBERT : constant double :=
     (1.076_391_04e-3);  --  /usr/include/gsl/gsl_const_cgs.h:103
   GSL_CONST_CGS_CURIE : constant double :=
     (3.7e10);  --  /usr/include/gsl/gsl_const_cgs.h:104
   GSL_CONST_CGS_ROENTGEN : constant double :=
     (2.58e-7);  --  /usr/include/gsl/gsl_const_cgs.h:105
   GSL_CONST_CGS_RAD : constant double :=
     (1.0e2);  --  /usr/include/gsl/gsl_const_cgs.h:106
   GSL_CONST_CGS_SOLAR_MASS : constant double :=
     (1.988_92e33);  --  /usr/include/gsl/gsl_const_cgs.h:107
   GSL_CONST_CGS_BOHR_RADIUS : constant double :=
     (5.291_772_083e-9);  --  /usr/include/gsl/gsl_const_cgs.h:108
   GSL_CONST_CGS_NEWTON : constant double :=
     (1.0e5);  --  /usr/include/gsl/gsl_const_cgs.h:109
   GSL_CONST_CGS_DYNE : constant double :=
     (1.0e0);  --  /usr/include/gsl/gsl_const_cgs.h:110
   GSL_CONST_CGS_JOULE : constant double :=
     (1.0e7);  --  /usr/include/gsl/gsl_const_cgs.h:111
   GSL_CONST_CGS_ERG : constant double :=
     (1.0e0);  --  /usr/include/gsl/gsl_const_cgs.h:112
   GSL_CONST_CGS_STEFAN_BOLTZMANN_CONSTANT : constant double :=
     (5.670_400_473_74e-5);  --  /usr/include/gsl/gsl_const_cgs.h:113
   GSL_CONST_CGS_THOMSON_CROSS_SECTION : constant double :=
     (6.652_458_936_99e-25);  --  /usr/include/gsl/gsl_const_cgs.h:114
end gsl.const.cgs;
