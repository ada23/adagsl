pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with gsl.sf.result;

package gsl.sf.clausen is

   function clausen_e
     (x      : double;
      result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_clausen.h:46
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_clausen_e";

   function clausen
     (x : double)
      return double  -- /usr/include/gsl/gsl_sf_clausen.h:47
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_clausen";

end gsl.sf.clausen;
