pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with gsl.wavelet ;
with gsl.matrix_double ;

package gsl.wavelet2d is

   function transform
     (w : access constant gsl.wavelet.gsl_wavelet;
      data : access double;
      tda : size_t;
      size1 : size_t;
      size2 : size_t;
      dir : gsl.wavelet.gsl_wavelet_direction;
      work : access gsl.wavelet.gsl_wavelet_workspace) return int  -- /usr/include/gsl/gsl_wavelet2d.h:40
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_wavelet2d_transform";

   function transform_forward
     (w : access constant gsl.wavelet.gsl_wavelet;
      data : access double;
      tda : size_t;
      size1 : size_t;
      size2 : size_t;
      work : access gsl.wavelet.gsl_wavelet_workspace) return int  -- /usr/include/gsl/gsl_wavelet2d.h:46
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_wavelet2d_transform_forward";

   function transform_inverse
     (w : access constant gsl.wavelet.gsl_wavelet;
      data : access double;
      tda : size_t;
      size1 : size_t;
      size2 : size_t;
      work : access gsl.wavelet.gsl_wavelet_workspace) return int  -- /usr/include/gsl/gsl_wavelet2d.h:51
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_wavelet2d_transform_inverse";

   function nstransform
     (w : access constant gsl.wavelet.gsl_wavelet;
      data : access double;
      tda : size_t;
      size1 : size_t;
      size2 : size_t;
      dir : gsl.wavelet.gsl_wavelet_direction;
      work : access gsl.wavelet.gsl_wavelet_workspace) return int  -- /usr/include/gsl/gsl_wavelet2d.h:56
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_wavelet2d_nstransform";

   function nstransform_forward
     (w : access constant gsl.wavelet.gsl_wavelet;
      data : access double;
      tda : size_t;
      size1 : size_t;
      size2 : size_t;
      work : access gsl.wavelet.gsl_wavelet_workspace) return int  -- /usr/include/gsl/gsl_wavelet2d.h:62
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_wavelet2d_nstransform_forward";

   function nstransform_inverse
     (w : access constant gsl.wavelet.gsl_wavelet;
      data : access double;
      tda : size_t;
      size1 : size_t;
      size2 : size_t;
      work : access gsl.wavelet.gsl_wavelet_workspace) return int  -- /usr/include/gsl/gsl_wavelet2d.h:67
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_wavelet2d_nstransform_inverse";

   function transform_matrix
     (w : access constant gsl.wavelet.gsl_wavelet;
      a : access gsl.matrix_double.gsl_matrix;
      dir : gsl.wavelet.gsl_wavelet_direction;
      work : access gsl.wavelet.gsl_wavelet_workspace) return int  -- /usr/include/gsl/gsl_wavelet2d.h:73
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_wavelet2d_transform_matrix";

   function transform_matrix_forward
     (w : access constant gsl.wavelet.gsl_wavelet;
      a : access gsl.matrix_double.gsl_matrix;
      work : access gsl.wavelet.gsl_wavelet_workspace) return int  -- /usr/include/gsl/gsl_wavelet2d.h:79
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_wavelet2d_transform_matrix_forward";

   function transform_matrix_inverse
     (w : access constant gsl.wavelet.gsl_wavelet;
      a : access gsl.matrix_double.gsl_matrix;
      work : access gsl.wavelet.gsl_wavelet_workspace) return int  -- /usr/include/gsl/gsl_wavelet2d.h:84
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_wavelet2d_transform_matrix_inverse";

   function nstransform_matrix
     (w : access constant gsl.wavelet.gsl_wavelet;
      a : access gsl.matrix_double.gsl_matrix;
      dir : gsl.wavelet.gsl_wavelet_direction;
      work : access gsl.wavelet.gsl_wavelet_workspace) return int  -- /usr/include/gsl/gsl_wavelet2d.h:90
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_wavelet2d_nstransform_matrix";

   function nstransform_matrix_forward
     (w : access constant gsl.wavelet.gsl_wavelet;
      a : access gsl.matrix_double.gsl_matrix;
      work : access gsl.wavelet.gsl_wavelet_workspace) return int  -- /usr/include/gsl/gsl_wavelet2d.h:96
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_wavelet2d_nstransform_matrix_forward";

   function nstransform_matrix_inverse
     (w : access constant gsl.wavelet.gsl_wavelet;
      a : access gsl.matrix_double.gsl_matrix;
      work : access gsl.wavelet.gsl_wavelet_workspace) return int  -- /usr/include/gsl/gsl_wavelet2d.h:101
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_wavelet2d_nstransform_matrix_inverse";

end gsl.wavelet2d ;
