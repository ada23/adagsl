pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with gsl.blas_types;
with gsl.matrix_double;
with gsl.vector_double;
with gsl.spmatrix_double;

package gsl.spblas is

   function dgemv
     (TransA : gsl.blas_types.CBLAS_TRANSPOSE_t; alpha : double;
      A      : access constant gsl.spmatrix_double.gsl_spmatrix;
      x      : access constant gsl.vector_double.gsl_vector; beta : double;
      y      : access gsl.vector_double.gsl_vector)
      return int  -- /usr/include/gsl/gsl_spblas.h:47
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spblas_dgemv";

   function dgemm
     (alpha : double; A : access constant gsl.spmatrix_double.gsl_spmatrix;
      B     : access constant gsl.spmatrix_double.gsl_spmatrix;
      C     : access gsl.spmatrix_double.gsl_spmatrix)
      return int  -- /usr/include/gsl/gsl_spblas.h:50
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spblas_dgemm";

   function scatter
     (A     : access constant gsl.spmatrix_double.gsl_spmatrix; j : size_t;
      alpha : double; w : access int; x : access double; mark : int;
      C     : access gsl.spmatrix_double.gsl_spmatrix; nz : size_t)
      return size_t  -- /usr/include/gsl/gsl_spblas.h:52
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spblas_scatter";

end gsl.spblas;
