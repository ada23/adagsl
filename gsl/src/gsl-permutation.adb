with Ada.Text_IO;          use Ada.Text_IO;
with Ada.Text_IO.C_Streams;
with Interfaces.C.Strings; use Interfaces.C.Strings;
package body gsl.permutation is

   function show (p : access gsl_permutation) return int is
      Status : int;
   begin
      Status :=
        gsl.permutation.fprintf
          (Ada.Text_IO.C_Streams.C_Stream (Ada.Text_IO.Standard_Output), p,
           New_String (" %u "));
      New_Line;
      return Status;
   end show;

end gsl.permutation;
