pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

limited with gsl.rng;

limited with gsl.vector_double;
limited with gsl.matrix_double;
with System;

package gsl.randist is

   function bernoulli
     (r : access constant gsl.rng.gsl_rng;
      p : double)
      return unsigned  -- /usr/include/gsl/gsl_randist.h:38
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_bernoulli";

   function bernoulli_pdf
     (k : unsigned;
      p : double)
      return double  -- /usr/include/gsl/gsl_randist.h:39
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_bernoulli_pdf";

   function beta
     (r : access constant gsl.rng.gsl_rng; a : double; b : double)
      return double  -- /usr/include/gsl/gsl_randist.h:41
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_beta";

   function beta_pdf
     (x : double;
      a : double;
      b : double)
      return double  -- /usr/include/gsl/gsl_randist.h:42
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_beta_pdf";

   function binomial
     (r : access constant gsl.rng.gsl_rng; p : double; n : unsigned)
      return unsigned  -- /usr/include/gsl/gsl_randist.h:44
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_binomial";

   function binomial_knuth
     (r : access constant gsl.rng.gsl_rng; p : double; n : unsigned)
      return unsigned  -- /usr/include/gsl/gsl_randist.h:45
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_binomial_knuth";

   function binomial_tpe
     (r : access constant gsl.rng.gsl_rng; p : double; n : unsigned)
      return unsigned  -- /usr/include/gsl/gsl_randist.h:46
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_binomial_tpe";

   function binomial_pdf
     (k : unsigned;
      p : double;
      n : unsigned)
      return double  -- /usr/include/gsl/gsl_randist.h:47
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_binomial_pdf";

   function exponential
     (r  : access constant gsl.rng.gsl_rng;
      mu : double)
      return double  -- /usr/include/gsl/gsl_randist.h:49
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_exponential";

   function exponential_pdf
     (x  : double;
      mu : double)
      return double  -- /usr/include/gsl/gsl_randist.h:50
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_exponential_pdf";

   function exppow
     (r : access constant gsl.rng.gsl_rng; a : double; b : double)
      return double  -- /usr/include/gsl/gsl_randist.h:52
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_exppow";

   function exppow_pdf
     (x : double;
      a : double;
      b : double)
      return double  -- /usr/include/gsl/gsl_randist.h:53
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_exppow_pdf";

   function cauchy
     (r : access constant gsl.rng.gsl_rng;
      a : double)
      return double  -- /usr/include/gsl/gsl_randist.h:55
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_cauchy";

   function cauchy_pdf
     (x : double;
      a : double)
      return double  -- /usr/include/gsl/gsl_randist.h:56
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_cauchy_pdf";

   function chisq
     (r  : access constant gsl.rng.gsl_rng;
      nu : double)
      return double  -- /usr/include/gsl/gsl_randist.h:58
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_chisq";

   function chisq_pdf
     (x  : double;
      nu : double)
      return double  -- /usr/include/gsl/gsl_randist.h:59
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_chisq_pdf";

   procedure dirichlet
     (r : access constant gsl.rng.gsl_rng; K : size_t; alpha : access double;
      theta : access double)  -- /usr/include/gsl/gsl_randist.h:61
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_dirichlet";

   function dirichlet_pdf
     (K : size_t; alpha : access double; theta : access double)
      return double  -- /usr/include/gsl/gsl_randist.h:62
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_dirichlet_pdf";

   function dirichlet_lnpdf
     (K : size_t; alpha : access double; theta : access double)
      return double  -- /usr/include/gsl/gsl_randist.h:63
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_dirichlet_lnpdf";

   function erlang
     (r : access constant gsl.rng.gsl_rng; a : double; n : double)
      return double  -- /usr/include/gsl/gsl_randist.h:65
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_erlang";

   function erlang_pdf
     (x : double;
      a : double;
      n : double)
      return double  -- /usr/include/gsl/gsl_randist.h:66
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_erlang_pdf";

   function fdist
     (r : access constant gsl.rng.gsl_rng; nu1 : double; nu2 : double)
      return double  -- /usr/include/gsl/gsl_randist.h:68
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_fdist";

   function fdist_pdf
     (x   : double;
      nu1 : double;
      nu2 : double)
      return double  -- /usr/include/gsl/gsl_randist.h:69
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_fdist_pdf";

   function flat
     (r : access constant gsl.rng.gsl_rng; a : double; b : double)
      return double  -- /usr/include/gsl/gsl_randist.h:71
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_flat";

   function flat_pdf
     (x : double;
      a : double;
      b : double)
      return double  -- /usr/include/gsl/gsl_randist.h:72
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_flat_pdf";

   function gamma
     (r : access constant gsl.rng.gsl_rng; a : double; b : double)
      return double  -- /usr/include/gsl/gsl_randist.h:74
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_gamma";

   function gamma_int
     (r : access constant gsl.rng.gsl_rng;
      a : unsigned)
      return double  -- /usr/include/gsl/gsl_randist.h:75
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_gamma_int";

   function gamma_pdf
     (x : double;
      a : double;
      b : double)
      return double  -- /usr/include/gsl/gsl_randist.h:76
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_gamma_pdf";

   function gamma_mt
     (r : access constant gsl.rng.gsl_rng; a : double; b : double)
      return double  -- /usr/include/gsl/gsl_randist.h:77
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_gamma_mt";

   function gamma_knuth
     (r : access constant gsl.rng.gsl_rng; a : double; b : double)
      return double  -- /usr/include/gsl/gsl_randist.h:78
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_gamma_knuth";

   function gaussian
     (r     : access constant gsl.rng.gsl_rng;
      sigma : double)
      return double  -- /usr/include/gsl/gsl_randist.h:80
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_gaussian";

   function gaussian_ratio_method
     (r     : access constant gsl.rng.gsl_rng;
      sigma : double)
      return double  -- /usr/include/gsl/gsl_randist.h:81
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_gaussian_ratio_method";

   function gaussian_ziggurat
     (r     : access constant gsl.rng.gsl_rng;
      sigma : double)
      return double  -- /usr/include/gsl/gsl_randist.h:82
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_gaussian_ziggurat";

   function gaussian_pdf
     (x     : double;
      sigma : double)
      return double  -- /usr/include/gsl/gsl_randist.h:83
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_gaussian_pdf";

   function ugaussian
     (r : access constant gsl.rng.gsl_rng)
      return double  -- /usr/include/gsl/gsl_randist.h:85
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_ugaussian";

   function ugaussian_ratio_method
     (r : access constant gsl.rng.gsl_rng)
      return double  -- /usr/include/gsl/gsl_randist.h:86
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_ugaussian_ratio_method";

   function ugaussian_pdf
     (x : double)
      return double  -- /usr/include/gsl/gsl_randist.h:87
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_ugaussian_pdf";

   function gaussian_tail
     (r : access constant gsl.rng.gsl_rng; a : double; sigma : double)
      return double  -- /usr/include/gsl/gsl_randist.h:89
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_gaussian_tail";

   function gaussian_tail_pdf
     (x     : double;
      a     : double;
      sigma : double)
      return double  -- /usr/include/gsl/gsl_randist.h:90
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_gaussian_tail_pdf";

   function ugaussian_tail
     (r : access constant gsl.rng.gsl_rng;
      a : double)
      return double  -- /usr/include/gsl/gsl_randist.h:92
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_ugaussian_tail";

   function ugaussian_tail_pdf
     (x : double;
      a : double)
      return double  -- /usr/include/gsl/gsl_randist.h:93
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_ugaussian_tail_pdf";

   procedure bivariate_gaussian
     (r : access constant gsl.rng.gsl_rng; sigma_x : double; sigma_y : double;
      rho : double;
      x   : access double;
      y   : access double)  -- /usr/include/gsl/gsl_randist.h:95
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_bivariate_gaussian";

   function bivariate_gaussian_pdf
     (x : double; y : double; sigma_x : double; sigma_y : double; rho : double)
      return double  -- /usr/include/gsl/gsl_randist.h:96
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_bivariate_gaussian_pdf";

   function multivariate_gaussian
     (r      : access constant gsl.rng.gsl_rng;
      mu     : access constant gsl.vector_double.gsl_vector;
      L      : access constant gsl.matrix_double.gsl_matrix;
      result : access gsl.vector_double.gsl_vector)
      return int  -- /usr/include/gsl/gsl_randist.h:98
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_multivariate_gaussian";

   function multivariate_gaussian_log_pdf
     (x    : access constant gsl.vector_double.gsl_vector;
      mu   : access constant gsl.vector_double.gsl_vector;
      L : access constant gsl.matrix_double.gsl_matrix; result : access double;
      work : access gsl.vector_double.gsl_vector)
      return int  -- /usr/include/gsl/gsl_randist.h:99
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_multivariate_gaussian_log_pdf";

   function multivariate_gaussian_pdf
     (x    : access constant gsl.vector_double.gsl_vector;
      mu   : access constant gsl.vector_double.gsl_vector;
      L : access constant gsl.matrix_double.gsl_matrix; result : access double;
      work : access gsl.vector_double.gsl_vector)
      return int  -- /usr/include/gsl/gsl_randist.h:104
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_multivariate_gaussian_pdf";

   function multivariate_gaussian_mean
     (X      : access constant gsl.matrix_double.gsl_matrix;
      mu_hat : access gsl.vector_double.gsl_vector)
      return int  -- /usr/include/gsl/gsl_randist.h:109
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_multivariate_gaussian_mean";

   function multivariate_gaussian_vcov
     (X         : access constant gsl.matrix_double.gsl_matrix;
      sigma_hat : access gsl.matrix_double.gsl_matrix)
      return int  -- /usr/include/gsl/gsl_randist.h:110
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_multivariate_gaussian_vcov";

   function wishart
     (r      : access constant gsl.rng.gsl_rng; df : double;
      L      : access constant gsl.matrix_double.gsl_matrix;
      result : access gsl.matrix_double.gsl_matrix;
      work   : access gsl.matrix_double.gsl_matrix)
      return int  -- /usr/include/gsl/gsl_randist.h:112
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_wishart";

   function wishart_log_pdf
     (X    : access constant gsl.matrix_double.gsl_matrix;
      L_X  : access constant gsl.matrix_double.gsl_matrix; df : double;
      L : access constant gsl.matrix_double.gsl_matrix; result : access double;
      work : access gsl.matrix_double.gsl_matrix)
      return int  -- /usr/include/gsl/gsl_randist.h:117
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_wishart_log_pdf";

   function wishart_pdf
     (X    : access constant gsl.matrix_double.gsl_matrix;
      L_X  : access constant gsl.matrix_double.gsl_matrix; df : double;
      L : access constant gsl.matrix_double.gsl_matrix; result : access double;
      work : access gsl.matrix_double.gsl_matrix)
      return int  -- /usr/include/gsl/gsl_randist.h:123
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_wishart_pdf";

   function landau
     (r : access constant gsl.rng.gsl_rng)
      return double  -- /usr/include/gsl/gsl_randist.h:130
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_landau";

   function landau_pdf
     (x : double)
      return double  -- /usr/include/gsl/gsl_randist.h:131
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_landau_pdf";

   function geometric
     (r : access constant gsl.rng.gsl_rng;
      p : double)
      return unsigned  -- /usr/include/gsl/gsl_randist.h:133
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_geometric";

   function geometric_pdf
     (k : unsigned;
      p : double)
      return double  -- /usr/include/gsl/gsl_randist.h:134
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_geometric_pdf";

   function hypergeometric
     (r : access constant gsl.rng.gsl_rng; n1 : unsigned; n2 : unsigned;
      t : unsigned)
      return unsigned  -- /usr/include/gsl/gsl_randist.h:136
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_hypergeometric";

   function hypergeometric_pdf
     (k : unsigned; n1 : unsigned; n2 : unsigned; t : unsigned)
      return double  -- /usr/include/gsl/gsl_randist.h:137
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_hypergeometric_pdf";

   function gumbel1
     (r : access constant gsl.rng.gsl_rng; a : double; b : double)
      return double  -- /usr/include/gsl/gsl_randist.h:139
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_gumbel1";

   function gumbel1_pdf
     (x : double;
      a : double;
      b : double)
      return double  -- /usr/include/gsl/gsl_randist.h:140
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_gumbel1_pdf";

   function gumbel2
     (r : access constant gsl.rng.gsl_rng; a : double; b : double)
      return double  -- /usr/include/gsl/gsl_randist.h:142
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_gumbel2";

   function gumbel2_pdf
     (x : double;
      a : double;
      b : double)
      return double  -- /usr/include/gsl/gsl_randist.h:143
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_gumbel2_pdf";

   function logistic
     (r : access constant gsl.rng.gsl_rng;
      a : double)
      return double  -- /usr/include/gsl/gsl_randist.h:145
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_logistic";

   function logistic_pdf
     (x : double;
      a : double)
      return double  -- /usr/include/gsl/gsl_randist.h:146
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_logistic_pdf";

   function lognormal
     (r : access constant gsl.rng.gsl_rng; zeta : double; sigma : double)
      return double  -- /usr/include/gsl/gsl_randist.h:148
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_lognormal";

   function lognormal_pdf
     (x     : double;
      zeta  : double;
      sigma : double)
      return double  -- /usr/include/gsl/gsl_randist.h:149
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_lognormal_pdf";

   function logarithmic
     (r : access constant gsl.rng.gsl_rng;
      p : double)
      return unsigned  -- /usr/include/gsl/gsl_randist.h:151
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_logarithmic";

   function logarithmic_pdf
     (k : unsigned;
      p : double)
      return double  -- /usr/include/gsl/gsl_randist.h:152
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_logarithmic_pdf";

   procedure multinomial
     (r : access constant gsl.rng.gsl_rng; K : size_t; NN : unsigned;
      p : access double;
      n : access unsigned)  -- /usr/include/gsl/gsl_randist.h:154
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_multinomial";

   function multinomial_pdf
     (K : size_t;
      p : access double;
      n : access unsigned)
      return double  -- /usr/include/gsl/gsl_randist.h:157
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_multinomial_pdf";

   function multinomial_lnpdf
     (K : size_t;
      p : access double;
      n : access unsigned)
      return double  -- /usr/include/gsl/gsl_randist.h:159
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_multinomial_lnpdf";

   function negative_binomial
     (r : access constant gsl.rng.gsl_rng; p : double; n : double)
      return unsigned  -- /usr/include/gsl/gsl_randist.h:163
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_negative_binomial";

   function negative_binomial_pdf
     (k : unsigned;
      p : double;
      n : double)
      return double  -- /usr/include/gsl/gsl_randist.h:164
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_negative_binomial_pdf";

   function pascal
     (r : access constant gsl.rng.gsl_rng; p : double; n : unsigned)
      return unsigned  -- /usr/include/gsl/gsl_randist.h:166
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_pascal";

   function pascal_pdf
     (k : unsigned;
      p : double;
      n : unsigned)
      return double  -- /usr/include/gsl/gsl_randist.h:167
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_pascal_pdf";

   function pareto
     (r : access constant gsl.rng.gsl_rng; a : double; b : double)
      return double  -- /usr/include/gsl/gsl_randist.h:169
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_pareto";

   function pareto_pdf
     (x : double;
      a : double;
      b : double)
      return double  -- /usr/include/gsl/gsl_randist.h:170
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_pareto_pdf";

   function poisson
     (r  : access constant gsl.rng.gsl_rng;
      mu : double)
      return unsigned  -- /usr/include/gsl/gsl_randist.h:172
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_poisson";

   procedure poisson_array
     (r       : access constant gsl.rng.gsl_rng; n : size_t;
      c_array : access unsigned;
      mu      : double)  -- /usr/include/gsl/gsl_randist.h:173
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_poisson_array";

   function poisson_pdf
     (k  : unsigned;
      mu : double)
      return double  -- /usr/include/gsl/gsl_randist.h:175
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_poisson_pdf";

   function rayleigh
     (r     : access constant gsl.rng.gsl_rng;
      sigma : double)
      return double  -- /usr/include/gsl/gsl_randist.h:177
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_rayleigh";

   function rayleigh_pdf
     (x     : double;
      sigma : double)
      return double  -- /usr/include/gsl/gsl_randist.h:178
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_rayleigh_pdf";

   function rayleigh_tail
     (r : access constant gsl.rng.gsl_rng; a : double; sigma : double)
      return double  -- /usr/include/gsl/gsl_randist.h:180
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_rayleigh_tail";

   function rayleigh_tail_pdf
     (x     : double;
      a     : double;
      sigma : double)
      return double  -- /usr/include/gsl/gsl_randist.h:181
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_rayleigh_tail_pdf";

   function tdist
     (r  : access constant gsl.rng.gsl_rng;
      nu : double)
      return double  -- /usr/include/gsl/gsl_randist.h:183
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_tdist";

   function tdist_pdf
     (x  : double;
      nu : double)
      return double  -- /usr/include/gsl/gsl_randist.h:184
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_tdist_pdf";

   function laplace
     (r : access constant gsl.rng.gsl_rng;
      a : double)
      return double  -- /usr/include/gsl/gsl_randist.h:186
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_laplace";

   function laplace_pdf
     (x : double;
      a : double)
      return double  -- /usr/include/gsl/gsl_randist.h:187
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_laplace_pdf";

   function levy
     (r : access constant gsl.rng.gsl_rng; c : double; alpha : double)
      return double  -- /usr/include/gsl/gsl_randist.h:189
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_levy";

   function levy_skew
     (r    : access constant gsl.rng.gsl_rng; c : double; alpha : double;
      beta : double)
      return double  -- /usr/include/gsl/gsl_randist.h:190
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_levy_skew";

   function weibull
     (r : access constant gsl.rng.gsl_rng; a : double; b : double)
      return double  -- /usr/include/gsl/gsl_randist.h:192
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_weibull";

   function weibull_pdf
     (x : double;
      a : double;
      b : double)
      return double  -- /usr/include/gsl/gsl_randist.h:193
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_weibull_pdf";

   procedure dir_2d
     (r : access constant gsl.rng.gsl_rng; x : access double;
      y : access double)  -- /usr/include/gsl/gsl_randist.h:195
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_dir_2d";

   procedure dir_2d_trig_method
     (r : access constant gsl.rng.gsl_rng; x : access double;
      y : access double)  -- /usr/include/gsl/gsl_randist.h:196
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_dir_2d_trig_method";

   procedure dir_3d
     (r : access constant gsl.rng.gsl_rng; x : access double;
      y : access double;
      z : access double)  -- /usr/include/gsl/gsl_randist.h:197
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_dir_3d";

   procedure dir_nd
     (r : access constant gsl.rng.gsl_rng;
      n : size_t;
      x : access double)  -- /usr/include/gsl/gsl_randist.h:198
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_dir_nd";

   procedure shuffle
     (r      : access constant gsl.rng.gsl_rng; base : System.Address;
      nmembm : size_t;
      size   : size_t)  -- /usr/include/gsl/gsl_randist.h:200
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_shuffle";

   function choose
     (r : access constant gsl.rng.gsl_rng; dest : System.Address; k : size_t;
      src  : System.Address;
      n    : size_t;
      size : size_t)
      return int  -- /usr/include/gsl/gsl_randist.h:201
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_choose";

   procedure sample
     (r : access constant gsl.rng.gsl_rng; dest : System.Address; k : size_t;
      src  : System.Address;
      n    : size_t;
      size : size_t)  -- /usr/include/gsl/gsl_randist.h:202
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_sample";

      --  skipped anonymous struct anon_anon_110

   type gsl_ran_discrete_t is record
      K : aliased size_t;  -- /usr/include/gsl/gsl_randist.h:206
      A : access size_t;  -- /usr/include/gsl/gsl_randist.h:207
      F : access double;  -- /usr/include/gsl/gsl_randist.h:208
   end record with
      Convention => C_Pass_By_Copy;  -- /usr/include/gsl/gsl_randist.h:209

   function discrete_preproc
     (K : size_t;
      P : access double)
      return access gsl_ran_discrete_t  -- /usr/include/gsl/gsl_randist.h:211
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_discrete_preproc";

   procedure discrete_free
     (g : access gsl_ran_discrete_t)  -- /usr/include/gsl/gsl_randist.h:212
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_discrete_free";

   function discrete
     (r : access constant gsl.rng.gsl_rng;
      g : access constant gsl_ran_discrete_t)
      return size_t  -- /usr/include/gsl/gsl_randist.h:213
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_discrete";

   function discrete_pdf
     (k : size_t;
      g : access constant gsl_ran_discrete_t)
      return double  -- /usr/include/gsl/gsl_randist.h:214
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_ran_discrete_pdf";

end gsl.randist;
