package body gsl.fft.halfcomplex is

   function unpack
     (halfcomplex_coefficient : access gsl.vector_double.gsl_vector;
      complex_coefficient : gsl.complex.gsl_complex_complex_array) return int
   is

   begin
      return
        unpack
          (halfcomplex_coefficient.data, complex_coefficient, 1,
           halfcomplex_coefficient.size);
   end unpack;

   function radix2_unpack
     (halfcomplex_coefficient : access gsl.vector_double.gsl_vector;
      complex_coefficient : gsl.complex.gsl_complex_complex_array) return int
   is
   begin
      return
        radix2_unpack
          (halfcomplex_coefficient.data, complex_coefficient, 1,
           halfcomplex_coefficient.size);
   end radix2_unpack;

end gsl.fft.halfcomplex;
