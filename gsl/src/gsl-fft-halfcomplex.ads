pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with gsl.vector_double;
with gsl.complex;
with gsl.fft.real;

package gsl.fft.halfcomplex is

   function radix2_backward
     (data   : double_array;
      stride : size_t;
      n      : size_t)
      return int  -- /usr/include/gsl/gsl_fft_halfcomplex.h:42
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fft_halfcomplex_radix2_backward";

   function radix2_inverse
     (data   : double_array;
      stride : size_t;
      n      : size_t)
      return int  -- /usr/include/gsl/gsl_fft_halfcomplex.h:43
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fft_halfcomplex_radix2_inverse";

   function radix2_transform
     (data   : double_array;
      stride : size_t;
      n      : size_t)
      return int  -- /usr/include/gsl/gsl_fft_halfcomplex.h:44
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fft_halfcomplex_radix2_transform";

      --  skipped anonymous struct anon_anon_73

   type gsl_fft_halfcomplex_wavetable_array10114 is
     array (0 .. 63) of aliased size_t;
   type gsl_fft_halfcomplex_wavetable_array10116 is
     array (0 .. 63) of access gsl.complex.gsl_complex;
   type gsl_fft_halfcomplex_wavetable is record
      n       : aliased size_t;  -- /usr/include/gsl/gsl_fft_halfcomplex.h:48
      nf      : aliased size_t;  -- /usr/include/gsl/gsl_fft_halfcomplex.h:49
      factor : aliased gsl_fft_halfcomplex_wavetable_array10114;  -- /usr/include/gsl/gsl_fft_halfcomplex.h:50
      twiddle : gsl_fft_halfcomplex_wavetable_array10116;  -- /usr/include/gsl/gsl_fft_halfcomplex.h:51
      trig    : access gsl.complex
        .gsl_complex;  -- /usr/include/gsl/gsl_fft_halfcomplex.h:52
   end record with
      Convention => C_Pass_By_Copy;  -- /usr/include/gsl/gsl_fft_halfcomplex.h:54

   function wavetable_alloc
     (n : size_t)
      return access gsl_fft_halfcomplex_wavetable  -- /usr/include/gsl/gsl_fft_halfcomplex.h:56
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fft_halfcomplex_wavetable_alloc";

   procedure wavetable_free
     (wavetable : access gsl_fft_halfcomplex_wavetable)  -- /usr/include/gsl/gsl_fft_halfcomplex.h:59
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fft_halfcomplex_wavetable_free";

   function backward
     (data      : double_array; stride : size_t; n : size_t;
      wavetable : access constant gsl_fft_halfcomplex_wavetable;
      work      : access gsl.fft.real.gsl_fft_real_workspace)
      return int  -- /usr/include/gsl/gsl_fft_halfcomplex.h:62
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fft_halfcomplex_backward";

   function inverse
     (data      : double_array; stride : size_t; n : size_t;
      wavetable : access constant gsl_fft_halfcomplex_wavetable;
      work      : access gsl.fft.real.gsl_fft_real_workspace)
      return int  -- /usr/include/gsl/gsl_fft_halfcomplex.h:66
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fft_halfcomplex_inverse";

   function transform
     (data      : double_array; stride : size_t; n : size_t;
      wavetable : access constant gsl_fft_halfcomplex_wavetable;
      work      : access gsl.fft.real.gsl_fft_real_workspace)
      return int  -- /usr/include/gsl/gsl_fft_halfcomplex.h:70
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fft_halfcomplex_transform";

   function unpack
     (halfcomplex_coefficient : access double;
      complex_coefficient     : gsl.complex.gsl_complex_complex_array;
      stride                  : size_t;
      n                       : size_t)
      return int  -- /usr/include/gsl/gsl_fft_halfcomplex.h:75
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fft_halfcomplex_unpack";

   function unpack
     (halfcomplex_coefficient : double_array;
      complex_coefficient     : gsl.complex.gsl_complex_complex_array;
      stride                  : size_t;
      n                       : size_t)
      return int  -- /usr/include/gsl/gsl_fft_halfcomplex.h:75
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fft_halfcomplex_unpack";

   function unpack
     (halfcomplex_coefficient : access gsl.vector_double.gsl_vector;
      complex_coefficient : gsl.complex.gsl_complex_complex_array) return int;

   function radix2_unpack
     (halfcomplex_coefficient : access double;
      complex_coefficient     : gsl.complex.gsl_complex_complex_array;
      stride                  : size_t;
      n                       : size_t)
      return int  -- /usr/include/gsl/gsl_fft_halfcomplex.h:80
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fft_halfcomplex_radix2_unpack";

   function radix2_unpack
     (halfcomplex_coefficient : double_array;
      complex_coefficient     : gsl.complex.gsl_complex_complex_array;
      stride                  : size_t;
      n                       : size_t)
      return int  -- /usr/include/gsl/gsl_fft_halfcomplex.h:80
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fft_halfcomplex_radix2_unpack";

   function radix2_unpack
     (halfcomplex_coefficient : access gsl.vector_double.gsl_vector;
      complex_coefficient : gsl.complex.gsl_complex_complex_array) return int;

end gsl.fft.halfcomplex;
