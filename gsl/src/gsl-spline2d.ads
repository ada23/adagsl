pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with gsl.interp2d;
with gsl.interp;
with Interfaces.C.Strings;

package gsl.spline2d is

   --  skipped anonymous struct anon_anon_22

   type gsl_spline2d is record
      interp_object : aliased gsl.interp2d
        .gsl_interp2d;  -- /usr/include/gsl/gsl_spline2d.h:45
      xarr : access double;  -- /usr/include/gsl/gsl_spline2d.h:46
      yarr : access double;  -- /usr/include/gsl/gsl_spline2d.h:47
      zarr : access double;  -- /usr/include/gsl/gsl_spline2d.h:48
   end record with
      Convention => C_Pass_By_Copy;  -- /usr/include/gsl/gsl_spline2d.h:49

   function alloc
     (T     : access constant gsl.interp2d.gsl_interp2d_type; xsize : size_t;
      ysize : size_t)
      return access gsl_spline2d  -- /usr/include/gsl/gsl_spline2d.h:51
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spline2d_alloc";

   function init
     (interp : access gsl_spline2d; xa : access double; ya : access double;
      za     : access double;
      xsize  : size_t;
      ysize  : size_t)
      return int  -- /usr/include/gsl/gsl_spline2d.h:53
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spline2d_init";

   procedure gsl_spline2d_free
     (interp : access gsl_spline2d)  -- /usr/include/gsl/gsl_spline2d.h:57
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spline2d_free";

   function eval
     (interp : access constant gsl_spline2d; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel)
      return double  -- /usr/include/gsl/gsl_spline2d.h:59
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spline2d_eval";

   function eval_e
     (interp : access constant gsl_spline2d; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel; z : access double)
      return int  -- /usr/include/gsl/gsl_spline2d.h:62
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spline2d_eval_e";

   function eval_extrap
     (interp : access constant gsl_spline2d; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel)
      return double  -- /usr/include/gsl/gsl_spline2d.h:66
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spline2d_eval_extrap";

   function eval_extrap_e
     (interp : access constant gsl_spline2d; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel; z : access double)
      return int  -- /usr/include/gsl/gsl_spline2d.h:69
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spline2d_eval_extrap_e";

   function eval_deriv_x
     (interp : access constant gsl_spline2d; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel)
      return double  -- /usr/include/gsl/gsl_spline2d.h:73
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spline2d_eval_deriv_x";

   function eval_deriv_x_e
     (interp : access constant gsl_spline2d; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel; z : access double)
      return int  -- /usr/include/gsl/gsl_spline2d.h:76
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spline2d_eval_deriv_x_e";

   function eval_deriv_y
     (interp : access constant gsl_spline2d; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel)
      return double  -- /usr/include/gsl/gsl_spline2d.h:80
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spline2d_eval_deriv_y";

   function eval_deriv_y_e
     (interp : access constant gsl_spline2d; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel; z : access double)
      return int  -- /usr/include/gsl/gsl_spline2d.h:84
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spline2d_eval_deriv_y_e";

   function eval_deriv_xx
     (interp : access constant gsl_spline2d; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel)
      return double  -- /usr/include/gsl/gsl_spline2d.h:88
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spline2d_eval_deriv_xx";

   function eval_deriv_xx_e
     (interp : access constant gsl_spline2d; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel; z : access double)
      return int  -- /usr/include/gsl/gsl_spline2d.h:91
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spline2d_eval_deriv_xx_e";

   function eval_deriv_yy
     (interp : access constant gsl_spline2d; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel)
      return double  -- /usr/include/gsl/gsl_spline2d.h:95
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spline2d_eval_deriv_yy";

   function eval_deriv_yy_e
     (interp : access constant gsl_spline2d; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel; z : access double)
      return int  -- /usr/include/gsl/gsl_spline2d.h:98
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spline2d_eval_deriv_yy_e";

   function eval_deriv_xy
     (interp : access constant gsl_spline2d; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel)
      return double  -- /usr/include/gsl/gsl_spline2d.h:102
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spline2d_eval_deriv_xy";

   function eval_deriv_xy_e
     (interp : access constant gsl_spline2d; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel; z : access double)
      return int  -- /usr/include/gsl/gsl_spline2d.h:105
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spline2d_eval_deriv_xy_e";

   function min_size
     (interp : access constant gsl_spline2d)
      return size_t  -- /usr/include/gsl/gsl_spline2d.h:109
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spline2d_min_size";

   function name
     (interp : access constant gsl_spline2d)
      return Interfaces.C.Strings
     .chars_ptr  -- /usr/include/gsl/gsl_spline2d.h:111
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spline2d_name";

   function set
     (interp : access constant gsl_spline2d; zarr : access double; i : size_t;
      j      : size_t;
      z      : double)
      return int  -- /usr/include/gsl/gsl_spline2d.h:113
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spline2d_set";

   function get
     (interp : access constant gsl_spline2d; zarr : access double; i : size_t;
      j      : size_t)
      return double  -- /usr/include/gsl/gsl_spline2d.h:115
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spline2d_get";

end gsl.spline2d;
