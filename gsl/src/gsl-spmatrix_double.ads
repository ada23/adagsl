pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with System;

with gsl.bst;
with Interfaces.C.Strings;

with gsl.vector_double;
with gsl.matrix_double;

package gsl.spmatrix_double is

   --  skipped anonymous struct anon_anon_86

   type anon_anon_87 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            work_void : System
              .Address;  -- /usr/include/gsl/gsl_spmatrix_double.h:99
         when 1 =>
            work_int : access int;  -- /usr/include/gsl/gsl_spmatrix_double.h:100
         when others =>
            work_atomic : access double;  -- /usr/include/gsl/gsl_spmatrix_double.h:101
      end case;
   end record with
      Convention      => C_Pass_By_Copy,
      Unchecked_Union => True;
   type gsl_spmatrix is record
      size1 : aliased size_t;  -- /usr/include/gsl/gsl_spmatrix_double.h:67
      size2 : aliased size_t;  -- /usr/include/gsl/gsl_spmatrix_double.h:68
      i     : access int;  -- /usr/include/gsl/gsl_spmatrix_double.h:75
      data  : access double;  -- /usr/include/gsl/gsl_spmatrix_double.h:77
      p     : access int;  -- /usr/include/gsl/gsl_spmatrix_double.h:84
      nzmax : aliased size_t;  -- /usr/include/gsl/gsl_spmatrix_double.h:86
      nz    : aliased size_t;  -- /usr/include/gsl/gsl_spmatrix_double.h:87
      tree  : access gsl.bst
        .gsl_bst_workspace;  -- /usr/include/gsl/gsl_spmatrix_double.h:89
      node_size : aliased size_t;  -- /usr/include/gsl/gsl_spmatrix_double.h:91
      work : aliased anon_anon_87;  -- /usr/include/gsl/gsl_spmatrix_double.h:102
      sptype    : aliased int;  -- /usr/include/gsl/gsl_spmatrix_double.h:104
      spflags : aliased size_t;  -- /usr/include/gsl/gsl_spmatrix_double.h:105
   end record with
      Convention => C_Pass_By_Copy;  -- /usr/include/gsl/gsl_spmatrix_double.h:106

   function alloc
     (n1 : size_t;
      n2 : size_t)
      return access gsl_spmatrix  -- /usr/include/gsl/gsl_spmatrix_double.h:114
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_alloc";

   function alloc_nzmax
     (n1 : size_t; n2 : size_t; nzmax : size_t; sptype : int)
      return access gsl_spmatrix  -- /usr/include/gsl/gsl_spmatrix_double.h:115
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_alloc_nzmax";

   procedure gsl_spmatrix_free
     (m : access gsl_spmatrix)  -- /usr/include/gsl/gsl_spmatrix_double.h:117
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_free";

   function realloc
     (nzmax : size_t;
      m     : access gsl_spmatrix)
      return int  -- /usr/include/gsl/gsl_spmatrix_double.h:118
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_realloc";

   function nnz
     (m : access constant gsl_spmatrix)
      return size_t  -- /usr/include/gsl/gsl_spmatrix_double.h:119
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_nnz";

   function typeof
     (m : access constant gsl_spmatrix)
      return Interfaces.C.Strings
     .chars_ptr  -- /usr/include/gsl/gsl_spmatrix_double.h:120
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_type";

   function set_zero
     (m : access gsl_spmatrix)
      return int  -- /usr/include/gsl/gsl_spmatrix_double.h:121
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_set_zero";

   function tree_rebuild
     (m : access gsl_spmatrix)
      return int  -- /usr/include/gsl/gsl_spmatrix_double.h:122
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_tree_rebuild";

   function csc
     (dest : access gsl_spmatrix; src : access constant gsl_spmatrix)
      return int  -- /usr/include/gsl/gsl_spmatrix_double.h:126
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_csc";

   function csr
     (dest : access gsl_spmatrix; src : access constant gsl_spmatrix)
      return int  -- /usr/include/gsl/gsl_spmatrix_double.h:127
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_csr";

   function compress
     (src : access constant gsl_spmatrix; sptype : int)
      return access gsl_spmatrix  -- /usr/include/gsl/gsl_spmatrix_double.h:128
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_compress";

   function compcol
     (src : access constant gsl_spmatrix)
      return access gsl_spmatrix  -- /usr/include/gsl/gsl_spmatrix_double.h:129
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_compcol";

   function ccs
     (src : access constant gsl_spmatrix)
      return access gsl_spmatrix  -- /usr/include/gsl/gsl_spmatrix_double.h:130
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_ccs";

   function crs
     (src : access constant gsl_spmatrix)
      return access gsl_spmatrix  -- /usr/include/gsl/gsl_spmatrix_double.h:131
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_crs";

   function memcpy
     (dest : access gsl_spmatrix; src : access constant gsl_spmatrix)
      return int  -- /usr/include/gsl/gsl_spmatrix_double.h:135
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_memcpy";

   function fprintf
     (stream : ICS.FILEs; m : access constant gsl_spmatrix;
      format : Interfaces.C.Strings.chars_ptr)
      return int  -- /usr/include/gsl/gsl_spmatrix_double.h:139
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_fprintf";

   function fscanf
     (stream : ICS.FILEs)
      return access gsl_spmatrix  -- /usr/include/gsl/gsl_spmatrix_double.h:140
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_fscanf";

   function fwrite
     (stream : ICS.FILEs; m : access constant gsl_spmatrix)
      return int  -- /usr/include/gsl/gsl_spmatrix_double.h:141
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_fwrite";

   function fread
     (stream : ICS.FILEs;
      m      : access gsl_spmatrix)
      return int  -- /usr/include/gsl/gsl_spmatrix_double.h:142
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_fread";

   function get
     (m : access constant gsl_spmatrix; i : size_t; j : size_t)
      return double  -- /usr/include/gsl/gsl_spmatrix_double.h:146
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_get";

   function set
     (m : access gsl_spmatrix; i : size_t; j : size_t; x : double)
      return int  -- /usr/include/gsl/gsl_spmatrix_double.h:147
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_set";

   function ptr
     (m : access constant gsl_spmatrix; i : size_t; j : size_t)
      return access double  -- /usr/include/gsl/gsl_spmatrix_double.h:148
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_ptr";

   function minmax
     (m       : access constant gsl_spmatrix; min_out : access double;
      max_out : access double)
      return int  -- /usr/include/gsl/gsl_spmatrix_double.h:152
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_minmax";

   function min_index
     (m        : access constant gsl_spmatrix; imin_out : access size_t;
      jmin_out : access size_t)
      return int  -- /usr/include/gsl/gsl_spmatrix_double.h:153
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_min_index";

   function scale
     (m : access gsl_spmatrix;
      x : double)
      return int  -- /usr/include/gsl/gsl_spmatrix_double.h:157
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_scale";

   function scale_columns
     (m : access gsl_spmatrix;
      x : access constant gsl.vector_double.gsl_vector)
      return int  -- /usr/include/gsl/gsl_spmatrix_double.h:158
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_scale_columns";

   function scale_rows
     (m : access gsl_spmatrix;
      x : access constant gsl.vector_double.gsl_vector)
      return int  -- /usr/include/gsl/gsl_spmatrix_double.h:159
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_scale_rows";

   function add
     (c : access gsl_spmatrix; a : access constant gsl_spmatrix;
      b : access constant gsl_spmatrix)
      return int  -- /usr/include/gsl/gsl_spmatrix_double.h:160
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_add";

   function dense_add
     (a : access gsl.matrix_double.gsl_matrix;
      b : access constant gsl_spmatrix)
      return int  -- /usr/include/gsl/gsl_spmatrix_double.h:161
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_dense_add";

   function dense_sub
     (a : access gsl.matrix_double.gsl_matrix;
      b : access constant gsl_spmatrix)
      return int  -- /usr/include/gsl/gsl_spmatrix_double.h:162
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_dense_sub";

   function d2sp
     (T : access gsl_spmatrix;
      A : access constant gsl.matrix_double.gsl_matrix)
      return int  -- /usr/include/gsl/gsl_spmatrix_double.h:163
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_d2sp";

   function sp2d
     (A : access gsl.matrix_double.gsl_matrix;
      S : access constant gsl_spmatrix)
      return int  -- /usr/include/gsl/gsl_spmatrix_double.h:164
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_sp2d";

   function add_to_dense
     (a : access gsl.matrix_double.gsl_matrix;
      b : access constant gsl_spmatrix)
      return int  -- /usr/include/gsl/gsl_spmatrix_double.h:168
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_add_to_dense";

   function equal
     (a : access constant gsl_spmatrix; b : access constant gsl_spmatrix)
      return int  -- /usr/include/gsl/gsl_spmatrix_double.h:174
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_equal";

   function norm1
     (a : access constant gsl_spmatrix)
      return double  -- /usr/include/gsl/gsl_spmatrix_double.h:175
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_norm1";

   function transpose
     (m : access gsl_spmatrix)
      return int  -- /usr/include/gsl/gsl_spmatrix_double.h:179
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_transpose";

   function transpose2
     (m : access gsl_spmatrix)
      return int  -- /usr/include/gsl/gsl_spmatrix_double.h:180
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_transpose2";

   function transpose_memcpy
     (dest : access gsl_spmatrix; src : access constant gsl_spmatrix)
      return int  -- /usr/include/gsl/gsl_spmatrix_double.h:181
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_spmatrix_transpose_memcpy";

end gsl.spmatrix_double;
