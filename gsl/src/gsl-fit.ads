pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

package gsl.fit is

   function linear
     (x : double_array; xstride : size_t; y : double_array; ystride : size_t;
      n     : size_t; c0 : access double; c1 : access double;
      cov00 : access double; cov01 : access double; cov11 : access double;
      sumsq : access double)
      return int  -- /usr/include/gsl/gsl_fit.h:38
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fit_linear";

   function wlinear
     (x : double_array; xstride : size_t; w : double_array; wstride : size_t;
      y     : double_array; ystride : size_t; n : size_t; c0 : access double;
      c1    : access double; cov00 : access double; cov01 : access double;
      cov11 : access double;
      chisq : access double)
      return int  -- /usr/include/gsl/gsl_fit.h:46
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fit_wlinear";

   function linear_est
     (x     : double; c0 : double; c1 : double; cov00 : double; cov01 : double;
      cov11 : double;
      y     : access double;
      y_err : access double)
      return int  -- /usr/include/gsl/gsl_fit.h:55
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fit_linear_est";

   function mul
     (x : double_array; xstride : size_t; y : double_array; ystride : size_t;
      n     : size_t; c1 : access double; cov11 : access double;
      sumsq : access double)
      return int  -- /usr/include/gsl/gsl_fit.h:61
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fit_mul";

   function wmul
     (x : double_array; xstride : size_t; w : double_array; wstride : size_t;
      y     : double_array; ystride : size_t; n : size_t; c1 : access double;
      cov11 : access double;
      sumsq : access double)
      return int  -- /usr/include/gsl/gsl_fit.h:68
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fit_wmul";

   function mul_est
     (x     : double; c1 : double; cov11 : double; y : access double;
      y_err : access double)
      return int  -- /usr/include/gsl/gsl_fit.h:78
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_fit_mul_est";

end gsl.fit;
