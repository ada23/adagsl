pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");
with System;
with Interfaces.C.Strings;

package gsl.combination is

   type gsl_combination_struct is record
      n    : aliased size_t;  -- /usr/include/gsl/gsl_combination.h:44
      k    : aliased size_t;  -- /usr/include/gsl/gsl_combination.h:45
      data : System.Address;  -- /usr/include/gsl/gsl_combination.h:46
   end record with
      Convention => C_Pass_By_Copy;  -- /usr/include/gsl/gsl_combination.h:42

   subtype gsl_combination is
     gsl_combination_struct;  -- /usr/include/gsl/gsl_combination.h:49

   function alloc
     (n : size_t;
      k : size_t)
      return access gsl_combination  -- /usr/include/gsl/gsl_combination.h:51
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_combination_alloc";

   function calloc
     (n : size_t;
      k : size_t)
      return access gsl_combination  -- /usr/include/gsl/gsl_combination.h:52
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_combination_calloc";

   procedure init_first
     (c : access gsl_combination)  -- /usr/include/gsl/gsl_combination.h:53
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_combination_init_first";

   procedure init_last
     (c : access gsl_combination)  -- /usr/include/gsl/gsl_combination.h:54
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_combination_init_last";

   procedure free
     (c : access gsl_combination)  -- /usr/include/gsl/gsl_combination.h:55
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_combination_free";

   function memcpy
     (dest : access gsl_combination; src : access constant gsl_combination)
      return int  -- /usr/include/gsl/gsl_combination.h:56
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_combination_memcpy";

   function fread
     (stream : ICS.FILEs;
      c      : access gsl_combination)
      return int  -- /usr/include/gsl/gsl_combination.h:58
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_combination_fread";

   function fwrite
     (stream : ICS.FILEs;
      c      : access constant gsl_combination)
      return int  -- /usr/include/gsl/gsl_combination.h:59
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_combination_fwrite";

   function fscanf
     (stream : ICS.FILEs;
      c      : access gsl_combination)
      return int  -- /usr/include/gsl/gsl_combination.h:60
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_combination_fscanf";

   function fprintf
     (stream : ICS.FILEs; c : access constant gsl_combination;
      format : Interfaces.C.Strings.chars_ptr)
      return int  -- /usr/include/gsl/gsl_combination.h:61
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_combination_fprintf";
   function show (c : access gsl_combination) return int;

   function n
     (c : access constant gsl_combination)
      return size_t  -- /usr/include/gsl/gsl_combination.h:63
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_combination_n";

   function k
     (c : access constant gsl_combination)
      return size_t  -- /usr/include/gsl/gsl_combination.h:64
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_combination_k";

   function data
     (c : access constant gsl_combination)
      return access size_t  -- /usr/include/gsl/gsl_combination.h:65
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_combination_data";

   function valid
     (c : access gsl_combination)
      return int  -- /usr/include/gsl/gsl_combination.h:67
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_combination_valid";

   function next
     (c : access gsl_combination)
      return int  -- /usr/include/gsl/gsl_combination.h:68
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_combination_next";

   function prev
     (c : access gsl_combination)
      return int  -- /usr/include/gsl/gsl_combination.h:69
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_combination_prev";

   function get
     (c : access constant gsl_combination;
      i : size_t)
      return size_t  -- /usr/include/gsl/gsl_combination.h:71
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_combination_get";

end gsl.combination;
