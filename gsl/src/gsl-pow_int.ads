pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

package gsl.pow_int is

   function pow_2
     (x : double)
      return double  -- /usr/include/gsl/gsl_pow_int.h:36
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_pow_2";

   function pow_3
     (x : double)
      return double  -- /usr/include/gsl/gsl_pow_int.h:37
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_pow_3";

   function pow_4
     (x : double)
      return double  -- /usr/include/gsl/gsl_pow_int.h:38
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_pow_4";

   function pow_5
     (x : double)
      return double  -- /usr/include/gsl/gsl_pow_int.h:39
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_pow_5";

   function pow_6
     (x : double)
      return double  -- /usr/include/gsl/gsl_pow_int.h:40
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_pow_6";

   function pow_7
     (x : double)
      return double  -- /usr/include/gsl/gsl_pow_int.h:41
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_pow_7";

   function pow_8
     (x : double)
      return double  -- /usr/include/gsl/gsl_pow_int.h:42
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_pow_8";

   function pow_9
     (x : double)
      return double  -- /usr/include/gsl/gsl_pow_int.h:43
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_pow_9";

   function pow_int
     (x : double;
      n : int)
      return double  -- /usr/include/gsl/gsl_pow_int.h:56
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_pow_int";

   function pow_uint
     (x : double;
      n : unsigned)
      return double  -- /usr/include/gsl/gsl_pow_int.h:57
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_pow_uint";

end gsl.pow_int;
