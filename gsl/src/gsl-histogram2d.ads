pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

package gsl.histogram2d is

   type gsl_histogram2d is record
      nx : aliased size_t;  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:39
      ny : aliased size_t;  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:39
      xrange : access double;  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:40
      yrange : access double;  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:41
      bin : access double;  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:42
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:43

   type gsl_histogram2d_pdf is record
      nx : aliased size_t;  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:46
      ny : aliased size_t;  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:46
      xrange : access double;  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:47
      yrange : access double;  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:48
      sum : access double;  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:49
   end record
   with Convention => C_Pass_By_Copy;  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:50

   function alloc (nx : size_t; ny : size_t) return access gsl_histogram2d  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:52
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_alloc";

   function calloc (nx : size_t; ny : size_t) return access gsl_histogram2d  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:53
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_calloc";

   function calloc_uniform
     (nx : size_t;
      ny : size_t;
      xmin : double;
      xmax : double;
      ymin : double;
      ymax : double) return access gsl_histogram2d  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:54
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_calloc_uniform";

   procedure free (h : access gsl_histogram2d)  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:58
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_free";

   function increment
     (h : access gsl_histogram2d;
      x : double;
      y : double) return int  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:60
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_increment";

   function accumulate
     (h : access gsl_histogram2d;
      x : double;
      y : double;
      weight : double) return int  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:61
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_accumulate";

   function find
     (h : access constant gsl_histogram2d;
      x : double;
      y : double;
      i : access size_t;
      j : access size_t) return int  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:63
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_find";

   function get
     (h : access constant gsl_histogram2d;
      i : size_t;
      j : size_t) return double  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:66
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_get";

   function get_xrange
     (h : access constant gsl_histogram2d;
      i : size_t;
      xlower : access double;
      xupper : access double) return int  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:67
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_get_xrange";

   function get_yrange
     (h : access constant gsl_histogram2d;
      j : size_t;
      ylower : access double;
      yupper : access double) return int  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:69
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_get_yrange";

   function xmax (h : access constant gsl_histogram2d) return double  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:73
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_xmax";

   function xmin (h : access constant gsl_histogram2d) return double  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:74
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_xmin";

   function nx (h : access constant gsl_histogram2d) return size_t  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:75
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_nx";

   function ymax (h : access constant gsl_histogram2d) return double  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:77
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_ymax";

   function ymin (h : access constant gsl_histogram2d) return double  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:78
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_ymin";

   function ny (h : access constant gsl_histogram2d) return size_t  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:79
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_ny";

   procedure reset (h : access gsl_histogram2d)  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:81
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_reset";

   function calloc_range
     (nx : size_t;
      ny : size_t;
      xrange : access double;
      yrange : access double) return access gsl_histogram2d  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:84
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_calloc_range";

   function set_ranges_uniform
     (h : access gsl_histogram2d;
      xmin : double;
      xmax : double;
      ymin : double;
      ymax : double) return int  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:88
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_set_ranges_uniform";

   function set_ranges
     (h : access gsl_histogram2d;
      xrange : access double;
      xsize : size_t;
      yrange : access double;
      ysize : size_t) return int  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:93
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_set_ranges";

   function memcpy (dest : access gsl_histogram2d; source : access constant gsl_histogram2d) return int  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:98
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_memcpy";

   function clone (source : access constant gsl_histogram2d) return access gsl_histogram2d  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:101
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_clone";

   function max_val (h : access constant gsl_histogram2d) return double  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:104
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_max_val";

   procedure max_bin
     (h : access constant gsl_histogram2d;
      i : access size_t;
      j : access size_t)  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:107
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_max_bin";

   function min_val (h : access constant gsl_histogram2d) return double  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:110
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_min_val";

   procedure min_bin
     (h : access constant gsl_histogram2d;
      i : access size_t;
      j : access size_t)  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:113
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_min_bin";

   function xmean (h : access constant gsl_histogram2d) return double  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:116
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_xmean";

   function ymean (h : access constant gsl_histogram2d) return double  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:119
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_ymean";

   function xsigma (h : access constant gsl_histogram2d) return double  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:122
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_xsigma";

   function ysigma (h : access constant gsl_histogram2d) return double  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:125
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_ysigma";

   function cov (h : access constant gsl_histogram2d) return double  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:128
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_cov";

   function sum (h : access constant gsl_histogram2d) return double  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:131
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_sum";

   function equal_bins_p (h1 : access constant gsl_histogram2d; h2 : access constant gsl_histogram2d) return int  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:134
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_equal_bins_p";

   function add (h1 : access gsl_histogram2d; h2 : access constant gsl_histogram2d) return int  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:138
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_add";

   function sub (h1 : access gsl_histogram2d; h2 : access constant gsl_histogram2d) return int  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:141
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_sub";

   function mul (h1 : access gsl_histogram2d; h2 : access constant gsl_histogram2d) return int  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:144
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_mul";

   function div (h1 : access gsl_histogram2d; h2 : access constant gsl_histogram2d) return int  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:147
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_div";

   function scale (h : access gsl_histogram2d; scale : double) return int  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:150
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_scale";

   function shift (h : access gsl_histogram2d; shift : double) return int  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:153
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_shift";

   function fwrite (stream : ICS.Files; h : access constant gsl_histogram2d) return int  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:155
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_fwrite";

   function fread (stream : ICS.Files; h : access gsl_histogram2d) return int  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:156
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_fread";

   function fprintf
     (stream : ICS.Files;
      h : access constant gsl_histogram2d;
      range_format : Interfaces.C.Strings.chars_ptr;
      bin_format : Interfaces.C.Strings.chars_ptr) return int  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:157
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_fprintf";

   function fscanf (stream : ICS.Files; h : access gsl_histogram2d) return int  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:160
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_fscanf";

   function pdf_alloc (nx : size_t; ny : size_t) return access gsl_histogram2d_pdf  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:162
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_pdf_alloc";

   function pdf_init (p : access gsl_histogram2d_pdf; h : access constant gsl_histogram2d) return int  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:163
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_pdf_init";

   procedure pdf_free (p : access gsl_histogram2d_pdf)  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:164
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_pdf_free";

   function pdf_sample
     (p : access constant gsl_histogram2d_pdf;
      r1 : double;
      r2 : double;
      x : access double;
      y : access double) return int  -- /opt/homebrew/include/gsl/gsl_histogram2d.h:165
   with Import => True, 
        Convention => C, 
        External_Name => "gsl_histogram2d_pdf_sample";

end gsl.histogram2d ;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
