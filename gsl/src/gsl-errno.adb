package body gsl.errno is
   use Interfaces.C.Strings;

   procedure error
     (reason : String; file : String; line : int; gsl_errno : int)
   is
   begin
      error (New_String (reason), New_String (file), line, gsl_errno);
   end error;

   procedure stream_printf
     (label : String; file : String; line : int; reason : String)
   is
   begin
      stream_printf
        (New_String (label), New_String (file), line, New_String (reason));
   end stream_printf;

   function strerror (gsl_errno : int) return String is
   begin
      return Value (StrError (gsl_errno));
   end strerror;
end gsl.errno;
