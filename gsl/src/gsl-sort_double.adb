package body gsl.sort_double is

   procedure sort (data : in out double_array) is
   begin
      sort (data, 1, data'Length);
   end sort;


   procedure index
     (p : access gsl.vector_int.gsl_vector ; 
     data : access gsl.vector_double.gsl_vector ; stride : size_t;
      n : size_t) is  -- /usr/include/gsl/gsl_sort_double.h:41
   begin
      index( gsl.vector_int.ptr(p,0)  , data.data , stride , n );
   end index ;

end gsl.sort_double;
