pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

package gsl.complex.math is

   --  unsupported macro: GSL_COMPLEX_ONE (gsl_complex_rect(1.0,0.0))
   --  unsupported macro: GSL_COMPLEX_ZERO (gsl_complex_rect(0.0,0.0))
   --  unsupported macro: GSL_COMPLEX_NEGONE (gsl_complex_rect(-1.0,0.0))

   use double_complex_types;

   GSL_COMPLEX_ONE    : constant gsl_complex := (Re => 1.0, Im => 0.0);
   GSL_COMPLEX_ZERO   : constant gsl_complex := (Re => 0.0, Im => 0.0);
   GSL_COMPLEX_NEGONE : constant gsl_complex := (Re => -1.0, Im => 0.0);

   function polar
     (r     : double;
      theta : double)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:39
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_polar";

   function rect
     (x : double;
      y : double)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:41
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_rect";

   function arg
     (z : gsl_complex)
      return double  -- /usr/include/gsl/gsl_complex_math.h:59
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_arg";

   function cabs
     (z : gsl_complex)
      return double  -- /usr/include/gsl/gsl_complex_math.h:60
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_abs";

   function abs2
     (z : gsl_complex)
      return double  -- /usr/include/gsl/gsl_complex_math.h:61
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_abs2";

   function logabs
     (z : gsl_complex)
      return double  -- /usr/include/gsl/gsl_complex_math.h:62
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_logabs";

   function add
     (a : gsl_complex;
      b : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:66
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_add";

   function sub
     (a : gsl_complex;
      b : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:67
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_sub";

   function mul
     (a : gsl_complex;
      b : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:68
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_mul";

   function div
     (a : gsl_complex;
      b : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:69
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_div";

   function add_real
     (a : gsl_complex;
      x : double)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:71
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_add_real";

   function sub_real
     (a : gsl_complex;
      x : double)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:72
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_sub_real";

   function mul_real
     (a : gsl_complex;
      x : double)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:73
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_mul_real";

   function div_real
     (a : gsl_complex;
      x : double)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:74
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_div_real";

   function add_imag
     (a : gsl_complex;
      y : double)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:76
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_add_imag";

   function sub_imag
     (a : gsl_complex;
      y : double)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:77
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_sub_imag";

   function mul_imag
     (a : gsl_complex;
      y : double)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:78
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_mul_imag";

   function div_imag
     (a : gsl_complex;
      y : double)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:79
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_div_imag";

   function conjugate
     (z : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:81
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_conjugate";

   function inverse
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:82
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_inverse";

   function negative
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:83
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_negative";

   function sqrt
     (z : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:87
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_sqrt";

   function sqrt_real
     (x : double)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:88
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_sqrt_real";

   function pow
     (a : gsl_complex;
      b : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:90
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_pow";

   function pow_real
     (a : gsl_complex;
      b : double)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:91
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_pow_real";

   function exp
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:93
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_exp";

   function log
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:94
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_log";

   function log10
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:95
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_log10";

   function log_b
     (a : gsl_complex;
      b : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:96
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_log_b";

   function sin
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:100
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_sin";

   function cos
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:101
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_cos";

   function sec
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:102
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_sec";

   function csc
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:103
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_csc";

   function tan
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:104
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_tan";

   function cot
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:105
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_cot";

   function arcsin
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:109
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_arcsin";

   function arcsin_real
     (a : double)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:110
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_arcsin_real";

   function arccos
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:111
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_arccos";

   function arccos_real
     (a : double)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:112
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_arccos_real";

   function arcsec
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:113
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_arcsec";

   function arcsec_real
     (a : double)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:114
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_arcsec_real";

   function arccsc
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:115
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_arccsc";

   function arccsc_real
     (a : double)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:116
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_arccsc_real";

   function arctan
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:117
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_arctan";

   function arccot
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:118
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_arccot";

   function sinh
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:122
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_sinh";

   function cosh
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:123
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_cosh";

   function sech
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:124
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_sech";

   function csch
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:125
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_csch";

   function tanh
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:126
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_tanh";

   function coth
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:127
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_coth";

   function arcsinh
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:131
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_arcsinh";

   function arccosh
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:132
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_arccosh";

   function arccosh_real
     (a : double)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:133
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_arccosh_real";

   function arcsech
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:134
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_arcsech";

   function arccsch
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:135
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_arccsch";

   function arctanh
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:136
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_arctanh";

   function arctanh_real
     (a : double)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:137
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_arctanh_real";

   function arccoth
     (a : gsl_complex)
      return gsl_complex  -- /usr/include/gsl/gsl_complex_math.h:138
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_complex_arccoth";

end gsl.complex.math;
