pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with gsl.cblas;

package gsl.blas_types is

   subtype CBLAS_INDEX_t is size_t;  -- /usr/include/gsl/gsl_blas_types.h:42

   subtype CBLAS_ORDER_t is
     gsl.cblas.CBLAS_ORDER;  -- /usr/include/gsl/gsl_blas_types.h:43

   subtype CBLAS_TRANSPOSE_t is
     gsl.cblas.CBLAS_TRANSPOSE;  -- /usr/include/gsl/gsl_blas_types.h:44

   subtype CBLAS_UPLO_t is
     gsl.cblas.CBLAS_UPLO;  -- /usr/include/gsl/gsl_blas_types.h:45

   subtype CBLAS_DIAG_t is
     gsl.cblas.CBLAS_DIAG;  -- /usr/include/gsl/gsl_blas_types.h:46

   subtype CBLAS_SIDE_t is
     gsl.cblas.CBLAS_SIDE;  -- /usr/include/gsl/gsl_blas_types.h:47

end gsl.blas_types;
