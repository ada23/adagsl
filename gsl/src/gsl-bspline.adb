package body gsl.bspline is
   function ncontrol (w : access gsl_bspline_workspace) return size_t is
   begin
      return ncoeffs (w);
   end ncontrol;

end gsl.bspline;
