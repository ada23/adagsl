pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with gsl.sf.result;

package gsl.sf.gamma is

   GSL_SF_GAMMA_XMAX : constant :=
     171.0;  --  /usr/include/gsl/gsl_sf_gamma.h:283

   GSL_SF_FACT_NMAX : constant := 170;  --  /usr/include/gsl/gsl_sf_gamma.h:286

   GSL_SF_DOUBLEFACT_NMAX : constant :=
     297;  --  /usr/include/gsl/gsl_sf_gamma.h:289

   function lngamma_e
     (x      : double;
      result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_gamma.h:47
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_lngamma_e";

   function lngamma
     (x : double)
      return double  -- /usr/include/gsl/gsl_sf_gamma.h:48
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_lngamma";

   function lngamma_sgn_e
     (x   : double; result_lg : access gsl.sf.result.gsl_sf_result;
      sgn : access double)
      return int  -- /usr/include/gsl/gsl_sf_gamma.h:58
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_lngamma_sgn_e";

   function gamma_e
     (x      : double;
      result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_gamma.h:66
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_gamma_e";

   function gamma
     (x : double)
      return double  -- /usr/include/gsl/gsl_sf_gamma.h:67
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_gamma";

   function gammastar_e
     (x      : double;
      result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_gamma.h:77
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_gammastar_e";

   function gammastar
     (x : double)
      return double  -- /usr/include/gsl/gsl_sf_gamma.h:78
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_gammastar";

   function gammainv_e
     (x      : double;
      result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_gamma.h:86
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_gammainv_e";

   function gammainv
     (x : double)
      return double  -- /usr/include/gsl/gsl_sf_gamma.h:87
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_gammainv";

   function lngamma_complex_e
     (zr  : double; zi : double; lnr : access gsl.sf.result.gsl_sf_result;
      arg : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_gamma.h:103
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_lngamma_complex_e";

   function taylorcoeff_e
     (n : int; x : double; result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_gamma.h:111
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_taylorcoeff_e";

   function taylorcoeff
     (n : int;
      x : double)
      return double  -- /usr/include/gsl/gsl_sf_gamma.h:112
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_taylorcoeff";

   function fact_e
     (n : unsigned; result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_gamma.h:119
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_fact_e";

   function fact
     (n : unsigned)
      return double  -- /usr/include/gsl/gsl_sf_gamma.h:120
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_fact";

   function doublefact_e
     (n : unsigned; result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_gamma.h:127
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_doublefact_e";

   function doublefact
     (n : unsigned)
      return double  -- /usr/include/gsl/gsl_sf_gamma.h:128
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_doublefact";

   function lnfact_e
     (n : unsigned; result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_gamma.h:136
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_lnfact_e";

   function lnfact
     (n : unsigned)
      return double  -- /usr/include/gsl/gsl_sf_gamma.h:137
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_lnfact";

   function lndoublefact_e
     (n : unsigned; result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_gamma.h:144
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_lndoublefact_e";

   function lndoublefact
     (n : unsigned)
      return double  -- /usr/include/gsl/gsl_sf_gamma.h:145
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_lndoublefact";

   function lnchoose_e
     (n : unsigned; m : unsigned; result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_gamma.h:152
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_lnchoose_e";

   function lnchoose
     (n : unsigned;
      m : unsigned)
      return double  -- /usr/include/gsl/gsl_sf_gamma.h:153
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_lnchoose";

   function choose_e
     (n : unsigned; m : unsigned; result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_gamma.h:160
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_choose_e";

   function choose
     (n : unsigned;
      m : unsigned)
      return double  -- /usr/include/gsl/gsl_sf_gamma.h:161
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_choose";

   function lnpoch_e
     (a : double; x : double; result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_gamma.h:172
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_lnpoch_e";

   function lnpoch
     (a : double;
      x : double)
      return double  -- /usr/include/gsl/gsl_sf_gamma.h:173
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_lnpoch";

   function lnpoch_sgn_e
     (a   : double; x : double; result : access gsl.sf.result.gsl_sf_result;
      sgn : access double)
      return int  -- /usr/include/gsl/gsl_sf_gamma.h:185
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_lnpoch_sgn_e";

   function poch_e
     (a : double; x : double; result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_gamma.h:195
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_poch_e";

   function poch
     (a : double;
      x : double)
      return double  -- /usr/include/gsl/gsl_sf_gamma.h:196
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_poch";

   function pochrel_e
     (a : double; x : double; result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_gamma.h:205
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_pochrel_e";

   function pochrel
     (a : double;
      x : double)
      return double  -- /usr/include/gsl/gsl_sf_gamma.h:206
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_pochrel";

   function gamma_inc_Q_e
     (a : double; x : double; result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_gamma.h:219
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_gamma_inc_Q_e";

   function gamma_inc_Q
     (a : double;
      x : double)
      return double  -- /usr/include/gsl/gsl_sf_gamma.h:220
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_gamma_inc_Q";

   function gamma_inc_P_e
     (a : double; x : double; result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_gamma.h:231
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_gamma_inc_P_e";

   function gamma_inc_P
     (a : double;
      x : double)
      return double  -- /usr/include/gsl/gsl_sf_gamma.h:232
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_gamma_inc_P";

   function gamma_inc_e
     (a : double; x : double; result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_gamma.h:244
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_gamma_inc_e";

   function gamma_inc
     (a : double;
      x : double)
      return double  -- /usr/include/gsl/gsl_sf_gamma.h:245
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_gamma_inc";

   function lnbeta_e
     (a : double; b : double; result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_gamma.h:254
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_lnbeta_e";

   function lnbeta
     (a : double;
      b : double)
      return double  -- /usr/include/gsl/gsl_sf_gamma.h:255
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_lnbeta";

   function lnbeta_sgn_e
     (x   : double; y : double; result : access gsl.sf.result.gsl_sf_result;
      sgn : access double)
      return int  -- /usr/include/gsl/gsl_sf_gamma.h:257
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_lnbeta_sgn_e";

   function beta_e
     (a : double; b : double; result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_gamma.h:266
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_beta_e";

   function beta
     (a : double;
      b : double)
      return double  -- /usr/include/gsl/gsl_sf_gamma.h:267
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_beta";

   function beta_inc_e
     (a      : double; b : double; x : double;
      result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_gamma.h:276
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_beta_inc_e";

   function beta_inc
     (a : double;
      b : double;
      x : double)
      return double  -- /usr/include/gsl/gsl_sf_gamma.h:277
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_beta_inc";

end gsl.sf.gamma;
