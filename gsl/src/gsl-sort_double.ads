pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with gsl.vector_int ;
with gsl.vector_double ;

package gsl.sort_double is

   procedure sort
     (data   : in out double_array;
      stride :        size_t;
      n      :        size_t)  -- /usr/include/gsl/gsl_sort_double.h:39
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sort";

   procedure sort (data : in out double_array);

   procedure sort2
     (data1   : in out double_array; stride1 : size_t;
      data2   : in out double_array;
      stride2 :        size_t;
      n       :        size_t)  -- /usr/include/gsl/gsl_sort_double.h:40
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sort2";

   procedure index
     (p : access int; data : access double; stride : size_t;
      n : size_t)  -- /usr/include/gsl/gsl_sort_double.h:41
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sort_index";

   procedure index
     (p : access size_t ; data : in out double_array; stride : size_t;
      n : size_t)  -- /usr/include/gsl/gsl_sort_double.h:41
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sort_index";

   procedure index
     (p : access gsl.vector_int.gsl_vector ; 
     data : access gsl.vector_double.gsl_vector ; stride : size_t;
      n : size_t);  -- /usr/include/gsl/gsl_sort_double.h:41
 

   function smallest
     (dest   : in out double_array; k : size_t; src : double_array;
      stride :        size_t;
      n      :        size_t)
      return int  -- /usr/include/gsl/gsl_sort_double.h:43
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sort_smallest";

   function smallest_index
     (p : access size_t; k : size_t; src : double_array; stride : size_t;
      n : size_t)
      return int  -- /usr/include/gsl/gsl_sort_double.h:44
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sort_smallest_index";

   function largest
     (dest : access double; k : size_t; src : double_array; stride : size_t;
      n    : size_t)
      return int  -- /usr/include/gsl/gsl_sort_double.h:46
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sort_largest";

   function largest_index
     (p : access size_t; k : size_t; src : double_array; stride : size_t;
      n : size_t)
      return int  -- /usr/include/gsl/gsl_sort_double.h:47
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sort_largest_index";

end gsl.sort_double;
