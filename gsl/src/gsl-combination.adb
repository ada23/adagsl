--with Ada.Text_IO;          use Ada.Text_IO;
with Ada.Text_IO.C_Streams;
with Interfaces.C.Strings; use Interfaces.C.Strings;
package body gsl.combination is
   function show (c : access gsl_combination) return int is
      Status : int;
   begin
      Status :=
        gsl.combination.fprintf
          (Ada.Text_IO.C_Streams.C_Stream (Ada.Text_IO.Standard_Output), c,
           New_String (" %u "));
      New_Line;
      return Status;
   end show;

end gsl.combination;
