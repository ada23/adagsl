package body gsl.fft.real is
   function radix2_transform
     (data : access constant gsl.vector_double.gsl_vector) return int
   is
   begin
      return radix2_transform (data.data, data.stride, data.size);
   end radix2_transform;

   function transform
     (data      : access constant gsl.vector_double.gsl_vector;
      wavetable : access constant gsl_fft_real_wavetable;
      work      : access gsl_fft_real_workspace) return int
   is
   begin
      return transform (data.data, data.stride, data.size, wavetable, work);
   end transform;

end gsl.fft.real;
