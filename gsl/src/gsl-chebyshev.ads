pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");
with System;
with gsl.math;
with gsl.mode;

package gsl.chebyshev is

   type gsl_cheb_series_struct is record
      c        : access double;  -- /usr/include/gsl/gsl_chebyshev.h:44
      order    : aliased size_t;  -- /usr/include/gsl/gsl_chebyshev.h:45
      a        : aliased double;  -- /usr/include/gsl/gsl_chebyshev.h:46
      b        : aliased double;  -- /usr/include/gsl/gsl_chebyshev.h:47
      order_sp : aliased size_t;  -- /usr/include/gsl/gsl_chebyshev.h:59
      f        : access double;  -- /usr/include/gsl/gsl_chebyshev.h:63
   end record with
      Convention => C_Pass_By_Copy;  -- /usr/include/gsl/gsl_chebyshev.h:42

   subtype gsl_cheb_series is
     gsl_cheb_series_struct;  -- /usr/include/gsl/gsl_chebyshev.h:65

   function alloc
     (order : size_t)
      return access gsl_cheb_series  -- /usr/include/gsl/gsl_chebyshev.h:72
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_cheb_alloc";

   procedure free
     (cs : access gsl_cheb_series)  -- /usr/include/gsl/gsl_chebyshev.h:76
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_cheb_free";

   function init
     (cs   : access gsl_cheb_series;
      func : access constant gsl.math.gsl_function; a : double; b : double)
      return int  -- /usr/include/gsl/gsl_chebyshev.h:83
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_cheb_init";

   function order
     (cs : access constant gsl_cheb_series)
      return size_t  -- /usr/include/gsl/gsl_chebyshev.h:87
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_cheb_order";

   function size
     (cs : access constant gsl_cheb_series)
      return size_t  -- /usr/include/gsl/gsl_chebyshev.h:88
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_cheb_size";

   function coeffs
     (cs : access constant gsl_cheb_series) return System
     .Address  -- /usr/include/gsl/gsl_chebyshev.h:89
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_cheb_coeffs";

   function eval
     (cs : access constant gsl_cheb_series;
      x  : double)
      return double  -- /usr/include/gsl/gsl_chebyshev.h:94
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_cheb_eval";

   function eval_err
     (cs : access constant gsl_cheb_series; x : double; result : access double;
      abserr : access double)
      return int  -- /usr/include/gsl/gsl_chebyshev.h:95
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_cheb_eval_err";

   function eval_n
     (cs : access constant gsl_cheb_series; order : size_t; x : double)
      return double  -- /usr/include/gsl/gsl_chebyshev.h:102
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_cheb_eval_n";

   function eval_n_err
     (cs     : access constant gsl_cheb_series; order : size_t; x : double;
      result : access double;
      abserr : access double)
      return int  -- /usr/include/gsl/gsl_chebyshev.h:104
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_cheb_eval_n_err";

   function eval_mode
     (cs   : access constant gsl_cheb_series; x : double;
      mode : gsl.mode.gsl_mode_t)
      return double  -- /usr/include/gsl/gsl_chebyshev.h:113
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_cheb_eval_mode";

   function eval_mode_e
     (cs     : access constant gsl_cheb_series; x : double;
      mode   : gsl.mode.gsl_mode_t; result : access double;
      abserr : access double)
      return int  -- /usr/include/gsl/gsl_chebyshev.h:114
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_cheb_eval_mode_e";

   function calc_deriv
     (deriv : access gsl_cheb_series; cs : access constant gsl_cheb_series)
      return int  -- /usr/include/gsl/gsl_chebyshev.h:120
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_cheb_calc_deriv";

   function calc_integ
     (integ : access gsl_cheb_series; cs : access constant gsl_cheb_series)
      return int  -- /usr/include/gsl/gsl_chebyshev.h:127
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_cheb_calc_integ";

end gsl.chebyshev;
