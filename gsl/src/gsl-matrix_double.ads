pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with gsl.block_double;
with gsl.vector_double;

with Interfaces.C.Strings;
with gsl.blas_types;

package gsl.matrix_double is

   --  skipped anonymous struct anon_anon_24

   type gsl_matrix is record
      size1 : aliased size_t;  -- /usr/include/gsl/gsl_matrix_double.h:45
      size2 : aliased size_t;  -- /usr/include/gsl/gsl_matrix_double.h:46
      tda   : aliased size_t;  -- /usr/include/gsl/gsl_matrix_double.h:47
      data  : access double;  -- /usr/include/gsl/gsl_matrix_double.h:48
      block : access gsl.block_double
        .gsl_block;  -- /usr/include/gsl/gsl_matrix_double.h:49
      owner : aliased int;  -- /usr/include/gsl/gsl_matrix_double.h:50
   end record with
      Convention => C_Pass_By_Copy;  -- /usr/include/gsl/gsl_matrix_double.h:51

      --  skipped anonymous struct anon_anon_25

   type u_gsl_matrix_view is record
      matrix : aliased gsl_matrix;  -- /usr/include/gsl/gsl_matrix_double.h:55
   end record with
      Convention => C_Pass_By_Copy;  -- /usr/include/gsl/gsl_matrix_double.h:56

   subtype gsl_matrix_view is
     u_gsl_matrix_view;  -- /usr/include/gsl/gsl_matrix_double.h:58

   --  skipped anonymous struct anon_anon_26

   type u_gsl_matrix_const_view is record
      matrix : aliased gsl_matrix;  -- /usr/include/gsl/gsl_matrix_double.h:62
   end record with
      Convention => C_Pass_By_Copy;  -- /usr/include/gsl/gsl_matrix_double.h:63

   subtype gsl_matrix_const_view is
     u_gsl_matrix_const_view;  -- /usr/include/gsl/gsl_matrix_double.h:65

   function alloc
     (n1 : size_t;
      n2 : size_t)
      return access gsl_matrix  -- /usr/include/gsl/gsl_matrix_double.h:70
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_alloc";

   function calloc
     (n1 : size_t;
      n2 : size_t)
      return access gsl_matrix  -- /usr/include/gsl/gsl_matrix_double.h:73
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_calloc";

   function alloc_from_block
     (b  : access gsl.block_double.gsl_block; offset : size_t; n1 : size_t;
      n2 : size_t;
      d2 : size_t)
      return access gsl_matrix  -- /usr/include/gsl/gsl_matrix_double.h:76
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_alloc_from_block";

   function alloc_from_matrix
     (m  : access gsl_matrix; k1 : size_t; k2 : size_t; n1 : size_t;
      n2 : size_t)
      return access gsl_matrix  -- /usr/include/gsl/gsl_matrix_double.h:83
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_alloc_from_matrix";

   function alloc_row_from_matrix
     (m : access gsl_matrix; i : size_t)
      return access gsl.vector_double
     .gsl_vector  -- /usr/include/gsl/gsl_matrix_double.h:90
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_vector_alloc_row_from_matrix";

   function alloc_col_from_matrix
     (m : access gsl_matrix; j : size_t)
      return access gsl.vector_double
     .gsl_vector  -- /usr/include/gsl/gsl_matrix_double.h:94
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_vector_alloc_col_from_matrix";

   procedure free
     (m : access gsl_matrix)  -- /usr/include/gsl/gsl_matrix_double.h:97
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_free";

   function submatrix
     (m : access gsl_matrix; i : size_t; j : size_t; n1 : size_t; n2 : size_t)
      return u_gsl_matrix_view  -- /usr/include/gsl/gsl_matrix_double.h:102
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_submatrix";

   function row
     (m : access gsl_matrix; i : size_t)
      return gsl.vector_double
     .u_gsl_vector_view  -- /usr/include/gsl/gsl_matrix_double.h:107
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_row";

   function column
     (m : access gsl_matrix; j : size_t)
      return gsl.vector_double
     .u_gsl_vector_view  -- /usr/include/gsl/gsl_matrix_double.h:110
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_column";

   function diagonal
     (m : access gsl_matrix) return gsl.vector_double
     .u_gsl_vector_view  -- /usr/include/gsl/gsl_matrix_double.h:113
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_diagonal";

   function subdiagonal
     (m : access gsl_matrix; k : size_t)
      return gsl.vector_double
     .u_gsl_vector_view  -- /usr/include/gsl/gsl_matrix_double.h:116
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_subdiagonal";

   function superdiagonal
     (m : access gsl_matrix; k : size_t)
      return gsl.vector_double
     .u_gsl_vector_view  -- /usr/include/gsl/gsl_matrix_double.h:119
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_superdiagonal";

   function subrow
     (m : access gsl_matrix; i : size_t; offset : size_t; n : size_t)
      return gsl.vector_double
     .u_gsl_vector_view  -- /usr/include/gsl/gsl_matrix_double.h:122
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_subrow";

   function subcolumn
     (m : access gsl_matrix; j : size_t; offset : size_t; n : size_t)
      return gsl.vector_double
     .u_gsl_vector_view  -- /usr/include/gsl/gsl_matrix_double.h:126
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_subcolumn";

   function view_array
     (base : access double; n1 : size_t; n2 : size_t)
      return u_gsl_matrix_view  -- /usr/include/gsl/gsl_matrix_double.h:130
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_view_array";

   function view_array_with_tda
     (base : access double; n1 : size_t; n2 : size_t; tda : size_t)
      return u_gsl_matrix_view  -- /usr/include/gsl/gsl_matrix_double.h:135
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_view_array_with_tda";

   function view_vector
     (v : access gsl.vector_double.gsl_vector; n1 : size_t; n2 : size_t)
      return u_gsl_matrix_view  -- /usr/include/gsl/gsl_matrix_double.h:142
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_view_vector";

   function view_vector_with_tda
     (v   : access gsl.vector_double.gsl_vector; n1 : size_t; n2 : size_t;
      tda : size_t)
      return u_gsl_matrix_view  -- /usr/include/gsl/gsl_matrix_double.h:147
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_view_vector_with_tda";

   function const_submatrix
     (m  : access constant gsl_matrix; i : size_t; j : size_t; n1 : size_t;
      n2 : size_t)
      return u_gsl_matrix_const_view  -- /usr/include/gsl/gsl_matrix_double.h:154
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_const_submatrix";

   function const_row
     (m : access constant gsl_matrix; i : size_t)
      return gsl.vector_double
     .u_gsl_vector_const_view  -- /usr/include/gsl/gsl_matrix_double.h:159
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_const_row";

   function const_column
     (m : access constant gsl_matrix; j : size_t)
      return gsl.vector_double
     .u_gsl_vector_const_view  -- /usr/include/gsl/gsl_matrix_double.h:163
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_const_column";

   function const_diagonal
     (m : access constant gsl_matrix)
      return gsl.vector_double
     .u_gsl_vector_const_view  -- /usr/include/gsl/gsl_matrix_double.h:167
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_const_diagonal";

   function const_subdiagonal
     (m : access constant gsl_matrix; k : size_t)
      return gsl.vector_double
     .u_gsl_vector_const_view  -- /usr/include/gsl/gsl_matrix_double.h:170
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_const_subdiagonal";

   function const_superdiagonal
     (m : access constant gsl_matrix; k : size_t)
      return gsl.vector_double
     .u_gsl_vector_const_view  -- /usr/include/gsl/gsl_matrix_double.h:174
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_const_superdiagonal";

   function const_subrow
     (m : access constant gsl_matrix; i : size_t; offset : size_t; n : size_t)
      return gsl.vector_double
     .u_gsl_vector_const_view  -- /usr/include/gsl/gsl_matrix_double.h:178
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_const_subrow";

   function const_subcolumn
     (m : access constant gsl_matrix; j : size_t; offset : size_t; n : size_t)
      return gsl.vector_double
     .u_gsl_vector_const_view  -- /usr/include/gsl/gsl_matrix_double.h:182
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_const_subcolumn";

   function const_view_array
     (base : access double; n1 : size_t; n2 : size_t)
      return u_gsl_matrix_const_view  -- /usr/include/gsl/gsl_matrix_double.h:186
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_const_view_array";

   function const_view_array_with_tda
     (base : access double; n1 : size_t; n2 : size_t; tda : size_t)
      return u_gsl_matrix_const_view  -- /usr/include/gsl/gsl_matrix_double.h:191
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_const_view_array_with_tda";

   function const_view_vector
     (v  : access constant gsl.vector_double.gsl_vector; n1 : size_t;
      n2 : size_t)
      return u_gsl_matrix_const_view  -- /usr/include/gsl/gsl_matrix_double.h:197
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_const_view_vector";

   function const_view_vector_with_tda
     (v   : access constant gsl.vector_double.gsl_vector; n1 : size_t;
      n2  : size_t;
      tda : size_t)
      return u_gsl_matrix_const_view  -- /usr/include/gsl/gsl_matrix_double.h:202
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_const_view_vector_with_tda";

   procedure set_zero
     (m : access gsl_matrix)  -- /usr/include/gsl/gsl_matrix_double.h:209
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_set_zero";

   procedure set_identity
     (m : access gsl_matrix)  -- /usr/include/gsl/gsl_matrix_double.h:210
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_set_identity";

   procedure set_all
     (m : access gsl_matrix;
      x : double)  -- /usr/include/gsl/gsl_matrix_double.h:211
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_set_all";

   function fread
     (stream : ICS.FILEs;
      m      : access gsl_matrix)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:213
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_fread";

   function fwrite
     (stream : ICS.FILEs;
      m      : access constant gsl_matrix)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:214
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_fwrite";

   function fscanf
     (stream : ICS.FILEs;
      m      : access gsl_matrix)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:215
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_fscanf";

   function fprintf
     (stream : ICS.FILEs; m : access constant gsl_matrix;
      format : Interfaces.C.Strings.chars_ptr)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:216
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_fprintf";

   function fprintf
     (m : access constant gsl_matrix; filename : String; format : String)
      return int;  -- /usr/include/gsl/gsl_matrix_double.h:216

   function memcpy
     (dest : access gsl_matrix; src : access constant gsl_matrix)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:218
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_memcpy";

   function swap
     (m1 : access gsl_matrix;
      m2 : access gsl_matrix)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:219
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_swap";

   function tricpy
     (Uplo : gsl.blas_types.CBLAS_UPLO_t; Diag : gsl.blas_types.CBLAS_DIAG_t;
      dest : access gsl_matrix; src : access constant gsl_matrix)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:220
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_tricpy";

   function swap_rows
     (m : access gsl_matrix;
      i : size_t;
      j : size_t)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:222
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_swap_rows";

   function swap_columns
     (m : access gsl_matrix;
      i : size_t;
      j : size_t)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:223
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_swap_columns";

   function swap_rowcol
     (m : access gsl_matrix;
      i : size_t;
      j : size_t)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:224
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_swap_rowcol";

   function transpose
     (m : access gsl_matrix)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:225
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_transpose";

   function transpose_memcpy
     (dest : access gsl_matrix; src : access constant gsl_matrix)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:226
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_transpose_memcpy";

   function transpose_tricpy
     (Uplo_src : gsl.blas_types.CBLAS_UPLO_t;
      Diag     : gsl.blas_types.CBLAS_DIAG_t; dest : access gsl_matrix;
      src      : access constant gsl_matrix)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:227
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_transpose_tricpy";

   function max
     (m : access constant gsl_matrix)
      return double  -- /usr/include/gsl/gsl_matrix_double.h:229
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_max";

   function min
     (m : access constant gsl_matrix)
      return double  -- /usr/include/gsl/gsl_matrix_double.h:230
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_min";

   procedure minmax
     (m       : access constant gsl_matrix; min_out : access double;
      max_out : access double)  -- /usr/include/gsl/gsl_matrix_double.h:231
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_minmax";

   procedure max_index
     (m    : access constant gsl_matrix; imax : access size_t;
      jmax : access size_t)  -- /usr/include/gsl/gsl_matrix_double.h:233
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_max_index";

   procedure min_index
     (m    : access constant gsl_matrix; imin : access size_t;
      jmin : access size_t)  -- /usr/include/gsl/gsl_matrix_double.h:234
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_min_index";

   procedure minmax_index
     (m    : access constant gsl_matrix; imin : access size_t;
      jmin : access size_t;
      imax : access size_t;
      jmax : access size_t)  -- /usr/include/gsl/gsl_matrix_double.h:235
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_minmax_index";

   function equal
     (a : access constant gsl_matrix; b : access constant gsl_matrix)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:237
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_equal";

   function isnull
     (m : access constant gsl_matrix)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:239
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_isnull";

   function ispos
     (m : access constant gsl_matrix)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:240
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_ispos";

   function isneg
     (m : access constant gsl_matrix)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:241
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_isneg";

   function isnonneg
     (m : access constant gsl_matrix)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:242
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_isnonneg";

   function norm1
     (m : access constant gsl_matrix)
      return double  -- /usr/include/gsl/gsl_matrix_double.h:244
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_norm1";

   function add
     (a : access gsl_matrix;
      b : access constant gsl_matrix)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:246
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_add";

   function sub
     (a : access gsl_matrix;
      b : access constant gsl_matrix)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:247
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_sub";

   function mul_elements
     (a : access gsl_matrix;
      b : access constant gsl_matrix)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:248
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_mul_elements";

   function div_elements
     (a : access gsl_matrix;
      b : access constant gsl_matrix)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:249
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_div_elements";

   function scale
     (a : access gsl_matrix;
      x : double)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:250
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_scale";

   function scale_rows
     (a : access gsl_matrix; x : access constant gsl.vector_double.gsl_vector)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:251
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_scale_rows";

   function scale_columns
     (a : access gsl_matrix; x : access constant gsl.vector_double.gsl_vector)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:252
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_scale_columns";

   function add_constant
     (a : access gsl_matrix;
      x : double)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:253
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_add_constant";

   function add_diagonal
     (a : access gsl_matrix;
      x : double)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:254
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_add_diagonal";

   function get_row
     (v : access gsl.vector_double.gsl_vector; m : access constant gsl_matrix;
      i : size_t)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:259
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_get_row";

   function get_col
     (v : access gsl.vector_double.gsl_vector; m : access constant gsl_matrix;
      j : size_t)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:260
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_get_col";

   function set_row
     (m : access gsl_matrix; i : size_t;
      v : access constant gsl.vector_double.gsl_vector)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:261
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_set_row";

   function set_col
     (m : access gsl_matrix; j : size_t;
      v : access constant gsl.vector_double.gsl_vector)
      return int  -- /usr/include/gsl/gsl_matrix_double.h:262
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_set_col";

   function get
     (m : access constant gsl_matrix; i : size_t; j : size_t)
      return double  -- /usr/include/gsl/gsl_matrix_double.h:267
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_get";

   procedure set
     (m : access gsl_matrix;
      i : size_t;
      j : size_t;
      x : double)  -- /usr/include/gsl/gsl_matrix_double.h:268
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_set";

   function ptr
     (m : access gsl_matrix;
      i : size_t;
      j : size_t)
      return access double  -- /usr/include/gsl/gsl_matrix_double.h:269
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_ptr";

   function const_ptr
     (m : access constant gsl_matrix; i : size_t; j : size_t)
      return access double  -- /usr/include/gsl/gsl_matrix_double.h:270
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_matrix_const_ptr";

   function To_Ada
     (c : access constant gsl_matrix)
      return Ada.Numerics.Long_Real_Arrays.Real_Matrix;
   function To_C
     (a : Ada.Numerics.Long_Real_Arrays.Real_Matrix) return access gsl_matrix;

end gsl.matrix_double;
