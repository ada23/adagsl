package body gsl.wavelet is

    use gsl.vector_double ;
   function transform
     (w : access constant gsl_wavelet;
      data : access gsl.vector_double.gsl_vector ;
      stride : size_t;
      n : size_t;
      dir : gsl_wavelet_direction;
      work : access gsl_wavelet_workspace) return int is
    begin
        return transform(w,ptr(data,0),stride,n,dir,work);
    end transform ;


   function transform_forward
     (w : access constant gsl_wavelet;
      data : access gsl.vector_double.gsl_vector ;
      stride : size_t;
      n : size_t;
      work : access gsl_wavelet_workspace) return int is
    begin
      return transform_forward(w,ptr(data,0),stride,n,work);
    end transform_forward;

  function transform_inverse
     (w : access constant gsl_wavelet;
      data : access gsl.vector_double.gsl_vector ;
      stride : size_t;
      n : size_t;
      work : access gsl_wavelet_workspace) return int is
  begin
     return transform_inverse(w,ptr(data,0) , stride , n , work );
  end transform_inverse;

end gsl.wavelet ;
