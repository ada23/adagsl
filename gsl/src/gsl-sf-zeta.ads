pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with gsl.sf.result;

package gsl.sf.zeta is

   function zeta_int_e
     (n      : int;
      result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_zeta.h:46
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_zeta_int_e";

   function zeta_int
     (n : int)
      return double  -- /usr/include/gsl/gsl_sf_zeta.h:47
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_zeta_int";

   function zeta_e
     (s      : double;
      result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_zeta.h:56
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_zeta_e";

   function zeta
     (s : double)
      return double  -- /usr/include/gsl/gsl_sf_zeta.h:57
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_zeta";

   function zetam1_e
     (s      : double;
      result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_zeta.h:67
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_zetam1_e";

   function zetam1
     (s : double)
      return double  -- /usr/include/gsl/gsl_sf_zeta.h:68
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_zetam1";

   function zetam1_int_e
     (s      : int;
      result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_zeta.h:78
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_zetam1_int_e";

   function zetam1_int
     (s : int)
      return double  -- /usr/include/gsl/gsl_sf_zeta.h:79
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_zetam1_int";

   function hzeta_e
     (s : double; q : double; result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_zeta.h:88
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_hzeta_e";

   function hzeta
     (s : double;
      q : double)
      return double  -- /usr/include/gsl/gsl_sf_zeta.h:89
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_hzeta";

   function eta_int_e
     (n      : int;
      result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_zeta.h:97
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_eta_int_e";

   function eta_int
     (n : int)
      return double  -- /usr/include/gsl/gsl_sf_zeta.h:98
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_eta_int";

   function eta_e
     (s      : double;
      result : access gsl.sf.result.gsl_sf_result)
      return int  -- /usr/include/gsl/gsl_sf_zeta.h:106
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_eta_e";

   function eta
     (s : double)
      return double  -- /usr/include/gsl/gsl_sf_zeta.h:107
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_sf_eta";

end gsl.sf.zeta;
