pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C.Strings;
with System;
package gsl.permutation is

   type gsl_permutation_struct is record
      size : aliased size_t;  -- /usr/include/gsl/gsl_permutation.h:43
      data : System.Address;  -- /usr/include/gsl/gsl_permutation.h:44
   end record with
      Convention => C_Pass_By_Copy;  -- /usr/include/gsl/gsl_permutation.h:41

   subtype gsl_permutation is
     gsl_permutation_struct;  -- /usr/include/gsl/gsl_permutation.h:47

   function alloc
     (n : size_t)
      return access gsl_permutation  -- /usr/include/gsl/gsl_permutation.h:49
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_permutation_alloc";

   function calloc
     (n : size_t)
      return access gsl_permutation  -- /usr/include/gsl/gsl_permutation.h:50
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_permutation_calloc";

   procedure init
     (p : access gsl_permutation)  -- /usr/include/gsl/gsl_permutation.h:51
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_permutation_init";

   procedure free
     (p : access gsl_permutation)  -- /usr/include/gsl/gsl_permutation.h:52
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_permutation_free";

   function memcpy
     (dest : access gsl_permutation; src : access constant gsl_permutation)
      return int  -- /usr/include/gsl/gsl_permutation.h:53
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_permutation_memcpy";

   function fread
     (stream : ICS.FILEs;
      p      : access gsl_permutation)
      return int  -- /usr/include/gsl/gsl_permutation.h:55
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_permutation_fread";

   function fwrite
     (stream : ICS.FILEs;
      p      : access constant gsl_permutation)
      return int  -- /usr/include/gsl/gsl_permutation.h:56
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_permutation_fwrite";

   function fscanf
     (stream : ICS.FILEs;
      p      : access gsl_permutation)
      return int  -- /usr/include/gsl/gsl_permutation.h:57
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_permutation_fscanf";

   function fprintf
     (stream : ICS.FILEs; p : access constant gsl_permutation;
      format : Interfaces.C.Strings.chars_ptr)
      return int  -- /usr/include/gsl/gsl_permutation.h:58
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_permutation_fprintf";

   function show (p : access gsl_permutation) return int;

   function size
     (p : access constant gsl_permutation)
      return size_t  -- /usr/include/gsl/gsl_permutation.h:60
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_permutation_size";

   function data
     (p : access constant gsl_permutation) return System
     .Address   -- /usr/include/gsl/gsl_permutation.h:61
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_permutation_data";

   function swap
     (p : access gsl_permutation;
      i : size_t;
      j : size_t)
      return int  -- /usr/include/gsl/gsl_permutation.h:63
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_permutation_swap";

   function valid
     (p : access constant gsl_permutation)
      return int  -- /usr/include/gsl/gsl_permutation.h:65
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_permutation_valid";

   procedure rreverse
     (p : access gsl_permutation)  -- /usr/include/gsl/gsl_permutation.h:66
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_permutation_reverse";

   function inverse
     (inv : access gsl_permutation; p : access constant gsl_permutation)
      return int  -- /usr/include/gsl/gsl_permutation.h:67
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_permutation_inverse";

   function next
     (p : access gsl_permutation)
      return int  -- /usr/include/gsl/gsl_permutation.h:68
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_permutation_next";

   function prev
     (p : access gsl_permutation)
      return int  -- /usr/include/gsl/gsl_permutation.h:69
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_permutation_prev";

   function mul
     (p  : access gsl_permutation; pa : access constant gsl_permutation;
      pb : access constant gsl_permutation)
      return int  -- /usr/include/gsl/gsl_permutation.h:70
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_permutation_mul";

   function linear_to_canonical
     (q : access gsl_permutation; p : access constant gsl_permutation)
      return int  -- /usr/include/gsl/gsl_permutation.h:72
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_permutation_linear_to_canonical";

   function canonical_to_linear
     (p : access gsl_permutation; q : access constant gsl_permutation)
      return int  -- /usr/include/gsl/gsl_permutation.h:73
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_permutation_canonical_to_linear";

   function inversions
     (p : access constant gsl_permutation)
      return size_t  -- /usr/include/gsl/gsl_permutation.h:75
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_permutation_inversions";

   function linear_cycles
     (p : access constant gsl_permutation)
      return size_t  -- /usr/include/gsl/gsl_permutation.h:76
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_permutation_linear_cycles";

   function canonical_cycles
     (q : access constant gsl_permutation)
      return size_t  -- /usr/include/gsl/gsl_permutation.h:77
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_permutation_canonical_cycles";

   function get
     (p : access constant gsl_permutation;
      i : size_t)
      return size_t  -- /usr/include/gsl/gsl_permutation.h:79
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_permutation_get";

end gsl.permutation;
