pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with gsl.math;

package gsl.deriv is

   function central
     (f      : access constant gsl.math.gsl_function; x : double; h : double;
      result : access double;
      abserr : access double)
      return int  -- /usr/include/gsl/gsl_deriv.h:36
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_deriv_central";

   function backward
     (f      : access constant gsl.math.gsl_function; x : double; h : double;
      result : access double;
      abserr : access double)
      return int  -- /usr/include/gsl/gsl_deriv.h:40
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_deriv_backward";

   function forward
     (f      : access constant gsl.math.gsl_function; x : double; h : double;
      result : access double;
      abserr : access double)
      return int  -- /usr/include/gsl/gsl_deriv.h:44
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_deriv_forward";

end gsl.deriv;
