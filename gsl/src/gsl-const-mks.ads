pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

package gsl.const.mks is

   GSL_CONST_MKS_SPEED_OF_LIGHT : constant double :=
     (2.997_924_58e8);  --  /usr/include/gsl/gsl_const_mks.h:24
   GSL_CONST_MKS_GRAVITATIONAL_CONSTANT : constant double :=
     (6.673e-11);  --  /usr/include/gsl/gsl_const_mks.h:25
   GSL_CONST_MKS_PLANCKS_CONSTANT_H : constant double :=
     (6.626_068_96e-34);  --  /usr/include/gsl/gsl_const_mks.h:26
   GSL_CONST_MKS_PLANCKS_CONSTANT_HBAR : constant double :=
     (1.054_571_628_25e-34);  --  /usr/include/gsl/gsl_const_mks.h:27
   GSL_CONST_MKS_ASTRONOMICAL_UNIT : constant double :=
     (1.495_978_706_91e11);  --  /usr/include/gsl/gsl_const_mks.h:28
   GSL_CONST_MKS_LIGHT_YEAR : constant double :=
     (9.460_536_207_07e15);  --  /usr/include/gsl/gsl_const_mks.h:29
   GSL_CONST_MKS_PARSEC : constant double :=
     (3.085_677_581_35e16);  --  /usr/include/gsl/gsl_const_mks.h:30
   GSL_CONST_MKS_GRAV_ACCEL : constant double :=
     (9.806_65e0);  --  /usr/include/gsl/gsl_const_mks.h:31
   GSL_CONST_MKS_ELECTRON_VOLT : constant double :=
     (1.602_176_487e-19);  --  /usr/include/gsl/gsl_const_mks.h:32
   GSL_CONST_MKS_MASS_ELECTRON : constant double :=
     (9.109_381_88e-31);  --  /usr/include/gsl/gsl_const_mks.h:33
   GSL_CONST_MKS_MASS_MUON : constant double :=
     (1.883_531_09e-28);  --  /usr/include/gsl/gsl_const_mks.h:34
   GSL_CONST_MKS_MASS_PROTON : constant double :=
     (1.672_621_58e-27);  --  /usr/include/gsl/gsl_const_mks.h:35
   GSL_CONST_MKS_MASS_NEUTRON : constant double :=
     (1.674_927_16e-27);  --  /usr/include/gsl/gsl_const_mks.h:36
   GSL_CONST_MKS_RYDBERG : constant double :=
     (2.179_871_969_68e-18);  --  /usr/include/gsl/gsl_const_mks.h:37
   GSL_CONST_MKS_BOLTZMANN : constant double :=
     (1.380_650_4e-23);  --  /usr/include/gsl/gsl_const_mks.h:38
   GSL_CONST_MKS_MOLAR_GAS : constant double :=
     (8.314_472e0);  --  /usr/include/gsl/gsl_const_mks.h:39
   GSL_CONST_MKS_STANDARD_GAS_VOLUME : constant double :=
     (2.271_098_1e-2);  --  /usr/include/gsl/gsl_const_mks.h:40
   GSL_CONST_MKS_MINUTE : constant double :=
     (6.0e1);  --  /usr/include/gsl/gsl_const_mks.h:41
   GSL_CONST_MKS_HOUR : constant double :=
     (3.6e3);  --  /usr/include/gsl/gsl_const_mks.h:42
   GSL_CONST_MKS_DAY : constant double :=
     (8.64e4);  --  /usr/include/gsl/gsl_const_mks.h:43
   GSL_CONST_MKS_WEEK : constant double :=
     (6.048e5);  --  /usr/include/gsl/gsl_const_mks.h:44
   GSL_CONST_MKS_INCH : constant double :=
     (2.54e-2);  --  /usr/include/gsl/gsl_const_mks.h:45
   GSL_CONST_MKS_FOOT : constant double :=
     (3.048e-1);  --  /usr/include/gsl/gsl_const_mks.h:46
   GSL_CONST_MKS_YARD : constant double :=
     (9.144e-1);  --  /usr/include/gsl/gsl_const_mks.h:47
   GSL_CONST_MKS_MILE : constant double :=
     (1.609_344e3);  --  /usr/include/gsl/gsl_const_mks.h:48
   GSL_CONST_MKS_NAUTICAL_MILE : constant double :=
     (1.852e3);  --  /usr/include/gsl/gsl_const_mks.h:49
   GSL_CONST_MKS_FATHOM : constant double :=
     (1.828_8e0);  --  /usr/include/gsl/gsl_const_mks.h:50
   GSL_CONST_MKS_MIL : constant double :=
     (2.54e-5);  --  /usr/include/gsl/gsl_const_mks.h:51
   GSL_CONST_MKS_POINT : constant double :=
     (3.527_777_777_78e-4);  --  /usr/include/gsl/gsl_const_mks.h:52
   GSL_CONST_MKS_TEXPOINT : constant double :=
     (3.514_598_035_15e-4);  --  /usr/include/gsl/gsl_const_mks.h:53
   GSL_CONST_MKS_MICRON : constant double :=
     (1.0e-6);  --  /usr/include/gsl/gsl_const_mks.h:54
   GSL_CONST_MKS_ANGSTROM : constant double :=
     (1.0e-10);  --  /usr/include/gsl/gsl_const_mks.h:55
   GSL_CONST_MKS_HECTARE : constant double :=
     (1.0e4);  --  /usr/include/gsl/gsl_const_mks.h:56
   GSL_CONST_MKS_ACRE : constant double :=
     (4.046_856_422_41e3);  --  /usr/include/gsl/gsl_const_mks.h:57
   GSL_CONST_MKS_BARN : constant double :=
     (1.0e-28);  --  /usr/include/gsl/gsl_const_mks.h:58
   GSL_CONST_MKS_LITER : constant double :=
     (1.0e-3);  --  /usr/include/gsl/gsl_const_mks.h:59
   GSL_CONST_MKS_US_GALLON : constant double :=
     (3.785_411_784_02e-3);  --  /usr/include/gsl/gsl_const_mks.h:60
   GSL_CONST_MKS_QUART : constant double :=
     (9.463_529_460_04e-4);  --  /usr/include/gsl/gsl_const_mks.h:61
   GSL_CONST_MKS_PINT : constant double :=
     (4.731_764_730_02e-4);  --  /usr/include/gsl/gsl_const_mks.h:62
   GSL_CONST_MKS_CUP : constant double :=
     (2.365_882_365_01e-4);  --  /usr/include/gsl/gsl_const_mks.h:63
   GSL_CONST_MKS_FLUID_OUNCE : constant double :=
     (2.957_352_956_26e-5);  --  /usr/include/gsl/gsl_const_mks.h:64
   GSL_CONST_MKS_TABLESPOON : constant double :=
     (1.478_676_478_13e-5);  --  /usr/include/gsl/gsl_const_mks.h:65
   GSL_CONST_MKS_TEASPOON : constant double :=
     (4.928_921_593_75e-6);  --  /usr/include/gsl/gsl_const_mks.h:66
   GSL_CONST_MKS_CANADIAN_GALLON : constant double :=
     (4.546_09e-3);  --  /usr/include/gsl/gsl_const_mks.h:67
   GSL_CONST_MKS_UK_GALLON : constant double :=
     (4.546_092e-3);  --  /usr/include/gsl/gsl_const_mks.h:68
   GSL_CONST_MKS_MILES_PER_HOUR : constant double :=
     (4.470_4e-1);  --  /usr/include/gsl/gsl_const_mks.h:69
   GSL_CONST_MKS_KILOMETERS_PER_HOUR : constant double :=
     (2.777_777_777_78e-1);  --  /usr/include/gsl/gsl_const_mks.h:70
   GSL_CONST_MKS_KNOT : constant double :=
     (5.144_444_444_44e-1);  --  /usr/include/gsl/gsl_const_mks.h:71
   GSL_CONST_MKS_POUND_MASS : constant double :=
     (4.535_923_7e-1);  --  /usr/include/gsl/gsl_const_mks.h:72
   GSL_CONST_MKS_OUNCE_MASS : constant double :=
     (2.834_952_312_5e-2);  --  /usr/include/gsl/gsl_const_mks.h:73
   GSL_CONST_MKS_TON : constant double :=
     (9.071_847_4e2);  --  /usr/include/gsl/gsl_const_mks.h:74
   GSL_CONST_MKS_METRIC_TON : constant double :=
     (1.0e3);  --  /usr/include/gsl/gsl_const_mks.h:75
   GSL_CONST_MKS_UK_TON : constant double :=
     (1.016_046_908_8e3);  --  /usr/include/gsl/gsl_const_mks.h:76
   GSL_CONST_MKS_TROY_OUNCE : constant double :=
     (3.110_347_5e-2);  --  /usr/include/gsl/gsl_const_mks.h:77
   GSL_CONST_MKS_CARAT : constant double :=
     (2.0e-4);  --  /usr/include/gsl/gsl_const_mks.h:78
   GSL_CONST_MKS_UNIFIED_ATOMIC_MASS : constant double :=
     (1.660_538_782e-27);  --  /usr/include/gsl/gsl_const_mks.h:79
   GSL_CONST_MKS_GRAM_FORCE : constant double :=
     (9.806_65e-3);  --  /usr/include/gsl/gsl_const_mks.h:80
   GSL_CONST_MKS_POUND_FORCE : constant double :=
     (4.448_221_615_26e0);  --  /usr/include/gsl/gsl_const_mks.h:81
   GSL_CONST_MKS_KILOPOUND_FORCE : constant double :=
     (4.448_221_615_26e3);  --  /usr/include/gsl/gsl_const_mks.h:82
   GSL_CONST_MKS_POUNDAL : constant double :=
     (1.382_55e-1);  --  /usr/include/gsl/gsl_const_mks.h:83
   GSL_CONST_MKS_CALORIE : constant double :=
     (4.186_8e0);  --  /usr/include/gsl/gsl_const_mks.h:84
   GSL_CONST_MKS_BTU : constant double :=
     (1.055_055_852_62e3);  --  /usr/include/gsl/gsl_const_mks.h:85
   GSL_CONST_MKS_THERM : constant double :=
     (1.055_06e8);  --  /usr/include/gsl/gsl_const_mks.h:86
   GSL_CONST_MKS_HORSEPOWER : constant double :=
     (7.457e2);  --  /usr/include/gsl/gsl_const_mks.h:87
   GSL_CONST_MKS_BAR : constant double :=
     (1.0e5);  --  /usr/include/gsl/gsl_const_mks.h:88
   GSL_CONST_MKS_STD_ATMOSPHERE : constant double :=
     (1.013_25e5);  --  /usr/include/gsl/gsl_const_mks.h:89
   GSL_CONST_MKS_TORR : constant double :=
     (1.333_223_684_21e2);  --  /usr/include/gsl/gsl_const_mks.h:90
   GSL_CONST_MKS_METER_OF_MERCURY : constant double :=
     (1.333_223_684_21e5);  --  /usr/include/gsl/gsl_const_mks.h:91
   GSL_CONST_MKS_INCH_OF_MERCURY : constant double :=
     (3.386_388_157_89e3);  --  /usr/include/gsl/gsl_const_mks.h:92
   GSL_CONST_MKS_INCH_OF_WATER : constant double :=
     (2.490_889e2);  --  /usr/include/gsl/gsl_const_mks.h:93
   GSL_CONST_MKS_PSI : constant double :=
     (6.894_757_293_17e3);  --  /usr/include/gsl/gsl_const_mks.h:94
   GSL_CONST_MKS_POISE : constant double :=
     (1.0e-1);  --  /usr/include/gsl/gsl_const_mks.h:95
   GSL_CONST_MKS_STOKES : constant double :=
     (1.0e-4);  --  /usr/include/gsl/gsl_const_mks.h:96
   GSL_CONST_MKS_STILB : constant double :=
     (1.0e4);  --  /usr/include/gsl/gsl_const_mks.h:97
   GSL_CONST_MKS_LUMEN : constant double :=
     (1.0e0);  --  /usr/include/gsl/gsl_const_mks.h:98
   GSL_CONST_MKS_LUX : constant double :=
     (1.0e0);  --  /usr/include/gsl/gsl_const_mks.h:99
   GSL_CONST_MKS_PHOT : constant double :=
     (1.0e4);  --  /usr/include/gsl/gsl_const_mks.h:100
   GSL_CONST_MKS_FOOTCANDLE : constant double :=
     (1.076e1);  --  /usr/include/gsl/gsl_const_mks.h:101
   GSL_CONST_MKS_LAMBERT : constant double :=
     (1.0e4);  --  /usr/include/gsl/gsl_const_mks.h:102
   GSL_CONST_MKS_FOOTLAMBERT : constant double :=
     (1.076_391_04e1);  --  /usr/include/gsl/gsl_const_mks.h:103
   GSL_CONST_MKS_CURIE : constant double :=
     (3.7e10);  --  /usr/include/gsl/gsl_const_mks.h:104
   GSL_CONST_MKS_ROENTGEN : constant double :=
     (2.58e-4);  --  /usr/include/gsl/gsl_const_mks.h:105
   GSL_CONST_MKS_RAD : constant double :=
     (1.0e-2);  --  /usr/include/gsl/gsl_const_mks.h:106
   GSL_CONST_MKS_SOLAR_MASS : constant double :=
     (1.988_92e30);  --  /usr/include/gsl/gsl_const_mks.h:107
   GSL_CONST_MKS_BOHR_RADIUS : constant double :=
     (5.291_772_083e-11);  --  /usr/include/gsl/gsl_const_mks.h:108
   GSL_CONST_MKS_NEWTON : constant double :=
     (1.0e0);  --  /usr/include/gsl/gsl_const_mks.h:109
   GSL_CONST_MKS_DYNE : constant double :=
     (1.0e-5);  --  /usr/include/gsl/gsl_const_mks.h:110
   GSL_CONST_MKS_JOULE : constant double :=
     (1.0e0);  --  /usr/include/gsl/gsl_const_mks.h:111
   GSL_CONST_MKS_ERG : constant double :=
     (1.0e-7);  --  /usr/include/gsl/gsl_const_mks.h:112
   GSL_CONST_MKS_STEFAN_BOLTZMANN_CONSTANT : constant double :=
     (5.670_400_473_74e-8);  --  /usr/include/gsl/gsl_const_mks.h:113
   GSL_CONST_MKS_THOMSON_CROSS_SECTION : constant double :=
     (6.652_458_936_99e-29);  --  /usr/include/gsl/gsl_const_mks.h:114
   GSL_CONST_MKS_BOHR_MAGNETON : constant double :=
     (9.274_008_99e-24);  --  /usr/include/gsl/gsl_const_mks.h:115
   GSL_CONST_MKS_NUCLEAR_MAGNETON : constant double :=
     (5.050_783_17e-27);  --  /usr/include/gsl/gsl_const_mks.h:116
   GSL_CONST_MKS_ELECTRON_MAGNETIC_MOMENT : constant double :=
     (9.284_763_62e-24);  --  /usr/include/gsl/gsl_const_mks.h:117
   GSL_CONST_MKS_PROTON_MAGNETIC_MOMENT : constant double :=
     (1.410_606_633e-26);  --  /usr/include/gsl/gsl_const_mks.h:118
   GSL_CONST_MKS_FARADAY : constant double :=
     (9.648_534_297_75e4);  --  /usr/include/gsl/gsl_const_mks.h:119
   GSL_CONST_MKS_ELECTRON_CHARGE : constant double :=
     (1.602_176_487e-19);  --  /usr/include/gsl/gsl_const_mks.h:120
   GSL_CONST_MKS_VACUUM_PERMITTIVITY : constant double :=
     (8.854_187_817e-12);  --  /usr/include/gsl/gsl_const_mks.h:121
   GSL_CONST_MKS_VACUUM_PERMEABILITY : constant double :=
     (1.256_637_061_44e-6);  --  /usr/include/gsl/gsl_const_mks.h:122
   GSL_CONST_MKS_DEBYE : constant double :=
     (3.335_640_951_98e-30);  --  /usr/include/gsl/gsl_const_mks.h:123
   GSL_CONST_MKS_GAUSS : constant double :=
     (1.0e-4);  --  /usr/include/gsl/gsl_const_mks.h:124

end gsl.const.mks;
