package body gsl.statistics is
   function mean (data : double_array) return double is
   begin
      return mean (data, 1, size_t (data'Length));
   end mean;

   function mean
     (data : access constant gsl.vector_double.gsl_vector) return double
   is
   begin
      return mean (data.data, 1, data.size);
   end mean;

   function variance (data : double_array) return double is
   begin
      return variance (data, 1, size_t (data'Length));
   end variance;

   function variance (data : access gsl.vector_double.gsl_vector) return double
   is
   begin
      return variance (data.data, 1, data.size);
   end variance;

   function sd (data : double_array) return double is
   begin
      return sd (data, 1, size_t (data'Length));
   end sd;
   function sd (data : access gsl.vector_double.gsl_vector) return double is
   begin
      return sd (data.data, 1, data.size);
   end sd;
   function variance_with_fixed_mean
     (data : double_array; mean : double) return double
   is
   begin
      return variance_with_fixed_mean (data, 1, size_t (data'Length), mean);
   end variance_with_fixed_mean;

   function variance_with_fixed_mean
     (data : access constant gsl.vector_double.gsl_vector; mean : double)
      return double
   is
   begin
      return variance_with_fixed_mean (data.data, 1, data.size, mean);
   end variance_with_fixed_mean;

   function sd_with_fixed_mean
     (data : double_array; mean : double) return double
   is
   begin
      return sd_with_fixed_mean (data, 1, size_t (data'Length), mean);
   end sd_with_fixed_mean;

   function sd_with_fixed_mean
     (data : access constant gsl.vector_double.gsl_vector; mean : double)
      return double
   is
   begin
      return sd_with_fixed_mean (data.data, 1, data.size, mean);
   end sd_with_fixed_mean;

   function tss (data : double_array) return double is
   begin
      return tss (data, 1, size_t (data'Length));
   end tss;

   function tss (data : access gsl.vector_double.gsl_vector) return double is
   begin
      return tss (data.data, 1, data.size);
   end tss;

   function tss_m (data : double_array; mean : double) return double is
   begin
      return tss_m (data, 1, size_t (data'Length), mean);
   end tss_m;

   function tss_m
     (data : access gsl.vector_double.gsl_vector; mean : double) return double
   is
   begin
      return tss_m (data.data, 1, data.size, mean);
   end tss_m;

   function absdev (data : double_array) return double is
   begin
      return absdev (data, 1, size_t (data'Length));
   end absdev;

   function absdev (data : access gsl.vector_double.gsl_vector) return double
   is
   begin
      return absdev (data.data, 1, data.size);
   end absdev;

   function skew (data : double_array) return double is
   begin
      return skew (data, 1, size_t (data'Length));
   end skew;

   function skew (data : access gsl.vector_double.gsl_vector) return double is
   begin
      return skew (data.data, 1, data.size);
   end skew;

   function kurtosis (data : double_array) return double is
   begin
      return kurtosis (data, 1, size_t (data'Length));
   end kurtosis;
   function kurtosis (data : access gsl.vector_double.gsl_vector) return double
   is
   begin
      return kurtosis (data.data, 1, data.size);
   end kurtosis;

   function lag1_autocorrelation (data : double_array) return double is
   begin
      return lag1_autocorrelation (data, 1, size_t (data'Length));
   end lag1_autocorrelation;

   function lag1_autocorrelation
     (data : access gsl.vector_double.gsl_vector) return double
   is
   begin
      return lag1_autocorrelation (data.data, 1, data.size);
   end lag1_autocorrelation;

   function covariance
     (data1 : double_array; data2 : double_array) return double
   is
   begin
      return covariance (data1, 1, data2, 1, size_t (data1'Length));
   end covariance;

   function covariance
     (data1 : access gsl.vector_double.gsl_vector;
      data2 : access gsl.vector_double.gsl_vector) return double
   is
   begin
      return covariance (data1.data, 1, data2.data, 1, data1.size);
   end covariance;

   function correlation
     (data1 : double_array; data2 : double_array) return double
   is
   begin
      return correlation (data1, 1, data2, 1, size_t (data1'Length));
   end correlation;

   function correlation
     (data1 : access gsl.vector_double.gsl_vector;
      data2 : access gsl.vector_double.gsl_vector) return double
   is
   begin
      return correlation (data1.data, 1, data2.data, 1, data1.size);
   end correlation;

   function max (data : double_array) return double is
   begin
      return max (data, 1, size_t (data'Length));
   end max;

   function min (data : double_array) return double is
   begin
      return min (data, 1, size_t (data'Length));
   end min;

   function max_index (data : Long_Float_array) return size_t is
   begin
      return max_index (data, 1, size_t (data'Length));
   end max_index;

   function min_index (data : Long_Float_array) return size_t is
   begin
      return min_index (data, 1, size_t (data'Length));
   end min_index;

   function median_from_sorted_data (sorted_data : double_array) return double
   is
   begin
      return
        median_from_sorted_data (sorted_data, 1, size_t (sorted_data'Length));
   end median_from_sorted_data;

   procedure median (sorted_data : in out double_array; result : out double) is
   begin
      result := median (sorted_data, 1, size_t (sorted_data'Length));
   end median;

   function quantile_from_sorted_data
     (sorted_data : double_array; f : double) return double
   is
   begin
      return
        quantile_from_sorted_data
          (sorted_data, 1, size_t (sorted_data'Length), f);
   end quantile_from_sorted_data;
   function gastwirth_from_sorted_data
     (sorted_data : double_array) return double
   is
   begin
      return
        gastwirth_from_sorted_data
          (sorted_data, 1, size_t (sorted_data'Length));
   end gastwirth_from_sorted_data;

end gsl.statistics;
