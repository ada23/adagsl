pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C.Strings;

with System;
with gsl.interp;

package gsl.interp2d is

   --  skipped anonymous struct anon_anon_20

   type gsl_interp2d_type is record
      name : Interfaces.C.Strings
        .chars_ptr;  -- /usr/include/gsl/gsl_interp2d.h:38
      min_size : aliased unsigned;  -- /usr/include/gsl/gsl_interp2d.h:39
      alloc    : access function
        (arg1 : size_t; arg2 : size_t)
         return System.Address;  -- /usr/include/gsl/gsl_interp2d.h:40
      init : access function
        (arg1 : System.Address; arg2 : double_array; arg3 : double_array;
         arg4 : double_array; arg5 : size_t; arg6 : size_t)
         return int;  -- /usr/include/gsl/gsl_interp2d.h:41
      eval : access function
        (arg1  : System.Address; arg2 : double_array; arg3 : double_array;
         arg4  : double_array; arg5 : size_t; arg6 : size_t; arg7 : double;
         arg8  : double; arg9 : access gsl.interp.gsl_interp_accel;
         arg10 : access gsl.interp.gsl_interp_accel; arg11 : double_array)
         return int;  -- /usr/include/gsl/gsl_interp2d.h:42
      eval_deriv_x : access function
        (arg1  : System.Address; arg2 : double_array; arg3 : double_array;
         arg4  : double_array; arg5 : size_t; arg6 : size_t; arg7 : double;
         arg8  : double; arg9 : access gsl.interp.gsl_interp_accel;
         arg10 : access gsl.interp.gsl_interp_accel; arg11 : double_array)
         return int;  -- /usr/include/gsl/gsl_interp2d.h:43
      eval_deriv_y : access function
        (arg1  : System.Address; arg2 : double_array; arg3 : double_array;
         arg4  : double_array; arg5 : size_t; arg6 : size_t; arg7 : double;
         arg8  : double; arg9 : access gsl.interp.gsl_interp_accel;
         arg10 : access gsl.interp.gsl_interp_accel; arg11 : double_array)
         return int;  -- /usr/include/gsl/gsl_interp2d.h:44
      eval_deriv_xx : access function
        (arg1  : System.Address; arg2 : double_array; arg3 : double_array;
         arg4  : double_array; arg5 : size_t; arg6 : size_t; arg7 : double;
         arg8  : double; arg9 : access gsl.interp.gsl_interp_accel;
         arg10 : access gsl.interp.gsl_interp_accel; arg11 : double_array)
         return int;  -- /usr/include/gsl/gsl_interp2d.h:45
      eval_deriv_xy : access function
        (arg1  : System.Address; arg2 : double_array; arg3 : double_array;
         arg4  : double_array; arg5 : size_t; arg6 : size_t; arg7 : double;
         arg8  : double; arg9 : access gsl.interp.gsl_interp_accel;
         arg10 : access gsl.interp.gsl_interp_accel; arg11 : double_array)
         return int;  -- /usr/include/gsl/gsl_interp2d.h:46
      eval_deriv_yy : access function
        (arg1  : System.Address; arg2 : double_array; arg3 : double_array;
         arg4  : double_array; arg5 : size_t; arg6 : size_t; arg7 : double;
         arg8  : double; arg9 : access gsl.interp.gsl_interp_accel;
         arg10 : access gsl.interp.gsl_interp_accel; arg11 : double_array)
         return int;  -- /usr/include/gsl/gsl_interp2d.h:47
      free : access procedure
        (arg1 : System.Address);  -- /usr/include/gsl/gsl_interp2d.h:48
   end record with
      Convention => C_Pass_By_Copy;  -- /usr/include/gsl/gsl_interp2d.h:49

      --  skipped anonymous struct anon_anon_21

   type gsl_interp2d is record
      c_type : access constant gsl_interp2d_type;  -- /usr/include/gsl/gsl_interp2d.h:52
      xmin   : aliased double;  -- /usr/include/gsl/gsl_interp2d.h:53
      xmax   : aliased double;  -- /usr/include/gsl/gsl_interp2d.h:54
      ymin   : aliased double;  -- /usr/include/gsl/gsl_interp2d.h:55
      ymax   : aliased double;  -- /usr/include/gsl/gsl_interp2d.h:56
      xsize  : aliased size_t;  -- /usr/include/gsl/gsl_interp2d.h:57
      ysize  : aliased size_t;  -- /usr/include/gsl/gsl_interp2d.h:58
      state  : System.Address;  -- /usr/include/gsl/gsl_interp2d.h:59
   end record with
      Convention => C_Pass_By_Copy;  -- /usr/include/gsl/gsl_interp2d.h:60

   gsl_interp2d_bilinear : access constant gsl_interp2d_type  -- /usr/include/gsl/gsl_interp2d.h:63
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_interp2d_bilinear";

   gsl_interp2d_bicubic : access constant gsl_interp2d_type  -- /usr/include/gsl/gsl_interp2d.h:64
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_interp2d_bicubic";

   function alloc
     (T : access constant gsl_interp2d_type; xsize : size_t; ysize : size_t)
      return access gsl_interp2d  -- /usr/include/gsl/gsl_interp2d.h:66
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_interp2d_alloc";

   function name
     (interp : access constant gsl_interp2d)
      return Interfaces.C.Strings
     .chars_ptr  -- /usr/include/gsl/gsl_interp2d.h:69
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_interp2d_name";

   function min_size
     (interp : access constant gsl_interp2d)
      return size_t  -- /usr/include/gsl/gsl_interp2d.h:70
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_interp2d_min_size";

   function type_min_size
     (T : access constant gsl_interp2d_type)
      return size_t  -- /usr/include/gsl/gsl_interp2d.h:71
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_interp2d_type_min_size";

   function set
     (interp : access constant gsl_interp2d; zarr : double_array; i : size_t;
      j      : size_t;
      z      : double)
      return int  -- /usr/include/gsl/gsl_interp2d.h:72
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_interp2d_set";

   function get
     (interp : access constant gsl_interp2d; zarr : double_array; i : size_t;
      j      : size_t)
      return double  -- /usr/include/gsl/gsl_interp2d.h:74
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_interp2d_get";

   function idx
     (interp : access constant gsl_interp2d; i : size_t; j : size_t)
      return size_t  -- /usr/include/gsl/gsl_interp2d.h:76
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_interp2d_idx";

   function init
     (interp : access gsl_interp2d; xa : double_array; ya : double_array;
      za     : double_array;
      xsize  : size_t;
      ysize  : size_t)
      return int  -- /usr/include/gsl/gsl_interp2d.h:78
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_interp2d_init";

   procedure gsl_interp2d_free
     (interp : access gsl_interp2d)  -- /usr/include/gsl/gsl_interp2d.h:80
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_interp2d_free";

   function eval
     (interp : access constant gsl_interp2d; xarr : double_array;
      yarr   : double_array; zarr : double_array; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel)
      return double  -- /usr/include/gsl/gsl_interp2d.h:82
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_interp2d_eval";

   function eval_extrap
     (interp : access constant gsl_interp2d; xarr : double_array;
      yarr   : double_array; zarr : double_array; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel)
      return double  -- /usr/include/gsl/gsl_interp2d.h:86
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_interp2d_eval_extrap";

   function eval_e
     (interp : access constant gsl_interp2d; xarr : double_array;
      yarr   : double_array; zarr : double_array; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel; z : double_array)
      return int  -- /usr/include/gsl/gsl_interp2d.h:92
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_interp2d_eval_e";

   function eval_e_extrap
     (interp : access constant gsl_interp2d; xarr : double_array;
      yarr   : double_array; zarr : double_array; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel; z : double_array)
      return int  -- /usr/include/gsl/gsl_interp2d.h:99
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_interp2d_eval_e_extrap";

   function eval_extrap_e
     (interp : access constant gsl_interp2d; xarr : double_array;
      yarr   : double_array; zarr : double_array; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel; z : double_array)
      return int  -- /usr/include/gsl/gsl_interp2d.h:111
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_interp2d_eval_extrap_e";

   function eval_deriv_x
     (interp : access constant gsl_interp2d; xarr : double_array;
      yarr   : double_array; zarr : double_array; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel)
      return double  -- /usr/include/gsl/gsl_interp2d.h:121
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_interp2d_eval_deriv_x";

   function eval_deriv_x_e
     (interp : access constant gsl_interp2d; xarr : double_array;
      yarr   : double_array; zarr : double_array; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel; z : double_array)
      return int  -- /usr/include/gsl/gsl_interp2d.h:126
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_interp2d_eval_deriv_x_e";

   function eval_deriv_y
     (interp : access constant gsl_interp2d; xarr : double_array;
      yarr   : double_array; zarr : double_array; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel)
      return double  -- /usr/include/gsl/gsl_interp2d.h:131
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_interp2d_eval_deriv_y";

   function eval_deriv_y_e
     (interp : access constant gsl_interp2d; xarr : double_array;
      yarr   : double_array; zarr : double_array; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel; z : double_array)
      return int  -- /usr/include/gsl/gsl_interp2d.h:136
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_interp2d_eval_deriv_y_e";

   function eval_deriv_xx
     (interp : access constant gsl_interp2d; xarr : double_array;
      yarr   : double_array; zarr : double_array; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel)
      return double  -- /usr/include/gsl/gsl_interp2d.h:141
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_interp2d_eval_deriv_xx";

   function eval_deriv_xx_e
     (interp : access constant gsl_interp2d; xarr : double_array;
      yarr   : double_array; zarr : double_array; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel; z : double_array)
      return int  -- /usr/include/gsl/gsl_interp2d.h:146
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_interp2d_eval_deriv_xx_e";

   function eval_deriv_yy
     (interp : access constant gsl_interp2d; xarr : double_array;
      yarr   : double_array; zarr : double_array; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel)
      return double  -- /usr/include/gsl/gsl_interp2d.h:151
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_interp2d_eval_deriv_yy";

   function eval_deriv_yy_e
     (interp : access constant gsl_interp2d; xarr : double_array;
      yarr   : double_array; zarr : double_array; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel; z : double_array)
      return int  -- /usr/include/gsl/gsl_interp2d.h:156
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_interp2d_eval_deriv_yy_e";

   function eval_deriv_xy
     (interp : access constant gsl_interp2d; xarr : double_array;
      yarr   : double_array; zarr : double_array; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel)
      return double  -- /usr/include/gsl/gsl_interp2d.h:161
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_interp2d_eval_deriv_xy";

   function eval_deriv_xy_e
     (interp : access constant gsl_interp2d; xarr : double_array;
      yarr   : double_array; zarr : double_array; x : double; y : double;
      xa     : access gsl.interp.gsl_interp_accel;
      ya     : access gsl.interp.gsl_interp_accel; z : double_array)
      return int  -- /usr/include/gsl/gsl_interp2d.h:166
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_interp2d_eval_deriv_xy_e";

end gsl.interp2d;
