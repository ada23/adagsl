with System;
package gsl.block is
   type gsl_block_struct is record
      size : aliased size_t;  -- /usr/include/gsl/gsl_block_double.h:40
      data : System.Address;  -- /usr/include/gsl/gsl_block_double.h:41
   end record with
      Convention => C_Pass_By_Copy;  -- /usr/include/gsl/gsl_block_double.h:38

   subtype gsl_block is gsl_block_struct;  --

end gsl.block;
