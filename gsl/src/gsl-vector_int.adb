package body gsl.vector_int is


   use Int_text_Io;
   use size_t_text_Io;
   procedure WriteCSV
     (v  : access constant gsl_vector; fn : String;
      v1 : access constant gsl_vector := null;
      v2 : access constant gsl_vector := null;
      v3 : access constant gsl_vector := null;
      v4 : access constant gsl_vector := null;
      v5 : access constant gsl_vector := null)
   is
      f : File_Type;
   begin
      Create (f, Out_File, fn);
      Set_Output (f);
      for i in 1 .. v.size loop
         Put (i);
         Put (" ; ");
         Put (get (v, i - 1));
         Put (" ; ");
         if v1 /= null then
            Put (get (v1, i - 1));
            Put (" ; ");
         end if;
         if v2 /= null then
            Put (get (v2, i - 1));
            Put (" ; ");
         end if;
         if v3 /= null then
            Put (get (v3, i - 1));
            Put (" ; ");
         end if;
         if v4 /= null then
            Put (get (v4, i - 1));
            Put (" ; ");
         end if;
         if v5 /= null then
            Put (get (v5, i - 1));
            Put (" ; ");
         end if;

         New_Line;
      end loop;
      Set_Output (Standard_Output);
      Close (f);
   end WriteCSV;

end gsl.vector_int;
