pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

package gsl.const.num is

   GSL_CONST_NUM_FINE_STRUCTURE : constant double :=
     (7.297_352_533e-3);  --  /usr/include/gsl/gsl_const_num.h:24
   GSL_CONST_NUM_AVOGADRO : constant double :=
     (6.022_141_99e23);  --  /usr/include/gsl/gsl_const_num.h:25
   GSL_CONST_NUM_YOTTA : constant double :=
     (1.0e24);  --  /usr/include/gsl/gsl_const_num.h:26
   GSL_CONST_NUM_ZETTA : constant double :=
     (1.0e21);  --  /usr/include/gsl/gsl_const_num.h:27
   GSL_CONST_NUM_EXA : constant double :=
     (1.0e18);  --  /usr/include/gsl/gsl_const_num.h:28
   GSL_CONST_NUM_PETA : constant double :=
     (1.0e15);  --  /usr/include/gsl/gsl_const_num.h:29
   GSL_CONST_NUM_TERA : constant double :=
     (1.0e12);  --  /usr/include/gsl/gsl_const_num.h:30
   GSL_CONST_NUM_GIGA : constant double :=
     (1.0e9);  --  /usr/include/gsl/gsl_const_num.h:31
   GSL_CONST_NUM_MEGA : constant double :=
     (1.0e6);  --  /usr/include/gsl/gsl_const_num.h:32
   GSL_CONST_NUM_KILO : constant double :=
     (1.0e3);  --  /usr/include/gsl/gsl_const_num.h:33
   GSL_CONST_NUM_MILLI : constant double :=
     (1.0e-3);  --  /usr/include/gsl/gsl_const_num.h:34
   GSL_CONST_NUM_MICRO : constant double :=
     (1.0e-6);  --  /usr/include/gsl/gsl_const_num.h:35
   GSL_CONST_NUM_NANO : constant double :=
     (1.0e-9);  --  /usr/include/gsl/gsl_const_num.h:36
   GSL_CONST_NUM_PICO : constant double :=
     (1.0e-12);  --  /usr/include/gsl/gsl_const_num.h:37
   GSL_CONST_NUM_FEMTO : constant double :=
     (1.0e-15);  --  /usr/include/gsl/gsl_const_num.h:38
   GSL_CONST_NUM_ATTO : constant double :=
     (1.0e-18);  --  /usr/include/gsl/gsl_const_num.h:39
   GSL_CONST_NUM_ZEPTO : constant double :=
     (1.0e-21);  --  /usr/include/gsl/gsl_const_num.h:40
   GSL_CONST_NUM_YOCTO : constant double :=
     (1.0e-24);  --  /usr/include/gsl/gsl_const_num.h:41

end gsl.const.num;
