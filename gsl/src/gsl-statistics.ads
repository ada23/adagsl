pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with gsl.vector_double;

package gsl.statistics is

   function mean
     (data   : access double;
      stride : size_t;
      n      : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:38
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_mean";

   function mean
     (data   : double_array;
      stride : size_t;
      n      : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:38
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_mean";

   function mean (data : double_array) return double;
   function mean
     (data : access constant gsl.vector_double.gsl_vector) return double;

   function variance
     (data   : access double;
      stride : size_t;
      n      : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:39
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_variance";

   function variance
     (data   : double_array;
      stride : size_t;
      n      : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:39
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_variance";

   function variance (data : double_array) return double;
   function variance
     (data : access gsl.vector_double.gsl_vector) return double;

   function sd
     (data   : double_array;
      stride : size_t;
      n      : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:40
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_sd";

   function sd
     (data   : access double;
      stride : size_t;
      n      : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:40
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_sd";

   function sd (data : double_array) return double;
   function sd (data : access gsl.vector_double.gsl_vector) return double;

   function variance_with_fixed_mean
     (data : double_array; stride : size_t; n : size_t; mean : double)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:41
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_variance_with_fixed_mean";

   function variance_with_fixed_mean
     (data : access double; stride : size_t; n : size_t; mean : double)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:41
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_variance_with_fixed_mean";

   function variance_with_fixed_mean
     (data : double_array; mean : double) return double;

   function variance_with_fixed_mean
     (data : access constant gsl.vector_double.gsl_vector; mean : double)
      return double;

   function sd_with_fixed_mean
     (data : double_array; stride : size_t; n : size_t; mean : double)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:42
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_sd_with_fixed_mean";

   function sd_with_fixed_mean
     (data : access double; stride : size_t; n : size_t; mean : double)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:42
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_sd_with_fixed_mean";

   function sd_with_fixed_mean
     (data : double_array; mean : double) return double;

   function sd_with_fixed_mean
     (data : access constant gsl.vector_double.gsl_vector; mean : double)
      return double;

   function tss
     (data   : double_array;
      stride : size_t;
      n      : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:43
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_tss";

   function tss
     (data   : access double;
      stride : size_t;
      n      : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:43
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_tss";
   function tss (data : double_array) return double;
   function tss (data : access gsl.vector_double.gsl_vector) return double;

   function tss_m
     (data : double_array; stride : size_t; n : size_t; mean : double)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:44
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_tss_m";

   function tss_m
     (data : access double; stride : size_t; n : size_t; mean : double)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:44
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_tss_m";

   function tss_m (data : double_array; mean : double) return double;
   function tss_m
     (data : access gsl.vector_double.gsl_vector; mean : double) return double;

   function absdev
     (data   : double_array;
      stride : size_t;
      n      : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:46
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_absdev";
   function absdev
     (data   : access double;
      stride : size_t;
      n      : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:46
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_absdev";

   function absdev (data : double_array) return double;
   function absdev (data : access gsl.vector_double.gsl_vector) return double;

   function skew
     (data   : double_array;
      stride : size_t;
      n      : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:47
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_skew";

   function skew
     (data   : access double;
      stride : size_t;
      n      : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:47
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_skew";

   function skew (data : double_array) return double;
   function skew (data : access gsl.vector_double.gsl_vector) return double;

   function kurtosis
     (data   : double_array;
      stride : size_t;
      n      : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:48
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_kurtosis";

   function kurtosis
     (data   : access double;
      stride : size_t;
      n      : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:48
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_kurtosis";
   function kurtosis (data : double_array) return double;
   function kurtosis
     (data : access gsl.vector_double.gsl_vector) return double;

   function lag1_autocorrelation
     (data   : double_array;
      stride : size_t;
      n      : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:49
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_lag1_autocorrelation";
   function lag1_autocorrelation
     (data   : access double;
      stride : size_t;
      n      : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:49
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_lag1_autocorrelation";

   function lag1_autocorrelation (data : double_array) return double;

   function lag1_autocorrelation
     (data : access gsl.vector_double.gsl_vector) return double;

   function covariance
     (data1   : double_array; stride1 : size_t; data2 : double_array;
      stride2 : size_t;
      n       : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:51
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_covariance";
   function covariance
     (data1   : access double; stride1 : size_t; data2 : access double;
      stride2 : size_t;
      n       : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:51
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_covariance";
   function covariance
     (data1 : double_array; data2 : double_array) return double;

   function covariance
     (data1 : access gsl.vector_double.gsl_vector;
      data2 : access gsl.vector_double.gsl_vector) return double;

   function correlation
     (data1   : double_array; stride1 : size_t; data2 : double_array;
      stride2 : size_t;
      n       : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:52
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_correlation";

   function correlation
     (data1   : access double; stride1 : size_t; data2 : access double;
      stride2 : size_t;
      n       : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:52
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_correlation";

   function correlation
     (data1 : double_array; data2 : double_array) return double;

   function correlation
     (data1 : access gsl.vector_double.gsl_vector;
      data2 : access gsl.vector_double.gsl_vector) return double;

   function spearman
     (data1   : double_array; stride1 : size_t; data2 : double_array;
      stride2 : size_t;
      n       : size_t;
      work    : double_array)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:53
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_spearman";

   function variance_m
     (data : double_array; stride : size_t; n : size_t; mean : double)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:55
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_variance_m";

   function sd_m
     (data : double_array; stride : size_t; n : size_t; mean : double)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:56
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_sd_m";

   function absdev_m
     (data : double_array; stride : size_t; n : size_t; mean : double)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:57
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_absdev_m";

   function skew_m_sd
     (data : double_array; stride : size_t; n : size_t; mean : double;
      sd   : double)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:58
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_skew_m_sd";

   function kurtosis_m_sd
     (data : double_array; stride : size_t; n : size_t; mean : double;
      sd   : double)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:59
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_kurtosis_m_sd";

   function lag1_autocorrelation_m
     (data : double_array; stride : size_t; n : size_t; mean : double)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:60
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_lag1_autocorrelation_m";

   function covariance_m
     (data1   : double_array; stride1 : size_t; data2 : double_array;
      stride2 : size_t; n : size_t; mean1 : double; mean2 : double)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:62
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_covariance_m";

   function wmean
     (w : double_array; wstride : size_t; data : double_array; stride : size_t;
      n : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:66
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_wmean";

   function wvariance
     (w : double_array; wstride : size_t; data : double_array; stride : size_t;
      n : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:67
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_wvariance";

   function wsd
     (w : double_array; wstride : size_t; data : double_array; stride : size_t;
      n : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:68
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_wsd";

   function wvariance_with_fixed_mean
     (w : double_array; wstride : size_t; data : double_array; stride : size_t;
      n    : size_t;
      mean : double)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:69
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_wvariance_with_fixed_mean";

   function wsd_with_fixed_mean
     (w : double_array; wstride : size_t; data : double_array; stride : size_t;
      n    : size_t;
      mean : double)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:70
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_wsd_with_fixed_mean";

   function wtss
     (w : double_array; wstride : size_t; data : double_array; stride : size_t;
      n : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:71
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_wtss";

   function wtss_m
     (w : double_array; wstride : size_t; data : double_array; stride : size_t;
      n     : size_t;
      wmean : double)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:72
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_wtss_m";

   function wabsdev
     (w : double_array; wstride : size_t; data : double_array; stride : size_t;
      n : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:73
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_wabsdev";

   function wskew
     (w : double_array; wstride : size_t; data : double_array; stride : size_t;
      n : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:74
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_wskew";

   function wkurtosis
     (w : double_array; wstride : size_t; data : double_array; stride : size_t;
      n : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:75
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_wkurtosis";

   function wvariance_m
     (w : double_array; wstride : size_t; data : double_array; stride : size_t;
      n     : size_t;
      wmean : double)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:77
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_wvariance_m";

   function wsd_m
     (w : double_array; wstride : size_t; data : double_array; stride : size_t;
      n     : size_t;
      wmean : double)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:78
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_wsd_m";

   function wabsdev_m
     (w : double_array; wstride : size_t; data : double_array; stride : size_t;
      n     : size_t;
      wmean : double)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:79
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_wabsdev_m";

   function wskew_m_sd
     (w : double_array; wstride : size_t; data : double_array; stride : size_t;
      n     : size_t;
      wmean : double;
      wsd   : double)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:80
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_wskew_m_sd";

   function wkurtosis_m_sd
     (w : double_array; wstride : size_t; data : double_array; stride : size_t;
      n     : size_t;
      wmean : double;
      wsd   : double)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:81
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_wkurtosis_m_sd";

   function pvariance
     (data1   : double_array; stride1 : size_t; n1 : size_t;
      data2   : double_array;
      stride2 : size_t;
      n2      : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:85
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_pvariance";

   function ttest
     (data1   : double_array; stride1 : size_t; n1 : size_t;
      data2   : double_array;
      stride2 : size_t;
      n2      : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:86
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_ttest";

   function max
     (data   : double_array;
      stride : size_t;
      n      : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:88
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_max";

   function max (data : double_array) return double;

   function min
     (data   : double_array;
      stride : size_t;
      n      : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:89
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_min";

   function min (data : double_array) return double;

   procedure minmax
     (min    : access Long_Float; max : access Long_Float; data : double_array;
      stride : size_t;
      n      : size_t)  -- /usr/include/gsl/gsl_statistics_double.h:90
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_minmax";

   function max_index
     (data   : double_array;
      stride : size_t;
      n      : size_t)
      return size_t  -- /usr/include/gsl/gsl_statistics_double.h:92
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_max_index";

   function max_index (data : Long_Float_array) return size_t;

   function min_index
     (data   : double_array;
      stride : size_t;
      n      : size_t)
      return size_t  -- /usr/include/gsl/gsl_statistics_double.h:93
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_min_index";

   function min_index (data : Long_Float_array) return size_t;

   procedure minmax_index
     (min_index : access size_t; max_index : access size_t;
      data      : double_array;
      stride    : size_t;
      n         : size_t)  -- /usr/include/gsl/gsl_statistics_double.h:94
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_minmax_index";

   function sselect
     (data : double_array; stride : size_t; n : size_t; k : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:96
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_select";

   function median_from_sorted_data
     (sorted_data : double_array; stride : size_t; n : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:98
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_median_from_sorted_data";
   function median_from_sorted_data (sorted_data : double_array) return double;

   function median
     (sorted_data : double_array; stride : size_t; n : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:99
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_median";

   procedure median (sorted_data : in out double_array; result : out double);

   function quantile_from_sorted_data
     (sorted_data : double_array; stride : size_t; n : size_t; f : double)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:100
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_quantile_from_sorted_data";

   function quantile_from_sorted_data
     (sorted_data : double_array; f : double) return double;

   function trmean_from_sorted_data
     (trim : double; sorted_data : double_array; stride : size_t; n : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:102
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_trmean_from_sorted_data";

   function gastwirth_from_sorted_data
     (sorted_data : double_array; stride : size_t; n : size_t)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:103
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_gastwirth_from_sorted_data";
   function gastwirth_from_sorted_data
     (sorted_data : double_array) return double;

   function mad0
     (data : double_array; stride : size_t; n : size_t; work : double_array)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:105
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_mad0";

   function mad
     (data : double_array; stride : size_t; n : size_t; work : double_array)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:106
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_mad";

   function Sn0_from_sorted_data
     (sorted_data : double_array; stride : size_t; n : size_t;
      work        : double_array)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:108
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_Sn0_from_sorted_data";

   function Sn_from_sorted_data
     (sorted_data : double_array; stride : size_t; n : size_t;
      work        : double_array)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:109
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_Sn_from_sorted_data";

   function Qn0_from_sorted_data
     (sorted_data : double_array; stride : size_t; n : size_t;
      work        : double_array;
      work_int    : access int)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:111
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_Qn0_from_sorted_data";

   function Qn_from_sorted_data
     (sorted_data : double_array; stride : size_t; n : size_t;
      work        : double_array;
      work_int    : access int)
      return double  -- /usr/include/gsl/gsl_statistics_double.h:112
   with
      Import        => True,
      Convention    => C,
      External_Name => "gsl_stats_Qn_from_sorted_data";

end gsl.statistics;
