# Ada Binding to gsl

This is an Ada binding to the Gnu Scientific Library V2.17. [gsl](https://www.gnu.org/software/gsl/). It may be considered thinnish. Starting from an automatically generated binding using the g++ tool - something like:
```
g++ -fdump-ada-spec gsl*.h
```
individual packages were renamed to be more Ada friendly as have the function and procedures. Data structures on the other hand have not been renamed.

## Examples

The goal is to translate examples in the official document to Ada. These are organized by chapters mirroring the guide. In some cases, eg chebyshev approximations, some extra examples have been added.

[GnuPlot](http://www.gnuplot.info) is used to generate all the plots.

## Code Organization

The binding itself is in the directory gsl and built using the command:

```
alr build
```

The examples are in the appropriately named directory parallel to the above. Each chapter has a different alr crate and can be built independently.

## Getting started

You need to install gsl and gnuplot.
## Project status

Bindings are work in process. In particular transforms have not yet been bound and will be the subject of immediate effort.

